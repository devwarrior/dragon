package app

import (
	"gitlab.com/devwarrior/dragon/logger"

	"time"

	"github.com/therecipe/qt/core"
	pb "gitlab.com/devwarrior/dragon/session"
)

//qtsig(int)
type updater struct {
	core.QObject
	// qtmoc will generate ConnectUpdateState, UpdateState, NewUpdater and others...
	_ func(int) `signal:"updateState"`
}

const pollInterval = 1 * time.Second

// A statePoller polls the session state for changes from ourselves and the counterparty
type statePoller struct {
	sessionID    string
	ticker       *time.Ticker
	check        chan int
	done         chan int
	currentState pb.STATE
	sig          *updater
}

// newStatePoller constructor
func newStatePoller(s *session) *statePoller {
	statePoller := statePoller{}
	statePoller.sessionID = s.sessionID
	statePoller.ticker = time.NewTicker(pollInterval)
	statePoller.check = make(chan int, 1)
	statePoller.done = make(chan int, 1)
	statePoller.currentState = pb.STATE_NoSession
	statePoller.sig = NewUpdater(nil)
	statePoller.sig.ConnectUpdateState(s.updateState)
	return &statePoller
}

// checkPoll can be used to ask the poller to check state now!
func (p *statePoller) checkPoll() {
	p.check <- 1
}

// endPoll stops polling the session state
func (p *statePoller) endPoll() {
	p.ticker.Stop()
	p.done <- 1
}

// StartPoll starts polling the session state
func (p *statePoller) startPoll() {
	go p.poll()
}

// poll is a goro for polling the session state
func (p *statePoller) poll() {
	for {
		select {
		case <-p.check:
			p.doPoll()
		case <-p.ticker.C:
			p.doPoll()
		case <-p.done:
			return
		}
	}
}

// doPoll is called on the goro thread and gets the state - .
// If the state has changed it sends a qtsignal to the qtslot: updateState
func (p *statePoller) doPoll() {
	var state = pb.STATE_Unknown
	request := &pb.StateRequest{Session: p.sessionID}
	response, err := dbGetState(request)
	if err == nil {
		state = response.CurrentState
	}
	if state != p.currentState {
		logger.Log.Printf("State change: %v => %v\n", p.currentState, state)
		p.currentState = state
		p.sig.UpdateState(int(state))
	}
}

//qtslot(int)
func (s *session) updateState(newState int) {
	// For Qt Gui usage we are now back on the UI thread from the goro thread
	logger.Log.Printf("poller: update() session=%s, new state=%d\n", s.sessionID, newState)
	s.stateChanged(pb.STATE(newState))
}

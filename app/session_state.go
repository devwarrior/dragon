package app

import (
	"gitlab.com/devwarrior/dragon/logger"

	pb "gitlab.com/devwarrior/dragon/session"
)

type sErr int32

const (
	sErrOK        sErr = 0
	sErrData      sErr = 1
	sErrTransport sErr = -1
)

// stateChanged is called from the poller when remote session state changes.
// We are guaranteed to be running on the main UI thread here
func (s *session) stateChanged(pollerState pb.STATE) {

	logger.Log.Printf("stateChanged: pollerState=%v(%d)\n", pollerState, pollerState)

	// ...if state moved to marked deleted by counterparty
	if pollerState == pb.STATE_MarkedDeleted {
		// mark local.session invalid
		s.endSessionAndInvalidateLocal()
		//tell UI
		s.app.onSessionMarkedDeleted(s.sessionID)
		//early exit
		return
	}

	// Get session data, including state (again)
	request := &pb.DataRequest{Session: s.sessionID}
	// Poller returns STATE_Unknown on _any_ error
	// The dbGetData1 function gives a better idea of error types
	response, err := dbGetData1(request)
	if err != nil {
		logger.Log.Printf("stateChanged - getData - error: %v", err)
		//Transport or other grpc error not of our making
		s.app.onSessionStateUnknown(sErrTransport)
		return
	}
	if response.Errno != pb.ERRNO_OK {
		logger.Log.Printf("stateChanged - getData - Errno: %v - %s", response.Errno, response.Errstr)
		// Internal database error sent back
		s.app.onSessionStateUnknown(sErrData)
		return
	}

	// Log if the state from calling GetData is different from the poller state
	if pollerState != response.Data.State {
		logger.Log.Printf("stateChanged - getData - State: %v -> %v", pollerState, response.Data.State)
	}

	// DBG: Really Unknown - currently unused by the database
	if response.Data.State == pb.STATE_Unknown {
		logger.Log.Printf("stateChanged - getData - State: %v", response.Data.State)
		// State is now returned as Unknown (-1)
		s.app.onSessionStateUnknown(sErrOK)
		return
	}

	// DBG: NoSession - Poller initial state - currently unused by the database
	if response.Data.State == pb.STATE_NoSession {
		logger.Log.Printf("stateChanged - getData - State: %v", response.Data.State)
		// State is now returned as NoSession (-2)
		s.app.onSessionStateUnknown(sErrOK)
		return
	}

	// MarkedDeleted
	if response.Data.State == pb.STATE_MarkedDeleted {
		logger.Log.Printf("stateChanged - getData - State: %v", response.Data.State)
		// State is now returned as MarkedDeleted (-3)
		// mark local.session invalid
		s.endSessionAndInvalidateLocal()
		// tell UI
		s.app.onSessionMarkedDeleted(s.sessionID)
		return
	}

	uiData := s.getUIData(s.role, response.Data)

	s.app.onSessionStateChanged(response.Data.State, uiData)
}

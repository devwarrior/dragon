package local

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"

	"gitlab.com/devwarrior/dragon/logger"

	"os"
)

// Role is the local session owner swap role
type Role int

// Local roles
const (
	Initiator   Role = 0
	Participant Role = 1
)

// LocalSession represents one of perhaps many local sessions
type LocalSession struct {
	SessionID          string `json:"session"`
	Role               Role   `json:"role"`
	OptSsig            string `json:"optssig,omitempty"`
	Testnet            bool   `json:"testnet,omitempty"`
	CPCoin             string `json:"cp_coin"`
	CPContract         string `json:"cp_contract,omitempty"`
	CPContractTx       string `json:"cp_contract_tx,omitempty"`
	CPContractP2SH     string `json:"cp_contract_p2sh,omitempty"`
	CPContractLocktime int64  `json:"cp_contract_locktime,omitempty"`
	CPContractRedeemed bool   `json:"cp_contract_redeemed,omitempty"`
	Invalid            bool   `json:"invalid,omitempty"`
}

// LocalSessions is a list of all local session objects
var localSessions []*LocalSession

const (
	localSessionsFile = "local.sessions"
	hexstr32len       = 32 * 2
)

func createLocalSessionsFileIfNotExist() error {
	_, err := os.Stat(localSessionsFile)
	if err == nil {
		return nil
	}
	if !os.IsNotExist(err) {
		return fmt.Errorf("file %s exists but cannot open - error: %v",
			localSessionsFile, err)
	}
	err = ioutil.WriteFile(localSessionsFile, []byte(`[]`), 0664)
	if err != nil {
		return fmt.Errorf("cannot write empty json array '[]' into the new file %s - error: %v",
			localSessionsFile, err)
	}
	logger.Log.Println("created new empty local sessions file with empty json array '[]'")
	return nil
}

func reload() error {
	rawData, err := ioutil.ReadFile(localSessionsFile)
	if err != nil {
		return err
	}
	// Unmarshal will allocate for us
	err = json.Unmarshal(rawData, &localSessions)
	if err != nil {
		return err
	}
	return nil
}

func save() error {
	b, err := json.MarshalIndent(&localSessions, "", "  ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(localSessionsFile, b, 0)
	if err != nil {
		return err
	}
	return nil
}

func getSession(sessionID string) *LocalSession {
	for _, s := range localSessions {
		if s.SessionID == sessionID {
			return s
		}
	}
	return nil
}

func sessionExists(sessionID string) bool {
	s := getSession(sessionID)
	return (s != nil)
}

func getValidSessionIDs() []string {
	var validLocalSessionIDs = []string{}
	for _, s := range localSessions {
		if !s.Invalid {
			validLocalSessionIDs = append(validLocalSessionIDs, s.SessionID)
		}
	}
	return validLocalSessionIDs
}

func appendNewSession(newSession *LocalSession) error {
	if len(newSession.SessionID) != hexstr32len {
		return errors.New("invalid session key length")
	}
	if sessionExists(newSession.SessionID) {
		return errors.New("invalid session key - already exists")
	}
	localSessions = append(localSessions, newSession)
	return nil
}

func removeSession(sessionID string) error {
	// slower but safer
	if !sessionExists(sessionID) {
		return errors.New("session does not exist")
	}
	// append will allocate for us
	var newArr []*LocalSession
	for _, s := range localSessions {
		if s.SessionID == sessionID {
			continue
		}
		newArr = append(newArr, s)
	}
	localSessions = newArr
	return nil
}

func removeInvalidSessions() error {
	// append will allocate for us
	var newArr []*LocalSession
	for _, s := range localSessions {
		if s.Invalid {
			continue
		}
		newArr = append(newArr, s)
	}
	localSessions = newArr
	return nil
}

func setContractData(sessionID, contract, contractTx, contractP2SH string, contractLocktime int64) error {
	session := getSession(sessionID)
	if session == nil {
		return errors.New("no such session")
	}
	session.CPContract = contract
	session.CPContractTx = contractTx
	session.CPContractP2SH = contractP2SH
	session.CPContractLocktime = contractLocktime
	session.CPContractRedeemed = false
	return nil
}

func setCounterpartyContractRedeemed(sessionID string) error {
	session := getSession(sessionID)
	if session == nil {
		return errors.New("no such session")
	}
	session.CPContractRedeemed = true
	return nil
}

func setInvalid(sessionID string) error {
	session := getSession(sessionID)
	if session == nil {
		return errors.New("no such session")
	}
	session.Invalid = true
	return nil
}

//////////////
// Exported //
//////////////

// Reload local sessions file from disk
func Reload() error {
	return reload()
}

// Load or create local sessions file on disk
func Load() error {
	err := createLocalSessionsFileIfNotExist()
	if err != nil {
		return err
	}
	return Reload()
}

// GetSession return a session object for a sessionID or nil
// if not exist
func GetSession(sessionID string) *LocalSession {
	return getSession(sessionID)
}

// AppendNewInitiatorSession appends a new initiator session object to
// localSessions list and saves object list to disk
func AppendNewInitiatorSession(sessionID, optSsig, cpCoin string, testnet bool) error {
	if len(sessionID) != hexstr32len {
		return errors.New("invalid session key length")
	}
	if len(optSsig) != hexstr32len {
		return errors.New("invalid optssig key length")
	}
	newSession := LocalSession{}
	newSession.SessionID = sessionID
	newSession.Role = Initiator
	newSession.OptSsig = optSsig
	newSession.CPCoin = cpCoin
	newSession.Testnet = testnet
	err := appendNewSession(&newSession)
	if err != nil {
		return err
	}
	return save()
}

// AppendNewParticipantSession appends a new participant session object to
// localSessions list and saves object list to disk
func AppendNewParticipantSession(sessionID, cpCoin string, testnet bool) error {
	newSession := LocalSession{}
	newSession.SessionID = sessionID
	newSession.Role = Participant
	newSession.CPCoin = cpCoin
	newSession.Testnet = testnet
	err := appendNewSession(&newSession)
	if err != nil {
		return err
	}
	return save()
}

// RemoveSession removes the session object from the localSessions list
func RemoveSession(sessionID string) error {
	err := removeSession(sessionID)
	if err != nil {
		return err
	}
	return save()
}

// SessionExists returns true if the session exists and is valid
func SessionExists(sessionID string) bool {
	s := getSession(sessionID)
	if s == nil {
		return false
	}
	if s.Invalid {
		return false
	}
	return true
}

/////////////////////
// Allowed Mutators//
/////////////////////

// SetContractData adds counterparty redeemable contract data to the session object
func SetContractData(sessionID, contract, contractTx, contractP2SH string, contractLocktime int64) error {
	err := setContractData(sessionID, contract, contractTx, contractP2SH, contractLocktime)
	if err != nil {
		return err
	}
	return save()
}

// SetCounterpartyContractRedeemed sets counterparty redeemable contract as redeemed
func SetCounterpartyContractRedeemed(sessionID string) error {
	err := setCounterpartyContractRedeemed(sessionID)
	if err != nil {
		return err
	}
	return save()
}

// SetInvalid invalidates the session object
func SetInvalid(sessionID string) error {
	err := setInvalid(sessionID)
	if err != nil {
		return err
	}
	return save()
}

///////////////////
// Sessions List //
///////////////////

// GetValidSessionIDs gets a list of valid (current) session IDs
func GetValidSessionIDs() []string {
	return getValidSessionIDs()
}

//////////////////
// Experimental //
//////////////////

// RemoveInvalidSessions removes all sessions marked as Invalid.
// Currently called on app startup
func RemoveInvalidSessions() error {
	err := removeInvalidSessions()
	if err != nil {
		return err
	}
	return save()
}

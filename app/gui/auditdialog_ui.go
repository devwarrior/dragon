// WARNING! All changes made in this file will be lost!
package gui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
)

type UIAuditDialog struct {
	ButtonBoxAcceptAudit     *widgets.QDialogButtonBox
	FormLayoutWidget         *widgets.QWidget
	FormLayout               *widgets.QFormLayout
	ContractAddressLabel     *widgets.QLabel
	ContractAddressLineEdit  *widgets.QLineEdit
	ContractValueLabel       *widgets.QLabel
	ContractValueLineEdit    *widgets.QLineEdit
	RecipientAddressLabel    *widgets.QLabel
	RecipientAddressLineEdit *widgets.QLineEdit
	RefundLocktimeLabel      *widgets.QLabel
	RefundLocktimeLineEdit   *widgets.QLineEdit
	VerticalLayoutWidget     *widgets.QWidget
	VerticalLayout           *widgets.QVBoxLayout
	Label                    *widgets.QLabel
	Label2                   *widgets.QLabel
}

func (this *UIAuditDialog) SetupUI(AuditDialog *widgets.QDialog) {
	AuditDialog.SetObjectName("AuditDialog")
	AuditDialog.SetWindowModality(core.Qt__WindowModal)
	AuditDialog.SetGeometry(core.NewQRect4(0, 0, 604, 244))
	AuditDialog.SetAutoFillBackground(false)
	this.ButtonBoxAcceptAudit = widgets.NewQDialogButtonBox(AuditDialog)
	this.ButtonBoxAcceptAudit.SetObjectName("ButtonBoxAcceptAudit")
	this.ButtonBoxAcceptAudit.SetGeometry(core.NewQRect4(400, 180, 181, 41))
	this.ButtonBoxAcceptAudit.SetLayoutDirection(core.Qt__LeftToRight)
	this.ButtonBoxAcceptAudit.SetOrientation(core.Qt__Horizontal)
	this.ButtonBoxAcceptAudit.SetStandardButtons(widgets.QDialogButtonBox__No | widgets.QDialogButtonBox__Yes)
	this.FormLayoutWidget = widgets.NewQWidget(AuditDialog, core.Qt__Widget)
	this.FormLayoutWidget.SetObjectName("FormLayoutWidget")
	this.FormLayoutWidget.SetGeometry(core.NewQRect4(20, 20, 561, 131))
	this.FormLayout = widgets.NewQFormLayout(this.FormLayoutWidget)
	this.FormLayout.SetObjectName("formLayout")
	this.FormLayout.SetContentsMargins(0, 0, 0, 0)
	this.FormLayout.SetSpacing(0)
	this.ContractAddressLabel = widgets.NewQLabel(this.FormLayoutWidget, core.Qt__Widget)
	this.ContractAddressLabel.SetObjectName("ContractAddressLabel")
	this.FormLayout.SetWidget(0, widgets.QFormLayout__LabelRole, this.ContractAddressLabel)
	this.ContractAddressLineEdit = widgets.NewQLineEdit(this.FormLayoutWidget)
	this.ContractAddressLineEdit.SetObjectName("ContractAddressLineEdit")
	this.ContractAddressLineEdit.SetReadOnly(true)
	this.FormLayout.SetWidget(0, widgets.QFormLayout__FieldRole, this.ContractAddressLineEdit)
	this.ContractValueLabel = widgets.NewQLabel(this.FormLayoutWidget, core.Qt__Widget)
	this.ContractValueLabel.SetObjectName("ContractValueLabel")
	this.FormLayout.SetWidget(1, widgets.QFormLayout__LabelRole, this.ContractValueLabel)
	this.ContractValueLineEdit = widgets.NewQLineEdit(this.FormLayoutWidget)
	this.ContractValueLineEdit.SetObjectName("ContractValueLineEdit")
	this.ContractValueLineEdit.SetReadOnly(true)
	this.FormLayout.SetWidget(1, widgets.QFormLayout__FieldRole, this.ContractValueLineEdit)
	this.RecipientAddressLabel = widgets.NewQLabel(this.FormLayoutWidget, core.Qt__Widget)
	this.RecipientAddressLabel.SetObjectName("RecipientAddressLabel")
	this.FormLayout.SetWidget(2, widgets.QFormLayout__LabelRole, this.RecipientAddressLabel)
	this.RecipientAddressLineEdit = widgets.NewQLineEdit(this.FormLayoutWidget)
	this.RecipientAddressLineEdit.SetObjectName("RecipientAddressLineEdit")
	this.RecipientAddressLineEdit.SetReadOnly(true)
	this.FormLayout.SetWidget(2, widgets.QFormLayout__FieldRole, this.RecipientAddressLineEdit)
	this.RefundLocktimeLabel = widgets.NewQLabel(this.FormLayoutWidget, core.Qt__Widget)
	this.RefundLocktimeLabel.SetObjectName("RefundLocktimeLabel")
	this.FormLayout.SetWidget(3, widgets.QFormLayout__LabelRole, this.RefundLocktimeLabel)
	this.RefundLocktimeLineEdit = widgets.NewQLineEdit(this.FormLayoutWidget)
	this.RefundLocktimeLineEdit.SetObjectName("RefundLocktimeLineEdit")
	this.RefundLocktimeLineEdit.SetReadOnly(true)
	this.FormLayout.SetWidget(3, widgets.QFormLayout__FieldRole, this.RefundLocktimeLineEdit)
	this.VerticalLayoutWidget = widgets.NewQWidget(AuditDialog, core.Qt__Widget)
	this.VerticalLayoutWidget.SetObjectName("VerticalLayoutWidget")
	this.VerticalLayoutWidget.SetGeometry(core.NewQRect4(20, 180, 321, 42))
	this.VerticalLayout = widgets.NewQVBoxLayout2(this.VerticalLayoutWidget)
	this.VerticalLayout.SetObjectName("verticalLayout")
	this.VerticalLayout.SetContentsMargins(0, 0, 0, 0)
	this.VerticalLayout.SetSpacing(0)
	this.Label = widgets.NewQLabel(this.VerticalLayoutWidget, core.Qt__Widget)
	this.Label.SetObjectName("Label")
	this.VerticalLayout.AddWidget(this.Label, 0, 0)
	this.Label2 = widgets.NewQLabel(this.VerticalLayoutWidget, core.Qt__Widget)
	this.Label2.SetObjectName("Label2")
	this.VerticalLayout.AddWidget(this.Label2, 0, 0)

	this.RetranslateUi(AuditDialog)

	this.ButtonBoxAcceptAudit.ConnectAccepted(AuditDialog.Accept)
	this.ButtonBoxAcceptAudit.ConnectRejected(AuditDialog.Reject)
}

func (this *UIAuditDialog) RetranslateUi(AuditDialog *widgets.QDialog) {
	_translate := core.QCoreApplication_Translate
	AuditDialog.SetWindowTitle(_translate("AuditDialog", "Audit contract", "", -1))
	this.ButtonBoxAcceptAudit.SetToolTip(_translate("AuditDialog", "Accept or reject the contract", "", -1))
	this.ContractAddressLabel.SetText(_translate("AuditDialog", "Contract Address", "", -1))
	this.ContractAddressLineEdit.SetToolTip(_translate("AuditDialog", "Contract P2SH address", "", -1))
	this.ContractValueLabel.SetText(_translate("AuditDialog", "Contract Value", "", -1))
	this.ContractValueLineEdit.SetToolTip(_translate("AuditDialog", "Contract value before network fees", "", -1))
	this.RecipientAddressLabel.SetText(_translate("AuditDialog", "RecipientAddress", "", -1))
	this.RecipientAddressLineEdit.SetToolTip(_translate("AuditDialog", "Address that receives the contract value when the contract is redeemed", "", -1))
	this.RefundLocktimeLabel.SetText(_translate("AuditDialog", "Refund Locktime", "", -1))
	this.RefundLocktimeLineEdit.SetToolTip(_translate("AuditDialog", "Time (UTC) after which a refund is possible", "", -1))
	this.Label.SetText(_translate("AuditDialog", "Yes to accept the contract, No to reject", "", -1))
	this.Label2.SetText(_translate("AuditDialog", "No will delete this session", "", -1))
}

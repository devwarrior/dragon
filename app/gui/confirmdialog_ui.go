// WARNING! All changes made in this file will be lost!
package gui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
)

type UIConfirmationDialog struct {
	HorizontalLayoutWidget *widgets.QWidget
	HorizontalLayout *widgets.QHBoxLayout
	PushButtonOk *widgets.QPushButton
	HorizontalSpacer *widgets.QSpacerItem
	PushButtonCancel *widgets.QPushButton
	PlainTextEditLog *widgets.QPlainTextEdit
	VerticalLayoutWidget *widgets.QWidget
	VerticalLayout *widgets.QVBoxLayout
	Label *widgets.QLabel
	Label2 *widgets.QLabel
}

func (this *UIConfirmationDialog) SetupUI(ConfirmationDialog *widgets.QDialog) {
	ConfirmationDialog.SetObjectName("ConfirmationDialog")
	ConfirmationDialog.SetGeometry(core.NewQRect4(0, 0, 677, 292))
	ConfirmationDialog.SetModal(true)
	this.HorizontalLayoutWidget = widgets.NewQWidget(ConfirmationDialog, core.Qt__Widget)
	this.HorizontalLayoutWidget.SetObjectName("HorizontalLayoutWidget")
	this.HorizontalLayoutWidget.SetGeometry(core.NewQRect4(470, 240, 191, 41))
	this.HorizontalLayout = widgets.NewQHBoxLayout2(this.HorizontalLayoutWidget)
	this.HorizontalLayout.SetObjectName("horizontalLayout")
	this.HorizontalLayout.SetContentsMargins(0, 0, 0, 0)
	this.HorizontalLayout.SetSpacing(0)
	this.PushButtonOk = widgets.NewQPushButton(this.HorizontalLayoutWidget)
	this.PushButtonOk.SetObjectName("PushButtonOk")
	this.PushButtonOk.SetEnabled(false)
	this.HorizontalLayout.AddWidget(this.PushButtonOk, 0, 0)
	this.HorizontalSpacer = widgets.NewQSpacerItem(40, 20, widgets.QSizePolicy__Expanding, widgets.QSizePolicy__Minimum)
	this.HorizontalLayout.AddItem(this.HorizontalSpacer)
	this.PushButtonCancel = widgets.NewQPushButton(this.HorizontalLayoutWidget)
	this.PushButtonCancel.SetObjectName("PushButtonCancel")
	this.PushButtonCancel.SetEnabled(true)
	this.HorizontalLayout.AddWidget(this.PushButtonCancel, 0, 0)
	this.PlainTextEditLog = widgets.NewQPlainTextEdit(ConfirmationDialog)
	this.PlainTextEditLog.SetObjectName("PlainTextEditLog")
	this.PlainTextEditLog.SetGeometry(core.NewQRect4(10, 10, 651, 221))
	this.PlainTextEditLog.SetReadOnly(true)
	this.VerticalLayoutWidget = widgets.NewQWidget(ConfirmationDialog, core.Qt__Widget)
	this.VerticalLayoutWidget.SetObjectName("VerticalLayoutWidget")
	this.VerticalLayoutWidget.SetGeometry(core.NewQRect4(10, 240, 441, 42))
	this.VerticalLayout = widgets.NewQVBoxLayout2(this.VerticalLayoutWidget)
	this.VerticalLayout.SetObjectName("verticalLayout")
	this.VerticalLayout.SetContentsMargins(0, 0, 0, 0)
	this.VerticalLayout.SetSpacing(0)
	this.Label = widgets.NewQLabel(this.VerticalLayoutWidget, core.Qt__Widget)
	this.Label.SetObjectName("Label")
	this.VerticalLayout.AddWidget(this.Label, 0, 0)
	this.Label2 = widgets.NewQLabel(this.VerticalLayoutWidget, core.Qt__Widget)
	this.Label2.SetObjectName("Label2")
	this.VerticalLayout.AddWidget(this.Label2, 0, 0)


    this.RetranslateUi(ConfirmationDialog)

}

func (this *UIConfirmationDialog) RetranslateUi(ConfirmationDialog *widgets.QDialog) {
    _translate := core.QCoreApplication_Translate
	ConfirmationDialog.SetWindowTitle(_translate("ConfirmationDialog", "Confirmation", "", -1))
	this.PushButtonOk.SetToolTip(_translate("ConfirmationDialog", "Exit the dialog after confirmation", "", -1))
	this.PushButtonOk.SetText(_translate("ConfirmationDialog", "OK", "", -1))
	this.PushButtonCancel.SetToolTip(_translate("ConfirmationDialog", "Cancel this dialogger.Log. Come back later to check confirmation status.", "", -1))
	this.PushButtonCancel.SetText(_translate("ConfirmationDialog", "Cancel", "", -1))
	this.Label.SetText(_translate("ConfirmationDialog", "Wait for confirmation that the tx has been mined then click OK", "", -1))
	this.Label2.SetText(_translate("ConfirmationDialog", "Cancel to exit dialog and come back later to check confirmation", "", -1))
}

// WARNING! All changes made in this file will be lost!
package gui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
)

type UIMainwindowMainWindow struct {
	Centralwidget             *widgets.QWidget
	FormLayoutWidget          *widgets.QWidget
	FormLayout                *widgets.QFormLayout
	SessionKeyLabel           *widgets.QLabel
	SessionKeyLineEdit        *widgets.QLineEdit
	SessionStateLabel         *widgets.QLabel
	SessionStateLineEdit      *widgets.QLineEdit
	TakerAddressLabel         *widgets.QLabel
	TakerAddressLineEdit      *widgets.QLineEdit
	MakerAddressLabel         *widgets.QLabel
	MakerAddressLineEdit      *widgets.QLineEdit
	NextStateLabel            *widgets.QLabel
	NextStateLineEdit         *widgets.QLineEdit
	Frame                     *widgets.QFrame
	HorizontalLayoutWidget5   *widgets.QWidget
	HorizontalLayoutNextState *widgets.QHBoxLayout
	LabelIAm                  *widgets.QLabel
	PushButtonReview          *widgets.QPushButton
	PushButtonNextState       *widgets.QPushButton
	GridLayoutWidget          *widgets.QWidget
	GridLayout                *widgets.QGridLayout
	LabelTkr                  *widgets.QLabel
	LabelMkrRcv               *widgets.QLabel
	LabelTkrRcv               *widgets.QLabel
	LabelMkr                  *widgets.QLabel
	LabelNetwork              *widgets.QLabel
	LabelNetworkId            *widgets.QLabel
	Menubar                   *widgets.QMenuBar
	MenuSession               *widgets.QMenu
	MenuHelp                  *widgets.QMenu
	Statusbar                 *widgets.QStatusBar
	ActionCreate              *widgets.QAction
	ActionJoin                *widgets.QAction
	ActionOpen                *widgets.QAction
	ActionClose               *widgets.QAction
	ActionDelete              *widgets.QAction
	ActionOptions             *widgets.QAction
	ActionExit                *widgets.QAction
	ActionAbout               *widgets.QAction
}

func (this *UIMainwindowMainWindow) SetupUI(MainWindow *widgets.QMainWindow) {
	MainWindow.SetObjectName("MainWindow")
	MainWindow.SetGeometry(core.NewQRect4(0, 0, 722, 397))
	this.Centralwidget = widgets.NewQWidget(MainWindow, core.Qt__Widget)
	this.Centralwidget.SetObjectName("Centralwidget")
	this.FormLayoutWidget = widgets.NewQWidget(this.Centralwidget, core.Qt__Widget)
	this.FormLayoutWidget.SetObjectName("FormLayoutWidget")
	this.FormLayoutWidget.SetGeometry(core.NewQRect4(30, 30, 661, 169))
	this.FormLayout = widgets.NewQFormLayout(this.FormLayoutWidget)
	this.FormLayout.SetObjectName("formLayout")
	this.FormLayout.SetContentsMargins(3, 0, 3, 0)
	this.FormLayout.SetSpacing(0)
	this.SessionKeyLabel = widgets.NewQLabel(this.FormLayoutWidget, core.Qt__Widget)
	this.SessionKeyLabel.SetObjectName("SessionKeyLabel")
	this.FormLayout.SetWidget(0, widgets.QFormLayout__LabelRole, this.SessionKeyLabel)
	this.SessionKeyLineEdit = widgets.NewQLineEdit(this.FormLayoutWidget)
	this.SessionKeyLineEdit.SetObjectName("SessionKeyLineEdit")
	this.SessionKeyLineEdit.SetReadOnly(true)
	this.FormLayout.SetWidget(0, widgets.QFormLayout__FieldRole, this.SessionKeyLineEdit)
	this.SessionStateLabel = widgets.NewQLabel(this.FormLayoutWidget, core.Qt__Widget)
	this.SessionStateLabel.SetObjectName("SessionStateLabel")
	this.FormLayout.SetWidget(3, widgets.QFormLayout__LabelRole, this.SessionStateLabel)
	this.SessionStateLineEdit = widgets.NewQLineEdit(this.FormLayoutWidget)
	this.SessionStateLineEdit.SetObjectName("SessionStateLineEdit")
	this.SessionStateLineEdit.SetReadOnly(true)
	this.FormLayout.SetWidget(3, widgets.QFormLayout__FieldRole, this.SessionStateLineEdit)
	this.TakerAddressLabel = widgets.NewQLabel(this.FormLayoutWidget, core.Qt__Widget)
	this.TakerAddressLabel.SetObjectName("TakerAddressLabel")
	this.FormLayout.SetWidget(2, widgets.QFormLayout__LabelRole, this.TakerAddressLabel)
	this.TakerAddressLineEdit = widgets.NewQLineEdit(this.FormLayoutWidget)
	this.TakerAddressLineEdit.SetObjectName("TakerAddressLineEdit")
	this.TakerAddressLineEdit.SetReadOnly(true)
	this.FormLayout.SetWidget(2, widgets.QFormLayout__FieldRole, this.TakerAddressLineEdit)
	this.MakerAddressLabel = widgets.NewQLabel(this.FormLayoutWidget, core.Qt__Widget)
	this.MakerAddressLabel.SetObjectName("MakerAddressLabel")
	this.FormLayout.SetWidget(1, widgets.QFormLayout__LabelRole, this.MakerAddressLabel)
	this.MakerAddressLineEdit = widgets.NewQLineEdit(this.FormLayoutWidget)
	this.MakerAddressLineEdit.SetObjectName("MakerAddressLineEdit")
	this.MakerAddressLineEdit.SetReadOnly(true)
	this.FormLayout.SetWidget(1, widgets.QFormLayout__FieldRole, this.MakerAddressLineEdit)
	this.NextStateLabel = widgets.NewQLabel(this.FormLayoutWidget, core.Qt__Widget)
	this.NextStateLabel.SetObjectName("NextStateLabel")
	this.FormLayout.SetWidget(4, widgets.QFormLayout__LabelRole, this.NextStateLabel)
	this.NextStateLineEdit = widgets.NewQLineEdit(this.FormLayoutWidget)
	this.NextStateLineEdit.SetObjectName("NextStateLineEdit")
	this.NextStateLineEdit.SetReadOnly(true)
	this.FormLayout.SetWidget(4, widgets.QFormLayout__FieldRole, this.NextStateLineEdit)
	this.Frame = widgets.NewQFrame(this.Centralwidget, core.Qt__Widget)
	this.Frame.SetObjectName("Frame")
	this.Frame.SetGeometry(core.NewQRect4(20, 20, 681, 311))
	this.Frame.SetFrameShape(widgets.QFrame__StyledPanel)
	this.Frame.SetFrameShadow(widgets.QFrame__Raised)
	this.HorizontalLayoutWidget5 = widgets.NewQWidget(this.Frame, core.Qt__Widget)
	this.HorizontalLayoutWidget5.SetObjectName("HorizontalLayoutWidget5")
	this.HorizontalLayoutWidget5.SetGeometry(core.NewQRect4(10, 270, 661, 31))
	this.HorizontalLayoutNextState = widgets.NewQHBoxLayout2(this.HorizontalLayoutWidget5)
	this.HorizontalLayoutNextState.SetObjectName("horizontalLayout_next_state")
	this.HorizontalLayoutNextState.SetContentsMargins(3, 0, 3, 0)
	this.HorizontalLayoutNextState.SetSpacing(0)
	this.LabelIAm = widgets.NewQLabel(this.HorizontalLayoutWidget5, core.Qt__Widget)
	this.LabelIAm.SetObjectName("LabelIAm")
	this.HorizontalLayoutNextState.AddWidget(this.LabelIAm, 0, 0)
	this.PushButtonReview = widgets.NewQPushButton(this.HorizontalLayoutWidget5)
	this.PushButtonReview.SetObjectName("PushButtonReview")
	this.PushButtonReview.SetEnabled(true)
	this.HorizontalLayoutNextState.AddWidget(this.PushButtonReview, 0, 0)
	this.PushButtonNextState = widgets.NewQPushButton(this.HorizontalLayoutWidget5)
	this.PushButtonNextState.SetObjectName("PushButtonNextState")
	this.HorizontalLayoutNextState.AddWidget(this.PushButtonNextState, 0, 0)
	this.GridLayoutWidget = widgets.NewQWidget(this.Frame, core.Qt__Widget)
	this.GridLayoutWidget.SetObjectName("GridLayoutWidget")
	this.GridLayoutWidget.SetGeometry(core.NewQRect4(10, 180, 661, 81))
	this.GridLayout = widgets.NewQGridLayout(this.GridLayoutWidget)
	this.GridLayout.SetObjectName("gridLayout")
	this.GridLayout.SetContentsMargins(3, 0, 3, 0)
	this.GridLayout.SetSpacing(0)
	this.LabelTkr = widgets.NewQLabel(this.GridLayoutWidget, core.Qt__Widget)
	this.LabelTkr.SetObjectName("LabelTkr")
	this.GridLayout.AddWidget3(this.LabelTkr, 3, 0, 1, 1, 0)
	this.LabelMkrRcv = widgets.NewQLabel(this.GridLayoutWidget, core.Qt__Widget)
	this.LabelMkrRcv.SetObjectName("LabelMkrRcv")
	this.GridLayout.AddWidget3(this.LabelMkrRcv, 2, 1, 1, 1, 0)
	this.LabelTkrRcv = widgets.NewQLabel(this.GridLayoutWidget, core.Qt__Widget)
	this.LabelTkrRcv.SetObjectName("LabelTkrRcv")
	this.GridLayout.AddWidget3(this.LabelTkrRcv, 3, 1, 1, 1, 0)
	this.LabelMkr = widgets.NewQLabel(this.GridLayoutWidget, core.Qt__Widget)
	this.LabelMkr.SetObjectName("LabelMkr")
	this.GridLayout.AddWidget3(this.LabelMkr, 2, 0, 1, 1, 0)
	this.LabelNetwork = widgets.NewQLabel(this.GridLayoutWidget, core.Qt__Widget)
	this.LabelNetwork.SetObjectName("LabelNetwork")
	this.GridLayout.AddWidget3(this.LabelNetwork, 4, 0, 1, 1, 0)
	this.LabelNetworkId = widgets.NewQLabel(this.GridLayoutWidget, core.Qt__Widget)
	this.LabelNetworkId.SetObjectName("LabelNetworkId")
	this.GridLayout.AddWidget3(this.LabelNetworkId, 4, 1, 1, 1, 0)
	this.Frame.Raise()
	this.FormLayoutWidget.Raise()
	MainWindow.SetCentralWidget(this.Centralwidget)
	this.Menubar = widgets.NewQMenuBar(MainWindow)
	this.Menubar.SetObjectName("Menubar")
	this.Menubar.SetGeometry(core.NewQRect4(0, 0, 722, 25))
	this.MenuSession = widgets.NewQMenu(this.Menubar)
	this.MenuSession.SetObjectName("MenuSession")
	this.MenuHelp = widgets.NewQMenu(this.Menubar)
	this.MenuHelp.SetObjectName("MenuHelp")
	MainWindow.SetMenuBar(this.Menubar)
	this.Statusbar = widgets.NewQStatusBar(MainWindow)
	this.Statusbar.SetObjectName("Statusbar")
	MainWindow.SetStatusBar(this.Statusbar)
	this.ActionCreate = widgets.NewQAction(MainWindow)
	this.ActionCreate.SetObjectName("actionCreate")
	this.ActionJoin = widgets.NewQAction(MainWindow)
	this.ActionJoin.SetObjectName("actionJoin")
	this.ActionOpen = widgets.NewQAction(MainWindow)
	this.ActionOpen.SetObjectName("actionOpen")
	this.ActionClose = widgets.NewQAction(MainWindow)
	this.ActionClose.SetObjectName("actionClose")
	this.ActionClose.SetEnabled(false)
	this.ActionDelete = widgets.NewQAction(MainWindow)
	this.ActionDelete.SetObjectName("actionDelete")
	this.ActionDelete.SetEnabled(false)
	this.ActionOptions = widgets.NewQAction(MainWindow)
	this.ActionOptions.SetObjectName("actionOptions")
	this.ActionExit = widgets.NewQAction(MainWindow)
	this.ActionExit.SetObjectName("actionExit")
	this.ActionAbout = widgets.NewQAction(MainWindow)
	this.ActionAbout.SetObjectName("actionAbout")
	this.MenuSession.QWidget.AddAction(this.ActionCreate)
	this.MenuSession.QWidget.AddAction(this.ActionJoin)
	this.MenuSession.QWidget.AddAction(this.ActionOpen)
	this.MenuSession.QWidget.AddAction(this.ActionClose)
	this.MenuSession.QWidget.AddAction(this.ActionDelete)
	this.MenuSession.AddSeparator()
	this.MenuSession.QWidget.AddAction(this.ActionOptions)
	this.MenuSession.AddSeparator()
	this.MenuHelp.QWidget.AddAction(this.ActionAbout)
	this.Menubar.QWidget.AddAction(this.MenuSession.MenuAction())
	this.Menubar.QWidget.AddAction(this.MenuHelp.MenuAction())

	this.RetranslateUi(MainWindow)

}

func (this *UIMainwindowMainWindow) RetranslateUi(MainWindow *widgets.QMainWindow) {
	_translate := core.QCoreApplication_Translate
	MainWindow.SetWindowTitle(_translate("MainWindow", "Dragon", "", -1))
	this.SessionKeyLabel.SetText(_translate("MainWindow", "Session Key", "", -1))
	this.SessionStateLabel.SetText(_translate("MainWindow", "Session State:", "", -1))
	this.TakerAddressLabel.SetText(_translate("MainWindow", "Taker Address:", "", -1))
	this.MakerAddressLabel.SetText(_translate("MainWindow", "MakerAddress:", "", -1))
	this.NextStateLabel.SetText(_translate("MainWindow", "", "", -1))
	this.LabelIAm.SetText(_translate("MainWindow", "", "", -1))
	this.PushButtonReview.SetToolTip(_translate("MainWindow", "Review the swap session contracts and redeem transactions", "", -1))
	this.PushButtonReview.SetText(_translate("MainWindow", "", "", -1))
	this.PushButtonNextState.SetText(_translate("MainWindow", "", "", -1))
	this.LabelTkr.SetText(_translate("MainWindow", "", "", -1))
	this.LabelMkrRcv.SetText(_translate("MainWindow", "", "", -1))
	this.LabelTkrRcv.SetText(_translate("MainWindow", "", "", -1))
	this.LabelMkr.SetText(_translate("MainWindow", "", "", -1))
	this.LabelNetwork.SetText(_translate("MainWindow", "", "", -1))
	this.LabelNetworkId.SetText(_translate("MainWindow", "", "", -1))
	this.MenuSession.SetTitle(_translate("MainWindow", "Session", "", -1))
	this.MenuHelp.SetTitle(_translate("MainWindow", "Help", "", -1))
	this.ActionCreate.SetText(_translate("MainWindow", "Create", "", -1))
	this.ActionCreate.SetToolTip(_translate("MainWindow", "Create a New Swap Session", "", -1))
	this.ActionJoin.SetText(_translate("MainWindow", "Join", "", -1))
	this.ActionJoin.SetToolTip(_translate("MainWindow", "Join a newly created Swap Session", "", -1))
	this.ActionOpen.SetText(_translate("MainWindow", "Open", "", -1))
	this.ActionClose.SetText(_translate("MainWindow", "Close", "", -1))
	this.ActionDelete.SetText(_translate("MainWindow", "Delete", "", -1))
	this.ActionOptions.SetText(_translate("MainWindow", "Options", "", -1))
	this.ActionExit.SetText(_translate("MainWindow", "Exit", "", -1))
	this.ActionAbout.SetText(_translate("MainWindow", "About", "", -1))
}

// WARNING! All changes made in this file will be lost!
package gui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
)

type UIDataDialog struct {
	VerticalLayoutWidget *widgets.QWidget
	VerticalLayout *widgets.QVBoxLayout
	TextEditData *widgets.QTextEdit
}

func (this *UIDataDialog) SetupUI(DataDialog *widgets.QDialog) {
	DataDialog.SetObjectName("DataDialog")
	DataDialog.SetGeometry(core.NewQRect4(0, 0, 721, 538))
	this.VerticalLayoutWidget = widgets.NewQWidget(DataDialog, core.Qt__Widget)
	this.VerticalLayoutWidget.SetObjectName("VerticalLayoutWidget")
	this.VerticalLayoutWidget.SetGeometry(core.NewQRect4(9, 9, 701, 521))
	this.VerticalLayout = widgets.NewQVBoxLayout2(this.VerticalLayoutWidget)
	this.VerticalLayout.SetObjectName("verticalLayout")
	this.VerticalLayout.SetContentsMargins(0, 0, 0, 0)
	this.VerticalLayout.SetSpacing(0)
	this.TextEditData = widgets.NewQTextEdit(this.VerticalLayoutWidget)
	this.TextEditData.SetObjectName("TextEditData")
	this.VerticalLayout.AddWidget(this.TextEditData, 0, 0)


    this.RetranslateUi(DataDialog)

}

func (this *UIDataDialog) RetranslateUi(DataDialog *widgets.QDialog) {
    _translate := core.QCoreApplication_Translate
	DataDialog.SetWindowTitle(_translate("DataDialog", "Session Data", "", -1))
}

// WARNING! All changes made in this file will be lost!
package gui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
)

type UICreateDialog struct {
	ButtonBox *widgets.QDialogButtonBox
	HorizontalLayoutWidget *widgets.QWidget
	HorizontalLayout *widgets.QHBoxLayout
	Label *widgets.QLabel
	HorizontalLayoutWidget2 *widgets.QWidget
	HorizontalLayout2 *widgets.QHBoxLayout
	DoubleSpinBoxSend *widgets.QDoubleSpinBox
	HorizontalSpacer *widgets.QSpacerItem
	Label2 *widgets.QLabel
	ComboBoxSend *widgets.QComboBox
	HorizontalLayoutWidget3 *widgets.QWidget
	HorizontalLayout3 *widgets.QHBoxLayout
	Label3 *widgets.QLabel
	HorizontalLayoutWidget4 *widgets.QWidget
	HorizontalLayout4 *widgets.QHBoxLayout
	DoubleSpinBoxReceive *widgets.QDoubleSpinBox
	HorizontalSpacer2 *widgets.QSpacerItem
	Label4 *widgets.QLabel
	ComboBoxReceive *widgets.QComboBox
	HorizontalLayoutWidget5 *widgets.QWidget
	HorizontalLayout5 *widgets.QHBoxLayout
	HorizontalSpacer3 *widgets.QSpacerItem
	CheckBoxTestnet *widgets.QCheckBox
	VerticalLayoutWidget *widgets.QWidget
	VerticalLayout *widgets.QVBoxLayout
	Label6 *widgets.QLabel
	Label7 *widgets.QLabel
}

func (this *UICreateDialog) SetupUI(CreateDialog *widgets.QDialog) {
	CreateDialog.SetObjectName("CreateDialog")
	CreateDialog.SetGeometry(core.NewQRect4(0, 0, 495, 306))
	this.ButtonBox = widgets.NewQDialogButtonBox(CreateDialog)
	this.ButtonBox.SetObjectName("ButtonBox")
	this.ButtonBox.SetGeometry(core.NewQRect4(210, 270, 161, 21))
	this.ButtonBox.SetOrientation(core.Qt__Horizontal)
	this.ButtonBox.SetStandardButtons(widgets.QDialogButtonBox__Cancel | widgets.QDialogButtonBox__Ok)
	this.HorizontalLayoutWidget = widgets.NewQWidget(CreateDialog, core.Qt__Widget)
	this.HorizontalLayoutWidget.SetObjectName("HorizontalLayoutWidget")
	this.HorizontalLayoutWidget.SetGeometry(core.NewQRect4(10, 10, 321, 21))
	this.HorizontalLayout = widgets.NewQHBoxLayout2(this.HorizontalLayoutWidget)
	this.HorizontalLayout.SetObjectName("horizontalLayout")
	this.HorizontalLayout.SetContentsMargins(0, 0, 0, 0)
	this.HorizontalLayout.SetSpacing(0)
	this.Label = widgets.NewQLabel(this.HorizontalLayoutWidget, core.Qt__Widget)
	this.Label.SetObjectName("Label")
	this.HorizontalLayout.AddWidget(this.Label, 0, 0)
	this.HorizontalLayoutWidget2 = widgets.NewQWidget(CreateDialog, core.Qt__Widget)
	this.HorizontalLayoutWidget2.SetObjectName("HorizontalLayoutWidget2")
	this.HorizontalLayoutWidget2.SetGeometry(core.NewQRect4(10, 39, 471, 31))
	this.HorizontalLayout2 = widgets.NewQHBoxLayout2(this.HorizontalLayoutWidget2)
	this.HorizontalLayout2.SetObjectName("horizontalLayout_2")
	this.HorizontalLayout2.SetContentsMargins(0, 0, 0, 0)
	this.HorizontalLayout2.SetSpacing(0)
	this.DoubleSpinBoxSend = widgets.NewQDoubleSpinBox(this.HorizontalLayoutWidget2)
	this.DoubleSpinBoxSend.SetObjectName("DoubleSpinBoxSend")
	this.DoubleSpinBoxSend.SetDecimals(8)
	this.DoubleSpinBoxSend.SetMinimum(0.010000)
	this.DoubleSpinBoxSend.SetMaximum(999999999.990000)
	this.HorizontalLayout2.AddWidget(this.DoubleSpinBoxSend, 0, 0)
	this.HorizontalSpacer = widgets.NewQSpacerItem(40, 20, widgets.QSizePolicy__Expanding, widgets.QSizePolicy__Minimum)
	this.HorizontalLayout2.AddItem(this.HorizontalSpacer)
	this.Label2 = widgets.NewQLabel(this.HorizontalLayoutWidget2, core.Qt__Widget)
	this.Label2.SetObjectName("Label2")
	this.HorizontalLayout2.AddWidget(this.Label2, 0, 0)
	this.ComboBoxSend = widgets.NewQComboBox(this.HorizontalLayoutWidget2)
	this.ComboBoxSend.SetObjectName("ComboBoxSend")
	this.HorizontalLayout2.AddWidget(this.ComboBoxSend, 0, 0)
	this.HorizontalLayoutWidget3 = widgets.NewQWidget(CreateDialog, core.Qt__Widget)
	this.HorizontalLayoutWidget3.SetObjectName("HorizontalLayoutWidget3")
	this.HorizontalLayoutWidget3.SetGeometry(core.NewQRect4(10, 90, 321, 21))
	this.HorizontalLayout3 = widgets.NewQHBoxLayout2(this.HorizontalLayoutWidget3)
	this.HorizontalLayout3.SetObjectName("horizontalLayout_3")
	this.HorizontalLayout3.SetContentsMargins(0, 0, 0, 0)
	this.HorizontalLayout3.SetSpacing(0)
	this.Label3 = widgets.NewQLabel(this.HorizontalLayoutWidget3, core.Qt__Widget)
	this.Label3.SetObjectName("Label3")
	this.HorizontalLayout3.AddWidget(this.Label3, 0, 0)
	this.HorizontalLayoutWidget4 = widgets.NewQWidget(CreateDialog, core.Qt__Widget)
	this.HorizontalLayoutWidget4.SetObjectName("HorizontalLayoutWidget4")
	this.HorizontalLayoutWidget4.SetGeometry(core.NewQRect4(10, 120, 471, 31))
	this.HorizontalLayout4 = widgets.NewQHBoxLayout2(this.HorizontalLayoutWidget4)
	this.HorizontalLayout4.SetObjectName("horizontalLayout_4")
	this.HorizontalLayout4.SetContentsMargins(0, 0, 0, 0)
	this.HorizontalLayout4.SetSpacing(0)
	this.DoubleSpinBoxReceive = widgets.NewQDoubleSpinBox(this.HorizontalLayoutWidget4)
	this.DoubleSpinBoxReceive.SetObjectName("DoubleSpinBoxReceive")
	this.DoubleSpinBoxReceive.SetDecimals(8)
	this.DoubleSpinBoxReceive.SetMinimum(0.010000)
	this.DoubleSpinBoxReceive.SetMaximum(999999999.990000)
	this.HorizontalLayout4.AddWidget(this.DoubleSpinBoxReceive, 0, 0)
	this.HorizontalSpacer2 = widgets.NewQSpacerItem(40, 20, widgets.QSizePolicy__Expanding, widgets.QSizePolicy__Minimum)
	this.HorizontalLayout4.AddItem(this.HorizontalSpacer2)
	this.Label4 = widgets.NewQLabel(this.HorizontalLayoutWidget4, core.Qt__Widget)
	this.Label4.SetObjectName("Label4")
	this.HorizontalLayout4.AddWidget(this.Label4, 0, 0)
	this.ComboBoxReceive = widgets.NewQComboBox(this.HorizontalLayoutWidget4)
	this.ComboBoxReceive.SetObjectName("ComboBoxReceive")
	this.HorizontalLayout4.AddWidget(this.ComboBoxReceive, 0, 0)
	this.HorizontalLayoutWidget5 = widgets.NewQWidget(CreateDialog, core.Qt__Widget)
	this.HorizontalLayoutWidget5.SetObjectName("HorizontalLayoutWidget5")
	this.HorizontalLayoutWidget5.SetGeometry(core.NewQRect4(10, 170, 471, 31))
	this.HorizontalLayout5 = widgets.NewQHBoxLayout2(this.HorizontalLayoutWidget5)
	this.HorizontalLayout5.SetObjectName("horizontalLayout_5")
	this.HorizontalLayout5.SetContentsMargins(0, 0, 0, 0)
	this.HorizontalLayout5.SetSpacing(0)
	this.HorizontalSpacer3 = widgets.NewQSpacerItem(40, 20, widgets.QSizePolicy__Expanding, widgets.QSizePolicy__Minimum)
	this.HorizontalLayout5.AddItem(this.HorizontalSpacer3)
	this.CheckBoxTestnet = widgets.NewQCheckBox(this.HorizontalLayoutWidget5)
	this.CheckBoxTestnet.SetObjectName("CheckBoxTestnet")
	this.HorizontalLayout5.AddWidget(this.CheckBoxTestnet, 0, 0)
	this.VerticalLayoutWidget = widgets.NewQWidget(CreateDialog, core.Qt__Widget)
	this.VerticalLayoutWidget.SetObjectName("VerticalLayoutWidget")
	this.VerticalLayoutWidget.SetGeometry(core.NewQRect4(10, 210, 473, 42))
	this.VerticalLayout = widgets.NewQVBoxLayout2(this.VerticalLayoutWidget)
	this.VerticalLayout.SetObjectName("verticalLayout")
	this.VerticalLayout.SetContentsMargins(0, 0, 0, 0)
	this.VerticalLayout.SetSpacing(0)
	this.Label6 = widgets.NewQLabel(this.VerticalLayoutWidget, core.Qt__Widget)
	this.Label6.SetObjectName("Label6")
	this.VerticalLayout.AddWidget(this.Label6, 0, 0)
	this.Label7 = widgets.NewQLabel(this.VerticalLayoutWidget, core.Qt__Widget)
	this.Label7.SetObjectName("Label7")
	this.VerticalLayout.AddWidget(this.Label7, 0, 0)


    this.RetranslateUi(CreateDialog)

}

func (this *UICreateDialog) RetranslateUi(CreateDialog *widgets.QDialog) {
    _translate := core.QCoreApplication_Translate
	CreateDialog.SetWindowTitle(_translate("CreateDialog", "Create New Session", "", -1))
	this.Label.SetText(_translate("CreateDialog", "I want to send:", "", -1))
	this.Label2.SetText(_translate("CreateDialog", "of my", "", -1))
	this.ComboBoxSend.SetToolTip(_translate("CreateDialog", "<html><head/><body><p>Available wallets</p></body></html>", "", -1))
	this.Label3.SetText(_translate("CreateDialog", "I want to receive:", "", -1))
	this.Label4.SetText(_translate("CreateDialog", "of their", "", -1))
	this.ComboBoxReceive.SetToolTip(_translate("CreateDialog", "<html><head/><body><p>Taker will send</p></body></html>", "", -1))
	this.CheckBoxTestnet.SetToolTip(_translate("CreateDialog", "<html><head/><body><p>Show testnet wallets</p></body></html>", "", -1))
	this.CheckBoxTestnet.SetText(_translate("CreateDialog", "testnet", "", -1))
	this.Label6.SetText(_translate("CreateDialog", "Clicking 'OK' will create a new shared session.", "", -1))
	this.Label7.SetText(_translate("CreateDialog", "You should share the session key with the other party so they can join", "", -1))
}

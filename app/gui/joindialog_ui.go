// WARNING! All changes made in this file will be lost!
package gui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
)

type UIJoinDialog struct {
	ButtonBox *widgets.QDialogButtonBox
	VerticalLayoutWidget *widgets.QWidget
	VerticalLayout *widgets.QVBoxLayout
	Label *widgets.QLabel
	LineEditSession *widgets.QLineEdit
}

func (this *UIJoinDialog) SetupUI(JoinDialog *widgets.QDialog) {
	JoinDialog.SetObjectName("JoinDialog")
	JoinDialog.SetGeometry(core.NewQRect4(0, 0, 400, 139))
	this.ButtonBox = widgets.NewQDialogButtonBox(JoinDialog)
	this.ButtonBox.SetObjectName("ButtonBox")
	this.ButtonBox.SetGeometry(core.NewQRect4(30, 90, 341, 32))
	this.ButtonBox.SetOrientation(core.Qt__Horizontal)
	this.ButtonBox.SetStandardButtons(widgets.QDialogButtonBox__Cancel | widgets.QDialogButtonBox__Ok)
	this.VerticalLayoutWidget = widgets.NewQWidget(JoinDialog, core.Qt__Widget)
	this.VerticalLayoutWidget.SetObjectName("VerticalLayoutWidget")
	this.VerticalLayoutWidget.SetGeometry(core.NewQRect4(10, 10, 381, 61))
	this.VerticalLayout = widgets.NewQVBoxLayout2(this.VerticalLayoutWidget)
	this.VerticalLayout.SetObjectName("verticalLayout")
	this.VerticalLayout.SetContentsMargins(0, 0, 0, 0)
	this.VerticalLayout.SetSpacing(0)
	this.Label = widgets.NewQLabel(this.VerticalLayoutWidget, core.Qt__Widget)
	this.Label.SetObjectName("Label")
	this.VerticalLayout.AddWidget(this.Label, 0, 0)
	this.LineEditSession = widgets.NewQLineEdit(this.VerticalLayoutWidget)
	this.LineEditSession.SetObjectName("LineEditSession")
	this.VerticalLayout.AddWidget(this.LineEditSession, 0, 0)


    this.RetranslateUi(JoinDialog)

	this.ButtonBox.ConnectAccepted(JoinDialog.Accept)
	this.ButtonBox.ConnectRejected(JoinDialog.Reject)
}

func (this *UIJoinDialog) RetranslateUi(JoinDialog *widgets.QDialog) {
    _translate := core.QCoreApplication_Translate
	JoinDialog.SetWindowTitle(_translate("JoinDialog", "Join a Session", "", -1))
	this.Label.SetText(_translate("JoinDialog", "Session Key:", "", -1))
}

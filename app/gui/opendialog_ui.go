// WARNING! All changes made in this file will be lost!
package gui

import (
	"github.com/therecipe/qt/widgets"
	"github.com/therecipe/qt/core"
)

type UIOpenDialog struct {
	ButtonBox *widgets.QDialogButtonBox
	VerticalLayoutWidget *widgets.QWidget
	VerticalLayout *widgets.QVBoxLayout
	LabelSavedSessions *widgets.QLabel
	ComboBoxSavedSessions *widgets.QComboBox
}

func (this *UIOpenDialog) SetupUI(OpenDialog *widgets.QDialog) {
	OpenDialog.SetObjectName("OpenDialog")
	OpenDialog.SetGeometry(core.NewQRect4(0, 0, 400, 140))
	this.ButtonBox = widgets.NewQDialogButtonBox(OpenDialog)
	this.ButtonBox.SetObjectName("ButtonBox")
	this.ButtonBox.SetGeometry(core.NewQRect4(30, 90, 341, 32))
	this.ButtonBox.SetOrientation(core.Qt__Horizontal)
	this.ButtonBox.SetStandardButtons(widgets.QDialogButtonBox__Cancel | widgets.QDialogButtonBox__Ok)
	this.VerticalLayoutWidget = widgets.NewQWidget(OpenDialog, core.Qt__Widget)
	this.VerticalLayoutWidget.SetObjectName("VerticalLayoutWidget")
	this.VerticalLayoutWidget.SetGeometry(core.NewQRect4(10, 10, 381, 61))
	this.VerticalLayout = widgets.NewQVBoxLayout2(this.VerticalLayoutWidget)
	this.VerticalLayout.SetObjectName("verticalLayout")
	this.VerticalLayout.SetContentsMargins(0, 0, 0, 0)
	this.VerticalLayout.SetSpacing(0)
	this.LabelSavedSessions = widgets.NewQLabel(this.VerticalLayoutWidget, core.Qt__Widget)
	this.LabelSavedSessions.SetObjectName("LabelSavedSessions")
	this.VerticalLayout.AddWidget(this.LabelSavedSessions, 0, 0)
	this.ComboBoxSavedSessions = widgets.NewQComboBox(this.VerticalLayoutWidget)
	this.ComboBoxSavedSessions.SetObjectName("ComboBoxSavedSessions")
	this.VerticalLayout.AddWidget(this.ComboBoxSavedSessions, 0, 0)


    this.RetranslateUi(OpenDialog)

	this.ButtonBox.ConnectAccepted(OpenDialog.Accept)
	this.ButtonBox.ConnectRejected(OpenDialog.Reject)
}

func (this *UIOpenDialog) RetranslateUi(OpenDialog *widgets.QDialog) {
    _translate := core.QCoreApplication_Translate
	OpenDialog.SetWindowTitle(_translate("OpenDialog", "Open Existing Session", "", -1))
	this.ButtonBox.SetWhatsThis(_translate("OpenDialog", "fred", "", -1))
	this.LabelSavedSessions.SetText(_translate("OpenDialog", "Saved Sessions - keys:", "", -1))
	this.ComboBoxSavedSessions.SetWhatsThis(_translate("OpenDialog", "", "", -1))
}

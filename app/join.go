package app

import (
	"fmt"

	"gitlab.com/devwarrior/dragon/logger"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/devwarrior/dragon/app/gui"
)

/////////////////
// Join dialog //
/////////////////

// Join dialog
type Join struct {
	app    *AppMain
	dialog *widgets.QDialog
	ui     *gui.UIJoinDialog
}

// NewJoin constructs a Join dialog object
func newJoin(appMain *AppMain) *Join {
	join := Join{app: appMain}
	join.dialog = widgets.NewQDialog(appMain.window, core.Qt__Dialog)
	join.ui = &gui.UIJoinDialog{}
	return &join
}

// Setup continues construction of the join dialog
func (j *Join) setup() *Join {
	j.ui.SetupUI(j.dialog)
	j.ui.ButtonBox.ConnectAccepted(j.onButtonBoxAccepted)
	j.ui.ButtonBox.ConnectRejected(j.onButtonBoxRejected)
	j.setDefaults()
	j.dialog.SetModal(true)
	return j
}

func (j *Join) center() {
	d := j.dialog
	w := j.app.window
	d.Move2(w.X()+((w.Width()-d.Width())/2), w.Y()+((w.Height()-d.Height())/2))
}

func (j *Join) showDialog() {
	j.dialog.Show()
	j.center()
}

func (j *Join) setDefaults() {
	j.ui.LineEditSession.SetText("")
}

// get dialog selection
func (j *Join) getSessionID() string {
	return j.ui.LineEditSession.Text()
}

func (j *Join) onButtonBoxAccepted() {
	logger.Log.Println("accepted")

	// get dialog value entered for session id
	sessionID := j.getSessionID()
	//...join a newly created session
	s, err := newParticipantSession(j.app, sessionID)
	if err != nil {
		msg := fmt.Sprintf("dbgMsg %v", err)
		widgets.QMessageBox_Warning(nil, "Join session", msg, widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	} else {
		j.app.session = s
	}
	j.setDefaults()
	j.dialog.SetVisible(false)
}

func (j *Join) onButtonBoxRejected() {
	logger.Log.Println("rejected")
	j.setDefaults()
	j.dialog.SetVisible(false)
}

package app

import (
	"errors"

	"gitlab.com/devwarrior/dragon/logger"

	"gitlab.com/devwarrior/dragon/app/local"
	"gitlab.com/devwarrior/dragon/app/wallets"
	pb "gitlab.com/devwarrior/dragon/session"
)

// Session role from the pov of the local gui user
type role int

const (
	initiator   role = role(local.Initiator)
	participant role = role(local.Participant)
)

// Session from the p.o.v. of the local gui user
type session struct {
	app        *AppMain
	role       role
	sessionID  string
	testnet    bool
	rcvAddress string
	rcvWallet  wallets.Wallet
	sndWallet  wallets.Wallet
	poller     *statePoller
}

// createParams are parameters for new initiator swap session creation
type createParams struct {
	testnet      bool
	coinRcv      string
	coinRcvValue float64
	coinSnd      string
	coinSndValue float64
}

//////////////////////////
// session constructors //
//////////////////////////

// newInitiatorSession creates a new swap session
func newInitiatorSession(app *AppMain, params *createParams) (*session, error) {
	err := dbPing()
	if err != nil {
		return nil, errors.New("DB Server Unavailable - See log for details")
	}

	// New session object
	s := &session{}
	s.app = app
	s.role = initiator
	s.sessionID = getRand32()
	s.testnet = params.testnet

	// make initiator Wallets
	s.rcvWallet, err = walletForCoin(params.testnet, params.coinRcv)
	if err != nil {
		return nil, err
	}
	s.sndWallet, err = walletForCoin(params.testnet, params.coinSnd)
	if err != nil {
		return nil, err
	}

	// check atomicswap (rpc) available for both coins
	err = s.rcvWallet.PingRPC()
	if err != nil {
		return nil, err
	}
	err = s.sndWallet.PingRPC()
	if err != nil {
		return nil, err
	}

	// get a new receive address
	s.rcvAddress, err = s.rcvWallet.GetNewAddress()
	if err != nil {
		return nil, err
	}
	logger.Log.Printf("new initiator rcvAddress: %s\n", s.rcvAddress)

	// prepare initiator receive coin values for database
	initCoinRcv := dbcoin(params.coinRcv)
	initCoinRcvValueAmount, err := NewAmount(params.coinRcvValue)
	if err != nil {
		return nil, err
	}
	initCoinRcvValue := int64(initCoinRcvValueAmount)
	logger.Log.Printf("initCoinRcv: %d (%s)\n", initCoinRcv, params.coinRcv)
	logger.Log.Printf("initCoinRcvValue: %d\n", initCoinRcvValue)

	// prepare participant receive coin values for database
	partCoinRcv := dbcoin(params.coinSnd)
	partCoinRcvValueAmount, err := NewAmount(params.coinSndValue)
	if err != nil {
		return nil, err
	}
	partCoinRcvValue := int64(partCoinRcvValueAmount)
	logger.Log.Printf("partCoinRcv: %d (%s)\n", partCoinRcv, params.coinSnd)
	logger.Log.Printf("partCoinRcvValue: %d\n", partCoinRcvValue)

	// create local.session as initiator
	secret := getRand32()
	secrethash, err := hash256(secret)
	if err != nil {
		return nil, err
	}
	err = local.AppendNewInitiatorSession(s.sessionID, secret, params.coinSnd, s.testnet)
	if err != nil {
		return nil, err
	}

	// call db to create session
	request := &pb.NewSessionRequest{}
	request.Session = s.sessionID
	request.Network = dbnetwork(s.testnet)
	request.Initiator = s.rcvAddress
	request.Secrethash = secrethash
	request.InitCoinRcv = initCoinRcv
	request.InitCoinRcvValue = initCoinRcvValue
	request.PartCoinRcv = partCoinRcv
	request.PartCoinRcvValue = partCoinRcvValue
	response, err := dbNewSession(request)
	if err != nil {
		local.RemoveSession(s.sessionID)
		return nil, err
	}
	logger.Log.Printf("created new session in database: %s\n", response.Data.Session)

	// create client statePoller
	s.poller = newStatePoller(s)
	s.poller.startPoll()

	// poller updates gui
	s.poller.checkPoll()

	return s, nil
}

// newParticipantSession is a session for a joining participant
func newParticipantSession(app *AppMain, sessionID string) (*session, error) {
	err := dbPing()
	if err != nil {
		return nil, errors.New("DB Server Unavailable - See log for details")
	}

	// check local.session not exist already for either role - this is an error
	//  - cannot join if you are also the initiator
	//  - cannot join twice as participant
	if local.SessionExists(sessionID) {
		return nil, errors.New("already connected to this session")
	}

	// New session object
	s := &session{}
	s.app = app
	s.role = participant
	s.sessionID = sessionID

	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		return nil, err
	}

	s.testnet = istestnet(dataResponse.Data.Network)

	coinSnd := coinname(dataResponse.Data.InitCoinRcv)
	coinRcv := coinname(dataResponse.Data.PartCoinRcv)

	// make Wallets for participant
	s.rcvWallet, err = walletForCoin(s.testnet, coinRcv)
	if err != nil {
		return nil, err
	}
	s.sndWallet, err = walletForCoin(s.testnet, coinSnd)
	if err != nil {
		return nil, err
	}

	// check atomicswap (rpc) available for both coins
	err = s.rcvWallet.PingRPC()
	if err != nil {
		return nil, err
	}
	err = s.sndWallet.PingRPC()
	if err != nil {
		return nil, err
	}

	// get a new receive address
	s.rcvAddress, err = s.rcvWallet.GetNewAddress()
	if err != nil {
		return nil, err
	}
	logger.Log.Printf("new participant rcvAddress: %s\n", s.rcvAddress)

	// create local.session as participant
	err = local.AppendNewParticipantSession(s.sessionID, coinSnd, s.testnet)
	if err != nil {
		return nil, err
	}

	// call db to join session
	request := &pb.AddParticipantRequest{Session: s.sessionID}
	request.Participant = s.rcvAddress
	response, err := dbAddParticipant(request)
	if err != nil {
		local.RemoveSession(s.sessionID)
		return nil, err
	}
	logger.Log.Printf("participant joined session: %s\n", response.Data.Session)

	// create client statePoller
	s.poller = newStatePoller(s)
	s.poller.startPoll()

	// poller updates gui
	s.poller.checkPoll()

	return s, nil
}

// newReopenedSession opens a previously saved session
func newReopenedSession(app *AppMain, sessionID string) (*session, error) {
	localSession := local.GetSession(sessionID)
	if localSession == nil {
		return nil, errors.New("no such session")
	}
	err := dbPing()
	if err != nil {
		return nil, errors.New("DB Server Unavailable - See log for details")
	}

	// New session object
	s := &session{}
	s.app = app

	// get details from local.session record ...
	s.sessionID = localSession.SessionID
	s.role = role(localSession.Role)

	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		return nil, err
	}

	if s.role == initiator {
		s.rcvAddress = dataResponse.Data.Initiator
	} else {
		s.rcvAddress = dataResponse.Data.Participant
	}

	s.testnet = istestnet(dataResponse.Data.Network)

	initCoinRcv := coinname(dataResponse.Data.InitCoinRcv)
	partCoinRcv := coinname(dataResponse.Data.PartCoinRcv)

	// make Wallets for local role
	var coinRcv, coinSnd string
	if s.role == initiator {
		coinRcv = initCoinRcv
		coinSnd = partCoinRcv
	} else {
		coinRcv = partCoinRcv
		coinSnd = initCoinRcv
	}
	s.rcvWallet, err = walletForCoin(s.testnet, coinRcv)
	if err != nil {
		return nil, err
	}
	s.sndWallet, err = walletForCoin(s.testnet, coinSnd)
	if err != nil {
		return nil, err
	}

	// check atomicswap (rpc) available for both coins
	err = s.rcvWallet.PingRPC()
	if err != nil {
		return nil, err
	}
	err = s.sndWallet.PingRPC()
	if err != nil {
		return nil, err
	}

	// create client statePoller
	s.poller = newStatePoller(s)
	s.poller.startPoll()

	// poller updates gui
	s.poller.checkPoll()

	return s, nil
}

///////////////////////////
// Session 'destructors' //
///////////////////////////

// endSession is called when We 'close' this side of the session
// It is also called always on app exit whether session alive or not
// hence we also check if the poller is alive
func (s *session) endSession() {
	logger.Log.Println("endSession")
	if s.poller != nil {
		logger.Log.Printf("endSession - stopping poller")
		s.poller.endPoll()
	}
}

// endSessionAndMarkDeleted is called when We 'delete' the session.
// The session is never truly deleted by local client calling
// delete on the session db. Database maintenance will do that.
func (s *session) endSessionAndMarkDeleted() {
	logger.Log.Println("endSessionAndMarkDeleted")
	// _first_ stop poller!!!
	s.poller.endPoll()
	// mark session as marked-deleted
	request := &pb.MarkDeletedRequest{Session: s.sessionID}
	_, err := dbMarkDeleted(request)
	if err != nil {
		logger.Log.Printf("endSessionAndMarkDeleted - could not mark session %s as deleted\n", s.sessionID)
	}
	// invalidate the local.session (removed at next app startup)
	local.SetInvalid(s.sessionID)
}

// endSessionAndInvalidateLocal is called when counterparty 'delete's' the session
// and the poller picks up on this.
func (s *session) endSessionAndInvalidateLocal() {
	logger.Log.Println("endSessionAndInvalidateLocal")
	// _first_ stop poller!!!
	s.poller.endPoll()
	// invalidate the local.session (removed at next app startup)
	local.SetInvalid(s.sessionID)
	// tell user
	infoMsg("session deleted", "counterparty deleted the session")
}

// endSessionCompleteInvalidateLocal is called when we 'remove' the session after
// completion. Only called from complete state.
func (s *session) endSessionCompleteInvalidateLocal() {
	logger.Log.Println("endSessionCompleteInvalidateLocal")
	// _first_ stop poller!!!
	s.poller.endPoll()
	// invalidate the local.session (removed at next app startup)
	local.SetInvalid(s.sessionID)
	// tell user
	infoMsg("session removed", "the session was removed")
}

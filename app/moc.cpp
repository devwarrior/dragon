

#define protected public
#define private public

#include "moc.h"
#include "_cgo_export.h"

#include <QByteArray>
#include <QCameraImageCapture>
#include <QChildEvent>
#include <QDBusPendingCallWatcher>
#include <QEvent>
#include <QExtensionFactory>
#include <QExtensionManager>
#include <QGraphicsObject>
#include <QGraphicsWidget>
#include <QLayout>
#include <QMediaPlaylist>
#include <QMediaRecorder>
#include <QMetaMethod>
#include <QObject>
#include <QOffscreenSurface>
#include <QPaintDeviceWindow>
#include <QPdfWriter>
#include <QQuickItem>
#include <QRadioData>
#include <QString>
#include <QTimerEvent>
#include <QWidget>
#include <QWindow>


class updater091c1b: public QObject
{
Q_OBJECT
public:
	updater091c1b(QObject *parent = Q_NULLPTR) : QObject(parent) {qRegisterMetaType<quintptr>("quintptr");updater091c1b_updater091c1b_QRegisterMetaType();updater091c1b_updater091c1b_QRegisterMetaTypes();callbackupdater091c1b_Constructor(this);};
	void Signal_UpdateState(qint32 v0) { callbackupdater091c1b_UpdateState(this, v0); };
	 ~updater091c1b() { callbackupdater091c1b_DestroyUpdater(this); };
	bool event(QEvent * e) { return callbackupdater091c1b_Event(this, e) != 0; };
	bool eventFilter(QObject * watched, QEvent * event) { return callbackupdater091c1b_EventFilter(this, watched, event) != 0; };
	void childEvent(QChildEvent * event) { callbackupdater091c1b_ChildEvent(this, event); };
	void connectNotify(const QMetaMethod & sign) { callbackupdater091c1b_ConnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void customEvent(QEvent * event) { callbackupdater091c1b_CustomEvent(this, event); };
	void deleteLater() { callbackupdater091c1b_DeleteLater(this); };
	void Signal_Destroyed(QObject * obj) { callbackupdater091c1b_Destroyed(this, obj); };
	void disconnectNotify(const QMetaMethod & sign) { callbackupdater091c1b_DisconnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void Signal_ObjectNameChanged(const QString & objectName) { QByteArray taa2c4f = objectName.toUtf8(); Moc_PackedString objectNamePacked = { const_cast<char*>(taa2c4f.prepend("WHITESPACE").constData()+10), taa2c4f.size()-10 };callbackupdater091c1b_ObjectNameChanged(this, objectNamePacked); };
	void timerEvent(QTimerEvent * event) { callbackupdater091c1b_TimerEvent(this, event); };
signals:
	void updateState(qint32 v0);
public slots:
private:
};

Q_DECLARE_METATYPE(updater091c1b*)


void updater091c1b_updater091c1b_QRegisterMetaTypes() {
}

void updater091c1b_ConnectUpdateState(void* ptr)
{
	QObject::connect(static_cast<updater091c1b*>(ptr), static_cast<void (updater091c1b::*)(qint32)>(&updater091c1b::updateState), static_cast<updater091c1b*>(ptr), static_cast<void (updater091c1b::*)(qint32)>(&updater091c1b::Signal_UpdateState));
}

void updater091c1b_DisconnectUpdateState(void* ptr)
{
	QObject::disconnect(static_cast<updater091c1b*>(ptr), static_cast<void (updater091c1b::*)(qint32)>(&updater091c1b::updateState), static_cast<updater091c1b*>(ptr), static_cast<void (updater091c1b::*)(qint32)>(&updater091c1b::Signal_UpdateState));
}

void updater091c1b_UpdateState(void* ptr, int v0)
{
	static_cast<updater091c1b*>(ptr)->updateState(v0);
}

int updater091c1b_updater091c1b_QRegisterMetaType()
{
	return qRegisterMetaType<updater091c1b*>();
}

int updater091c1b_updater091c1b_QRegisterMetaType2(char* typeName)
{
	return qRegisterMetaType<updater091c1b*>(const_cast<const char*>(typeName));
}

int updater091c1b_updater091c1b_QmlRegisterType()
{
#ifdef QT_QML_LIB
	return qmlRegisterType<updater091c1b>();
#else
	return 0;
#endif
}

int updater091c1b_updater091c1b_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName)
{
#ifdef QT_QML_LIB
	return qmlRegisterType<updater091c1b>(const_cast<const char*>(uri), versionMajor, versionMinor, const_cast<const char*>(qmlName));
#else
	return 0;
#endif
}

void* updater091c1b___dynamicPropertyNames_atList(void* ptr, int i)
{
	return new QByteArray(({QByteArray tmp = static_cast<QList<QByteArray>*>(ptr)->at(i); if (i == static_cast<QList<QByteArray>*>(ptr)->size()-1) { static_cast<QList<QByteArray>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void updater091c1b___dynamicPropertyNames_setList(void* ptr, void* i)
{
	static_cast<QList<QByteArray>*>(ptr)->append(*static_cast<QByteArray*>(i));
}

void* updater091c1b___dynamicPropertyNames_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QByteArray>();
}

void* updater091c1b___findChildren_atList2(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void updater091c1b___findChildren_setList2(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* updater091c1b___findChildren_newList2(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* updater091c1b___findChildren_atList3(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void updater091c1b___findChildren_setList3(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* updater091c1b___findChildren_newList3(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* updater091c1b___findChildren_atList(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void updater091c1b___findChildren_setList(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* updater091c1b___findChildren_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* updater091c1b___children_atList(void* ptr, int i)
{
	return ({QObject * tmp = static_cast<QList<QObject *>*>(ptr)->at(i); if (i == static_cast<QList<QObject *>*>(ptr)->size()-1) { static_cast<QList<QObject *>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void updater091c1b___children_setList(void* ptr, void* i)
{
	static_cast<QList<QObject *>*>(ptr)->append(static_cast<QObject*>(i));
}

void* updater091c1b___children_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject *>();
}

void* updater091c1b_NewUpdater(void* parent)
{
	if (dynamic_cast<QCameraImageCapture*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QCameraImageCapture*>(parent));
	} else if (dynamic_cast<QDBusPendingCallWatcher*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QDBusPendingCallWatcher*>(parent));
	} else if (dynamic_cast<QExtensionFactory*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QExtensionFactory*>(parent));
	} else if (dynamic_cast<QExtensionManager*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QExtensionManager*>(parent));
	} else if (dynamic_cast<QGraphicsObject*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QGraphicsObject*>(parent));
	} else if (dynamic_cast<QGraphicsWidget*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QGraphicsWidget*>(parent));
	} else if (dynamic_cast<QLayout*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QLayout*>(parent));
	} else if (dynamic_cast<QMediaPlaylist*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QMediaPlaylist*>(parent));
	} else if (dynamic_cast<QMediaRecorder*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QMediaRecorder*>(parent));
	} else if (dynamic_cast<QOffscreenSurface*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QOffscreenSurface*>(parent));
	} else if (dynamic_cast<QPaintDeviceWindow*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QPaintDeviceWindow*>(parent));
	} else if (dynamic_cast<QPdfWriter*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QPdfWriter*>(parent));
	} else if (dynamic_cast<QQuickItem*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QQuickItem*>(parent));
	} else if (dynamic_cast<QRadioData*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QRadioData*>(parent));
	} else if (dynamic_cast<QWidget*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QWidget*>(parent));
	} else if (dynamic_cast<QWindow*>(static_cast<QObject*>(parent))) {
		return new updater091c1b(static_cast<QWindow*>(parent));
	} else {
		return new updater091c1b(static_cast<QObject*>(parent));
	}
}

void updater091c1b_DestroyUpdater(void* ptr)
{
	static_cast<updater091c1b*>(ptr)->~updater091c1b();
}

void updater091c1b_DestroyUpdaterDefault(void* ptr)
{
	Q_UNUSED(ptr);

}

char updater091c1b_EventDefault(void* ptr, void* e)
{
	return static_cast<updater091c1b*>(ptr)->QObject::event(static_cast<QEvent*>(e));
}

char updater091c1b_EventFilterDefault(void* ptr, void* watched, void* event)
{
	return static_cast<updater091c1b*>(ptr)->QObject::eventFilter(static_cast<QObject*>(watched), static_cast<QEvent*>(event));
}

void updater091c1b_ChildEventDefault(void* ptr, void* event)
{
	static_cast<updater091c1b*>(ptr)->QObject::childEvent(static_cast<QChildEvent*>(event));
}

void updater091c1b_ConnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<updater091c1b*>(ptr)->QObject::connectNotify(*static_cast<QMetaMethod*>(sign));
}

void updater091c1b_CustomEventDefault(void* ptr, void* event)
{
	static_cast<updater091c1b*>(ptr)->QObject::customEvent(static_cast<QEvent*>(event));
}

void updater091c1b_DeleteLaterDefault(void* ptr)
{
	static_cast<updater091c1b*>(ptr)->QObject::deleteLater();
}

void updater091c1b_DisconnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<updater091c1b*>(ptr)->QObject::disconnectNotify(*static_cast<QMetaMethod*>(sign));
}

void updater091c1b_TimerEventDefault(void* ptr, void* event)
{
	static_cast<updater091c1b*>(ptr)->QObject::timerEvent(static_cast<QTimerEvent*>(event));
}



#include "moc_moc.h"

package app

/*
#cgo CFLAGS: -pipe -O2 -Wall -W -D_REENTRANT -fPIC -DQT_NO_DEBUG -DQT_MULTIMEDIA_LIB -DQT_DESIGNER_LIB -DQT_UIPLUGIN_LIB -DQT_WIDGETS_LIB -DQT_QUICK_LIB -DQT_GUI_LIB -DQT_QML_LIB -DQT_NETWORK_LIB -DQT_DBUS_LIB -DQT_XML_LIB -DQT_CORE_LIB
#cgo CXXFLAGS: -pipe -O2 -std=gnu++11 -Wall -W -D_REENTRANT -fPIC -DQT_NO_DEBUG -DQT_MULTIMEDIA_LIB -DQT_DESIGNER_LIB -DQT_UIPLUGIN_LIB -DQT_WIDGETS_LIB -DQT_QUICK_LIB -DQT_GUI_LIB -DQT_QML_LIB -DQT_NETWORK_LIB -DQT_DBUS_LIB -DQT_XML_LIB -DQT_CORE_LIB
#cgo CXXFLAGS: -I../../dragon -I. -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtMultimedia -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtDesigner -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtUiPlugin -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtWidgets -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtQuick -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtGui -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtQml -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtNetwork -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtDBus -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtXml -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/include/QtCore -I. -isystem /usr/include/libdrm -I/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/mkspecs/linux-g++
#cgo LDFLAGS: -O1 -Wl,-rpath,/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/lib
#cgo LDFLAGS:  -L/home/devwarrior/Qt5.12.0/5.12.0/gcc_64/lib -lQt5Multimedia -lQt5Designer -lQt5Widgets -lQt5Quick -lQt5Gui -lQt5Qml -lQt5Network -lQt5DBus -lQt5Xml -lQt5Core -lGL -lpthread
#cgo CFLAGS: -Wno-unused-parameter -Wno-unused-variable -Wno-return-type
#cgo CXXFLAGS: -Wno-unused-parameter -Wno-unused-variable -Wno-return-type
*/
import "C"

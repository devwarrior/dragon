package app

import (
	"gitlab.com/devwarrior/dragon/logger"

	"github.com/therecipe/qt/core"
	qtgui "github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/devwarrior/dragon/app/gui"
	"gitlab.com/devwarrior/dragon/app/local"
	pb "gitlab.com/devwarrior/dragon/session"
)

var _translate = core.QCoreApplication_Translate

// AppMain main application window
type AppMain struct {
	qtApp  *widgets.QApplication
	window *widgets.QMainWindow
	ui     *gui.UIMainwindowMainWindow
	create *Create
	join   *Join
	open   *Open
	// options *Options
	// about *About
	session *session
}

// NewAppMain constructs an App main object
func NewAppMain(qtApp *widgets.QApplication) *AppMain {
	appMain := AppMain{}
	appMain.qtApp = qtApp
	appMain.window = widgets.NewQMainWindow(nil, 0)
	appMain.ui = &gui.UIMainwindowMainWindow{}
	// dialogs
	appMain.create = nil
	appMain.join = nil
	appMain.open = nil
	// appMain.options = nil
	// appMain.about = nil
	// session: populated only while there is an active client session
	appMain.session = nil
	return &appMain
}

// LoadLocal starts up the local sessions file-backed list
func (a *AppMain) LoadLocal() error {
	err := local.Load()
	if err != nil {
		return err
	}
	local.RemoveInvalidSessions()
	return nil
}

// Start continues construction of the App main object
func (a *AppMain) Start() {
	a.window.ConnectShowEvent(a.windowOpening)
	a.window.ConnectCloseEvent(a.windowClosing)

	a.ui.SetupUI(a.window)

	a.create = newCreate(a).setup()
	a.join = newJoin(a).setup()
	a.open = newOpen(a).setup()

	a.createToolbar()
	a.ui.Statusbar.ShowMessage("No session", 0)

	a.setDefaults()
	a.center()
	a.window.Show()
}

func (a *AppMain) windowOpening(e *qtgui.QShowEvent) {
	logger.Log.Println("Main window opening")
	err := dbPing()
	if err != nil {
		warnMsg("Server Unavailable", "Session server is not available - check log")
	}
	e.Accept()
}

func (a *AppMain) windowClosing(e *qtgui.QCloseEvent) {
	logger.Log.Println("Main window closing")
	if a.session != nil {
		a.session.endSession()
	}
	e.Accept()
}

func (a *AppMain) createToolbar() {
	toolbar := widgets.NewQToolBar("Session", a.window)
	a.window.AddToolBar(core.Qt__TopToolBarArea, toolbar)

	// add the create action from the menu to the toolbar
	toolbar.QWidget.AddAction(a.ui.ActionCreate)
	iconCreate := a.window.Style().StandardIcon(widgets.QStyle__SP_FileDialogNewFolder, nil, nil)
	a.ui.ActionCreate.SetIcon(iconCreate)
	a.ui.ActionCreate.SetToolTip(_translate("MainWindow", "Create a New Swap session as Maker", "", -1))
	a.ui.ActionCreate.ConnectTriggered(a.onActionCreateTriggered)

	// add the join action from the menu to the toolbar
	toolbar.QWidget.AddAction(a.ui.ActionJoin)
	iconJoin := a.window.Style().StandardIcon(widgets.QStyle__SP_FileDialogBack, nil, nil)
	a.ui.ActionJoin.SetIcon(iconJoin)
	a.ui.ActionJoin.SetToolTip(_translate("MainWindow", "Join a New Swap session as Taker", "", -1))
	a.ui.ActionJoin.ConnectTriggered(a.onActionJoinTriggered)

	// add the open action from the menu to the toolbar
	toolbar.QWidget.AddAction(a.ui.ActionOpen)
	iconOpen := a.window.Style().StandardIcon(widgets.QStyle__SP_DialogOpenButton, nil, nil)
	a.ui.ActionOpen.SetIcon(iconOpen)
	a.ui.ActionOpen.SetToolTip(_translate("MainWindow", "Reopen a saved Swap session", "", -1))
	a.ui.ActionOpen.ConnectTriggered(a.onActionOpenTriggered)

	// add the close action from the menu to the toolbar
	toolbar.QWidget.AddAction(a.ui.ActionClose)
	iconClose := a.window.Style().StandardIcon(widgets.QStyle__SP_DialogCloseButton, nil, nil)
	a.ui.ActionClose.SetIcon(iconClose)
	a.ui.ActionClose.SetToolTip(_translate("MainWindow", "Close and save this Swap session", "", -1))
	a.ui.ActionClose.ConnectTriggered(a.onActionCloseTriggered)

	// add the delete action from the menu to the toolbar
	toolbar.QWidget.AddAction(a.ui.ActionDelete)
	iconDelete := a.window.Style().StandardIcon(widgets.QStyle__SP_DialogDiscardButton, nil, nil)
	a.ui.ActionDelete.SetIcon(iconDelete)
	a.ui.ActionDelete.SetToolTip(_translate("MainWindow", "Permanently delete the current Swap session", "", -1))
	a.ui.ActionDelete.ConnectTriggered(a.onActionDeleteTriggered)
}

func (a *AppMain) center() {
	screenRectCenter := a.qtApp.Desktop().ScreenGeometry(a.window).Center()
	windowRectCenter := a.window.FrameGeometry().Center()
	a.window.Move2(
		screenRectCenter.X()-windowRectCenter.X(), screenRectCenter.Y()-windowRectCenter.Y())
}

func (a *AppMain) setDefaults() {
	// Session info
	a.ui.SessionKeyLineEdit.SetText("")
	a.ui.MakerAddressLineEdit.SetText("")
	a.ui.TakerAddressLineEdit.SetText("")
	a.ui.SessionStateLineEdit.SetText("")
	a.ui.NextStateLineEdit.SetText("")
	// Deal
	a.ui.LabelMkr.SetText("")
	a.ui.LabelMkrRcv.SetText("")
	a.ui.LabelTkr.SetText("")
	a.ui.LabelTkrRcv.SetText("")
	a.ui.LabelNetwork.SetText("")
	a.ui.LabelNetworkId.SetText("")
	a.ui.LabelIAm.SetText("")
	// button review
	a.ui.PushButtonReview.DisconnectClicked()
	a.ui.PushButtonReview.SetVisible(false)
	// button state
	a.ui.PushButtonNextState.DisconnectClicked()
	a.ui.PushButtonNextState.SetVisible(false)
	//menu+toolbar actions
	a.ui.ActionCreate.SetEnabled(true)
	a.ui.ActionJoin.SetEnabled(true)
	a.ui.ActionOpen.SetEnabled(true)
	a.ui.ActionClose.SetEnabled(false)
	a.ui.ActionDelete.SetEnabled(false)

	a.ui.Statusbar.ShowMessage("No session", 0)
}

////////////////////
// Dialog actions //
////////////////////

func (a *AppMain) onActionCreateTriggered(checked bool) {
	logger.Log.Println("onActionCreateTriggered")
	a.create.setDefaults()
	a.create.showDialog()
}

func (a *AppMain) onActionJoinTriggered(checked bool) {
	logger.Log.Println("onActionJoinTriggered")
	a.join.setDefaults()
	a.join.showDialog()
}

func (a *AppMain) onActionOpenTriggered(checked bool) {
	logger.Log.Println("onActionOpenTriggered")
	a.open.setDefaults()
	a.open.showDialog()
}

func (a *AppMain) onActionCloseTriggered(checked bool) {
	logger.Log.Println("onActionCloseTriggered")
	if a.session == nil {
		return
	}
	a.session.endSession()
	a.session = nil
	a.setDefaults()
}

func (a *AppMain) onActionDeleteTriggered(checked bool) {
	logger.Log.Println("onActionDeleteTriggered")
	if !okMsg("Delete session", "Are you sure?") {
		return
	}
	if a.session == nil {
		return
	}
	a.session.endSessionAndMarkDeleted()
	a.session = nil
	a.setDefaults()
}

//////////////////////
// Session complete //
//////////////////////

// onSessionCompleteRemoveLocal is called when the user clicks state button
// [ Remove session ]
func (a *AppMain) onSessionCompleteRemoveLocal() {
	logger.Log.Println("onSessionCompleteRemoveLocal")
	if !okMsg("Remove session", "Are you sure?") {
		return
	}
	if a.session == nil {
		return
	}
	a.session.endSessionCompleteInvalidateLocal()
	a.session = nil
	a.setDefaults()
}

////////////////////
// Session states //
////////////////////

// onSessionMarkedDeleted is called when remote counterparty marked session as deleted (poller)
func (a *AppMain) onSessionMarkedDeleted(sessionID string) {
	logger.Log.Println("onSessionMarkedDeleted")
	a.setDefaults()
	a.ui.Statusbar.ShowMessage("No session", 0)
}

// onSessionStateUnknown is called when poller hits transport or other errors
func (a *AppMain) onSessionStateUnknown(sessionError sErr) {
	logger.Log.Println("onSessionStateUnknown")
	a.setDefaults()
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true) // only close
	a.ui.ActionDelete.SetEnabled(false)
	stateText := "Session State Unknown"
	switch sessionError {
	case sErrTransport:
		stateText = stateText + " - Transport error"
	case sErrData:
		stateText = stateText + " - Data error"
	}
	a.ui.SessionStateLineEdit.SetText(stateText)
	a.ui.Statusbar.ShowMessage(stateText, 0)
}

// onSessionStateChanged is called when the session init/sequence state changes
func (a *AppMain) onSessionStateChanged(newState pb.STATE, data *uidata) {
	uiState := data.uiState

	a.setUIData(data)
	a.setUIState(uiState)
	a.setNextStateButton(uiState)
	a.setReview(uiState)

	// State specific changes
	switch newState {

	// Session init
	case pb.STATE_NewSession:
		a.onStateNewSession(data)
	case pb.STATE_ParticipantJoined:
		a.onStateParticipantJoined(data)

	// Swap sequence - part 1
	case pb.STATE_PartRedeemableContractOffered:
		a.onStatePartRedeemableContractOffered(data)
	case pb.STATE_PartRedeemableContractAccepted:
		a.onStatePartRedeemableContractAccepted(data)
	case pb.STATE_InitRedeemableContractOffered:
		a.onStateInitRedeemableContractOffered(data)
	case pb.STATE_InitRedeemableContractAccepted:
		a.onStateInitRedeemableContractAccepted(data)

	// Swap sequence - part 2
	case pb.STATE_PartRedeemableContractPublished:
		a.onStatePartRedeemableContractPublished(data)
	case pb.STATE_InitRedeemableContractPublished:
		a.onStateInitRedeemableContractPublished(data)
	case pb.STATE_PartRedeemableContractConfirmed:
		a.onStatePartRedeemableContractConfirmed(data)
	case pb.STATE_InitRedeemableContractConfirmed:
		a.onStateInitRedeemableContractConfirmed(data)

	// Swap sequence - part 3
	case pb.STATE_InitRedeemTxPublished:
		a.onStateInitRedeemTxPublished(data)
	case pb.STATE_PartRedeemTxPublished:
		a.onStatePartRedeemTxPublished(data)
	}
}

// setUIData sets session, receive addresses, etc.
func (a *AppMain) setUIData(data *uidata) {
	a.ui.SessionKeyLineEdit.SetText(data.session)
	a.ui.MakerAddressLineEdit.SetText(data.initRcvAddress)
	a.ui.TakerAddressLineEdit.SetText(data.partRcvAddress)
	// Deal
	a.ui.LabelMkr.SetText("Maker receives")
	a.ui.LabelMkrRcv.SetText(data.initCoinRcvValue)
	a.ui.LabelTkr.SetText("Taker receives")
	a.ui.LabelTkrRcv.SetText(data.partCoinRcvValue)
	a.ui.LabelNetwork.SetText("Network")
	a.ui.LabelNetworkId.SetText(data.network)
	// UI Role
	a.ui.LabelIAm.SetText("I am " + uirole[data.sessionRole])
}

// setUIState sets current & next state info
func (a *AppMain) setUIState(uiState *uistate) {
	a.ui.SessionStateLineEdit.SetText(uiState.current)
	a.ui.NextStateLineEdit.SetText(uiState.next)
	a.ui.Statusbar.ShowMessage(uiState.current, 0)
}

// setNextStateButton sets the text, visibility and the fn to be called by the
// PushButtonNextState button
func (a *AppMain) setNextStateButton(uiState *uistate) {
	button := a.ui.PushButtonNextState
	button.DisconnectClicked()
	if !uiState.makeButton {
		button.SetText("")
		button.SetVisible(false)
		return
	}
	button.ConnectClicked(uiState.buttonFn)
	button.SetText(uiState.buttonText)
	button.SetVisible(true)
}

// allow review button if requested, currently only on session complete
func (a *AppMain) setReview(uiState *uistate) {
	review := a.ui.PushButtonReview
	review.DisconnectClicked()
	if !uiState.makeReview {
		review.SetText("")
		review.SetVisible(false)
		return
	}
	review.ConnectClicked(uiState.reviewFn)
	review.SetText("Review swap")
	review.SetVisible(true)
}

// State specific

func (a *AppMain) onStateNewSession(data *uidata) {
	logger.Log.Println("onStateNewSession")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(true)
}

func (a *AppMain) onStateParticipantJoined(data *uidata) {
	logger.Log.Println("onStateParticipantJoined")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(true)
}

func (a *AppMain) onStatePartRedeemableContractOffered(data *uidata) {
	logger.Log.Println("onStatePartRedeemableContractOffered")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(true)
}
func (a *AppMain) onStatePartRedeemableContractAccepted(data *uidata) {
	logger.Log.Println("onStatePartRedeemableContractAccepted")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(true)
}
func (a *AppMain) onStateInitRedeemableContractOffered(data *uidata) {
	logger.Log.Println("onStateInitRedeemableContractOffered")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(true)
}
func (a *AppMain) onStateInitRedeemableContractAccepted(data *uidata) {
	logger.Log.Println("onStateInitRedeemableContractAccepted")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(true)
}

func (a *AppMain) onStatePartRedeemableContractPublished(data *uidata) {
	logger.Log.Println("onStatePartRedeemableContractPublished")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	//switch delete off
	a.ui.ActionDelete.SetEnabled(false)
}
func (a *AppMain) onStateInitRedeemableContractPublished(data *uidata) {
	logger.Log.Println("onStateInitRedeemableContractPublished")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(false)
}
func (a *AppMain) onStatePartRedeemableContractConfirmed(data *uidata) {
	logger.Log.Println("onStatePartRedeemableContractConfirmed")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(false)
}
func (a *AppMain) onStateInitRedeemableContractConfirmed(data *uidata) {
	logger.Log.Println("onStateInitRedeemableContractConfirmed")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(false)
}

func (a *AppMain) onStateInitRedeemTxPublished(data *uidata) {
	logger.Log.Println("onStateInitRedeemTxPublished")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(false)
}
func (a *AppMain) onStatePartRedeemTxPublished(data *uidata) {
	logger.Log.Println("onStatePartRedeemTxPublished")
	a.ui.ActionCreate.SetEnabled(false)
	a.ui.ActionJoin.SetEnabled(false)
	a.ui.ActionOpen.SetEnabled(false)
	a.ui.ActionClose.SetEnabled(true)
	a.ui.ActionDelete.SetEnabled(false)
}

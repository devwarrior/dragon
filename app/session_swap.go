package app

import (
	"fmt"

	"gitlab.com/devwarrior/dragon/logger"

	"time"

	"github.com/devwarrior777/atomicswap/libs"
	"gitlab.com/devwarrior/dragon/app/local"
	pb "gitlab.com/devwarrior/dragon/session"
)

/////////////////////////////
// Swap Sequence functions //
/////////////////////////////

//////////////////////////////
// Phase 1. Agree contracts //
//////////////////////////////

// initMakeContract:
// Initiator makes a contract that the Participant can redeem later
func (s *session) initMakeContract(bool) {
	logger.Log.Println("initMakeContract")
	// local.session exist
	if !local.SessionExists(s.sessionID) {
		logger.Log.Println("local session does not exist")
		warnMsg("error", "cannot get local session data")
		return
	}
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	secretHash := dataResponse.Data.Secrethash
	addrToSendTo := dataResponse.Data.Participant
	amount := dataResponse.Data.PartCoinRcvValue
	// make contract, contractTx
	params := libs.InitiateParams{
		SecretHash: secretHash,
		CP2Addr:    addrToSendTo,
		CP2Amount:  amount,
	}
	initiateResult, err := s.sndWallet.Initiate(params)
	if err != nil {
		logger.Log.Printf("Initiate error: %v\n", err)
		warnMsg("error", "cannot make contract")
		return
	}
	contract := initiateResult.Contract
	contractTx := initiateResult.ContractTx
	contractP2SH := initiateResult.ContractP2SH
	contractLocktime := initiateResult.ContractRefundLocktime
	// update local.session
	err = local.SetContractData(s.sessionID, contract, contractTx, contractP2SH, contractLocktime)
	if err != nil {
		logger.Log.Printf("local error: %v\n", err)
		warnMsg("error", "cannot store contract to local session")
		return
	}
	// update session data & state with contract, contractTx
	request := &pb.InitStorePartRedeemableContractRequest{Session: s.sessionID}
	request.PartRedeemableContract = contract
	request.PartRedeemableContractTx = contractTx
	request.PartRedeemableContractP2Sh = contractP2SH
	request.PartRedeemableContractLocktime = contractLocktime
	_, err = dbInitStorePartRedeemableContract(request)
	if err != nil {
		logger.Log.Printf("db error: %v\n", err)
		warnMsg("error", "cannot store contract")
		return
	}
	// poller updates gui
	s.poller.checkPoll()
}

// partAcceptContract:
// Participant accepts or rejects the contract offered by the Initiator
// Rejection 'deletes' the db session
func (s *session) partAcceptContract(bool) {
	logger.Log.Println("partAcceptContract")
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	// prepare text for audit dialog
	contractAddr := dataResponse.Data.PartRedeemableContractP2Sh
	coin := coinname(dataResponse.Data.PartCoinRcv)
	amount := Amount(dataResponse.Data.PartCoinRcvValue)
	contractAmount := fmt.Sprintf("%0.08f %s", amount.ToCoins(), coin)
	recipientAddr := dataResponse.Data.Participant
	t := time.Unix(dataResponse.Data.PartRedeemableContractLocktime, 0)
	contractLocktime := fmt.Sprintf("%v", t.UTC())
	// let the audit dialog do the work
	a := newAudit(s.app).setup(contractAddr, contractAmount, recipientAddr, contractLocktime)
	a.show()
}

// partMakeContract:
// Participant makes a contract that the Initiator can redeem later
func (s *session) partMakeContract(bool) {
	logger.Log.Println("partMakeContract")
	// local.session exist
	if !local.SessionExists(s.sessionID) {
		logger.Log.Println("local session does not exist")
		warnMsg("error", "cannot get local session data")
		return
	}
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	secretHash := dataResponse.Data.Secrethash
	addrToSendTo := dataResponse.Data.Initiator
	amount := dataResponse.Data.InitCoinRcvValue
	// make contract, contractTx
	params := libs.ParticipateParams{
		SecretHash: secretHash,
		CP1Addr:    addrToSendTo,
		CP1Amount:  amount,
	}
	participateResult, err := s.sndWallet.Participate(params)
	if err != nil {
		logger.Log.Printf("Participate error: %v\n", err)
		warnMsg("error", "cannot make contract")
		return
	}
	contract := participateResult.Contract
	contractTx := participateResult.ContractTx
	contractP2SH := participateResult.ContractP2SH
	contractLocktime := participateResult.ContractRefundLocktime
	// update local.session
	err = local.SetContractData(s.sessionID, contract, contractTx, contractP2SH, contractLocktime)
	if err != nil {
		logger.Log.Printf("local error: %v\n", err)
		warnMsg("error", "cannot store contract to local session")
		return
	}
	// update session data & state with contract, contractTx
	request := &pb.PartStoreInitRedeemableContractRequest{Session: s.sessionID}
	request.InitRedeemableContract = contract
	request.InitRedeemableContractTx = contractTx
	request.InitRedeemableContractP2Sh = contractP2SH
	request.InitRedeemableContractLocktime = contractLocktime
	_, err = dbPartStoreInitRedeemableContract(request)
	if err != nil {
		logger.Log.Printf("db error: %v\n", err)
		warnMsg("error", "cannot store contract")
		return
	}
	// poller updates gui
	s.poller.checkPoll()
}

// initAcceptContract:
// Initiator accepts or rejects the contract offered by the Participant.
// Rejection 'deletes' the db session
func (s *session) initAcceptContract(bool) {
	logger.Log.Println("initAcceptContract")
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	// prepare text for audit dialog
	contractAddr := dataResponse.Data.InitRedeemableContractP2Sh
	coin := coinname(dataResponse.Data.InitCoinRcv)
	amount := Amount(dataResponse.Data.InitCoinRcvValue)
	contractAmount := fmt.Sprintf("%0.08f %s", amount.ToCoins(), coin)
	recipientAddr := dataResponse.Data.Initiator
	t := time.Unix(dataResponse.Data.InitRedeemableContractLocktime, 0)
	contractLocktime := fmt.Sprintf("%v", t.UTC())
	// let the audit dialog do the work
	a := newAudit(s.app).setup(contractAddr, contractAmount, recipientAddr, contractLocktime)
	a.show()
}

////////////////////////////////////////////
// Phase 2. Publish and confirm contracts //
////////////////////////////////////////////

// initPublishContract:
// Initiator publishes a contract that the Participant can redeem later
func (s *session) initPublishContract(bool) {
	logger.Log.Println("initPublishContract")
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	// initiator broadcasts the participant redeemable contract
	contractTx := dataResponse.Data.PartRedeemableContractTx
	txhash, err := s.sndWallet.Publish(contractTx)
	if err != nil {
		logger.Log.Printf("publish error: %v\n", err)
		warnMsg("error", "cannot publish contract tx")
		return
	}
	// store txid
	request := &pb.InitStorePublishedPartRedeemableContractTxidRequest{Session: s.sessionID}
	request.PartRedeemableContractTxid = txhash
	_, err = dbInitStorePublishedPartRedeemableContractTxid(request)
	if err != nil {
		logger.Log.Printf("db error: %v\n", err)
		warnMsg("error", "cannot store contract txid")
		return
	}
	// poller updates gui
	s.poller.checkPoll()
}

// partPublishContract:
// Participant publishes a contract that the Initiator can redeem later
func (s *session) partPublishContract(bool) {
	logger.Log.Println("partPublishContract")
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	// participant broadcasts the initiator redeemable contract
	contractTx := dataResponse.Data.InitRedeemableContractTx
	txhash, err := s.sndWallet.Publish(contractTx)
	if err != nil {
		logger.Log.Printf("publish error: %v\n", err)
		warnMsg("error", "cannot publish contract tx")
		return
	}
	// store txid
	request := &pb.PartStorePublishedInitRedeemableContractTxidRequest{Session: s.sessionID}
	request.InitRedeemableContractTxid = txhash
	_, err = dbPartStorePublishedInitRedeemableContractTxid(request)
	if err != nil {
		logger.Log.Printf("db error: %v\n", err)
		warnMsg("error", "cannot store contract txid")
		return
	}
	// poller updates gui
	s.poller.checkPoll()
}

func (s *session) initConfirmContract(bool) {
	logger.Log.Println("initConfirmContract")
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	txid := dataResponse.Data.PartRedeemableContractTxid
	// initial sanity check it is at least in mempool or even mined
	result, err := s.sndWallet.GetTx(txid)
	if err != nil {
		logger.Log.Printf("GetTx error: %v\n", err)
		warnMsg("error", "cannot get tx data for txid")
		return
	}
	logger.Log.Printf("tx data for txid %s: %v\n", txid, result)
	// let the dialog do the work
	c := newConfirm(s.app, txid).setup()
	c.show()
}

func (s *session) partConfirmContract(bool) {
	logger.Log.Println("partConfirmContract")
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	txid := dataResponse.Data.InitRedeemableContractTxid
	// initial sanity check it is at least in mempool or even mined
	result, err := s.sndWallet.GetTx(txid)
	if err != nil {
		logger.Log.Printf("GetTx error: %v\n", err)
		warnMsg("error", "cannot get tx data for txid")
		return
	}
	logger.Log.Printf("tx data for txid %s: %v\n", txid, result)
	// let the dialog do the work
	c := newConfirm(s.app, txid).setup()
	c.show()
}

///////////////////////////////
// Phase 3. Redeem contracts //
///////////////////////////////

func (s *session) initPublishRedeemTx(bool) {
	logger.Log.Println("initPublishRedeemTx")
	// local.session
	localSession := local.GetSession(s.sessionID)
	if localSession == nil {
		logger.Log.Println("local session does not exist")
		warnMsg("error", "cannot get local session data")
		return
	}
	optSsig := localSession.OptSsig
	if len(optSsig) == 0 {
		logger.Log.Println("local session optSsig does not exist")
		warnMsg("error", "cannot get local session optSsig")
		return
	}
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	redeemParams := libs.RedeemParams{}
	redeemParams.Secret = optSsig
	redeemParams.Contract = dataResponse.Data.InitRedeemableContract
	redeemParams.ContractTx = dataResponse.Data.InitRedeemableContractTx
	// get redeem tx
	redeemResult, err := s.rcvWallet.Redeem(redeemParams)
	if err != nil {
		logger.Log.Printf("rcvWallet.Redeem error: %v\n", err)
		warnMsg("error", "cannot make redeem transaction")
		return
	}
	// publish redeem tx
	tx := redeemResult.RedeemTx
	txhash, err := s.rcvWallet.Publish(tx)
	if err != nil {
		logger.Log.Printf("rcvWallet.Publish error: %v\n", err)
		warnMsg("error", "cannot publish redeem transaction")
		return
	}
	logger.Log.Printf("published redeem: %s\n", txhash)
	// update session
	request := &pb.InitStoreInitPublishedRedeemTxidRequest{Session: s.sessionID}
	request.InitRedeemTxid = txhash
	request.OptSsig = optSsig
	_, err = dbInitStoreInitPublishedRedeemTxid(request)
	if err != nil {
		logger.Log.Printf("db store error: %v\n", err)
		warnMsg("error", "cannot store redeem txid")
		return
	}
	// update local.session
	local.SetCounterpartyContractRedeemed(s.sessionID)
	// poller updates gui
	s.poller.checkPoll()
}

func (s *session) partPublishRedeemTx(bool) {
	logger.Log.Println("partPublishRedeemTx")
	// get session details
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	redeemParams := libs.RedeemParams{}
	redeemParams.Secret = dataResponse.Data.OptSsig
	if len(redeemParams.Secret) == 0 {
		logger.Log.Println("db session optSsig does not exist")
		warnMsg("error", "cannot get db session optSsig")
		return
	}
	redeemParams.Contract = dataResponse.Data.PartRedeemableContract
	redeemParams.ContractTx = dataResponse.Data.PartRedeemableContractTx
	// get redeem tx
	redeemResult, err := s.rcvWallet.Redeem(redeemParams)
	if err != nil {
		logger.Log.Printf("rcvWallet.Redeem error: %v\n", err)
		warnMsg("error", "cannot make redeem transaction")
		return
	}
	// publish redeem tx
	tx := redeemResult.RedeemTx
	txhash, err := s.rcvWallet.Publish(tx)
	if err != nil {
		logger.Log.Printf("rcvWallet.Publish error: %v\n", err)
		warnMsg("error", "cannot publish redeem transaction")
		return
	}
	logger.Log.Printf("published redeem: %s\n", txhash)
	// update session
	request := &pb.PartStorePartPublishedRedeemTxidRequest{Session: s.sessionID}
	request.PartRedeemTxid = txhash
	_, err = dbPartStorePartPublishedRedeemTxid(request)
	if err != nil {
		logger.Log.Printf("db store error: %v\n", err)
		warnMsg("error", "cannot store redeem txid")
		return
	}
	// update local.session
	local.SetCounterpartyContractRedeemed(s.sessionID)
	// poller updates gui
	s.poller.checkPoll()
}

/////////////////
// End session //
/////////////////

// last button state in the session - shown for both roles
func (s *session) removeLocalSession(bool) {
	logger.Log.Println("endLocalSession")
	s.app.onSessionCompleteRemoveLocal()
}

/////////////////
// Review swap //
/////////////////

// reviewSwap has it's own button and is visible when swap complete - shown for both roles
func (s *session) reviewSwap(bool) {
	logger.Log.Println("reviewSwap")
	dataRequest := &pb.DataRequest{Session: s.sessionID}
	dataResponse, err := dbGetData2(dataRequest)
	if err != nil {
		logger.Log.Printf("dbGetData error: %v\n", err)
		warnMsg("error", "cannot get session data")
		return
	}
	// let the review dialog show the data
	r := newReview(s.app).setup(dataResponse)
	r.show()
}

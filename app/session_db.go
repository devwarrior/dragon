package app

import (
	"fmt"

	"gitlab.com/devwarrior/dragon/logger"

	pb "gitlab.com/devwarrior/dragon/session"
	cc "gitlab.com/devwarrior/dragon/session/dbclient"
	"google.golang.org/grpc/status"
)

func dbPing() error {
	request := pb.PingRequest{}
	_, err := cc.Ping(&request)
	if err != nil {
		// The Go gRPC implementation guarantees that all errors returned from
		// RPC calls are status type errors.
		s := status.Convert(err)
		logger.Log.Printf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return err
	}
	return nil
}

func dbNewSession(request *pb.NewSessionRequest) (*pb.DataResponse, error) {
	response, err := cc.NewSession(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbAddParticipant(request *pb.AddParticipantRequest) (*pb.DataResponse, error) {
	response, err := cc.AddParticipant(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbInitStorePartRedeemableContract(
	request *pb.InitStorePartRedeemableContractRequest) (*pb.DataResponse, error) {
	response, err := cc.InitStorePartRedeemableContract(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbPartSetPartRedeemableContractAcceptance(
	request *pb.PartSetPartRedeemableContractAcceptanceRequest) (*pb.DataResponse, error) {
	response, err := cc.PartSetPartRedeemableContractAcceptance(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbPartStoreInitRedeemableContract(
	request *pb.PartStoreInitRedeemableContractRequest) (*pb.DataResponse, error) {
	response, err := cc.PartStoreInitRedeemableContract(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbInitSetInitRedeemableContractAcceptance(
	request *pb.InitSetInitRedeemableContractAcceptanceRequest) (*pb.DataResponse, error) {
	response, err := cc.InitSetInitRedeemableContractAcceptance(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbInitStorePublishedPartRedeemableContractTxid(
	request *pb.InitStorePublishedPartRedeemableContractTxidRequest) (*pb.DataResponse, error) {
	response, err := cc.InitStorePublishedPartRedeemableContractTxid(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbPartStorePublishedInitRedeemableContractTxid(
	request *pb.PartStorePublishedInitRedeemableContractTxidRequest) (*pb.DataResponse, error) {
	response, err := cc.PartStorePublishedInitRedeemableContractTxid(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbInitSetPartRedeemableContractConfirmed(
	request *pb.InitSetPartRedeemableContractConfirmedRequest) (*pb.DataResponse, error) {
	response, err := cc.InitSetPartRedeemableContractConfirmed(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbPartSetInitRedeemableContractConfirmed(
	request *pb.PartSetInitRedeemableContractConfirmedRequest) (*pb.DataResponse, error) {
	response, err := cc.PartSetInitRedeemableContractConfirmed(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbInitStoreInitPublishedRedeemTxid(
	request *pb.InitStoreInitPublishedRedeemTxidRequest) (*pb.DataResponse, error) {
	response, err := cc.InitStoreInitPublishedRedeemTxid(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbPartStorePartPublishedRedeemTxid(
	request *pb.PartStorePartPublishedRedeemTxidRequest) (*pb.DataResponse, error) {
	response, err := cc.PartStorePartPublishedRedeemTxid(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

func dbMarkDeleted(request *pb.MarkDeletedRequest) (*pb.MarkDeletedResponse, error) {
	response, err := cc.MarkDeleted(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

//
// No delete fn.
//

///////////////
// Accessors //
///////////////

// dbGetData1 gets data for a session.
//
// This one is a different pattern as Transport/infrastructure (Status) errors
// are sent back as an error. However database errors are passed through. This
// makes it easy to distinguish transport errors from the higher level database
// errors for reporting back to the ui
func dbGetData1(request *pb.DataRequest) (*pb.DataResponse, error) {
	response, err := cc.GetData(request)
	if err != nil {
		// Transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	//
	// Deal with Database errors on return
	//
	return response, nil
}

// dbGetData2 gets data for a session.
func dbGetData2(request *pb.DataRequest) (*pb.DataResponse, error) {
	response, err := cc.GetData(request)
	if err != nil {
		// Transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

// dbGetState gets latest session state -3, -2, -1, 0..13
//
// Used by the poller any error will be reported as -1 STATE_Unknown. This limit
// is really because of the limit on what can be passed to a Qt Slot function from
// another thread. So we just pass State as an int, and later re-check state using
// the dbGetData1 fn. above once we are executing on the UI thread again.
func dbGetState(request *pb.StateRequest) (*pb.StateResponse, error) {
	response, err := cc.GetState(request)
	if err != nil {
		// transport/infrastucture errors
		s := status.Convert(err)
		return nil, fmt.Errorf("status: %d - %v - %v", s.Code(), s.Code(), s.Message())
	}
	// database errors
	if response.Errno != pb.ERRNO_OK {
		return nil, fmt.Errorf("database errno: %v - %s", response.Errno, response.Errstr)
	}
	return response, nil
}

package app

import (
	"fmt"

	pb "gitlab.com/devwarrior/dragon/session"
)

type uistate struct {
	current    string
	next       string
	makeButton bool
	buttonText string
	buttonFn   func(bool)
	makeReview bool
	reviewFn   func(bool)
}

type uidata struct {
	sessionRole      role
	session          string
	initRcvAddress   string
	partRcvAddress   string
	uiState          *uistate
	network          string
	initCoinRcv      string
	initCoinRcvValue string
	partCoinRcv      string
	partCoinRcvValue string
}

func (s *session) getUIData(r role, pbData *pb.Data) *uidata {
	data := &uidata{
		sessionRole: r,
		session:     s.sessionID,
	}
	// get sequence and button texts, etc. for the user's role
	state := pbData.State
	data.uiState = &uistate{}
	s.getStateText(r, state, data.uiState)
	// get relevant strings from session data
	data.initRcvAddress = pbData.Initiator
	data.partRcvAddress = pbData.Participant
	data.network = pb.NET_TYPE_name[int32(pbData.Network)]
	data.initCoinRcv = pb.COIN_name[int32(pbData.InitCoinRcv)]
	data.partCoinRcv = pb.COIN_name[int32(pbData.PartCoinRcv)]
	data.initCoinRcvValue = getCoinsValueStr(pbData.InitCoinRcvValue, data.initCoinRcv)
	data.partCoinRcvValue = getCoinsValueStr(pbData.PartCoinRcvValue, data.partCoinRcv)
	return data
}

func (s *session) getStateText(r role, state pb.STATE, uiState *uistate) {
	// default no button and no review
	uiState.makeButton = false
	uiState.makeReview = false

	switch state {

	// Session init
	case pb.STATE_NewSession:
		uiState.current = "New Session created"
		uiState.next = "Waiting for Taker to join"

	case pb.STATE_ParticipantJoined:
		uiState.current = "Taker joined the session"
		uiState.next = "Waiting for Maker to offer contract"
		if r == initiator {
			uiState.makeButton = true
			uiState.buttonText = "Make contract"
			uiState.buttonFn = s.initMakeContract
		}

	// Swap sequence - part 1

	case pb.STATE_PartRedeemableContractOffered:
		uiState.current = "Maker offered contract"
		uiState.next = "Waiting for Taker to accept contract"
		if r == participant {
			uiState.makeButton = true
			uiState.buttonText = "Review contract"
			uiState.buttonFn = s.partAcceptContract
		}

	case pb.STATE_PartRedeemableContractAccepted:
		uiState.current = "Taker accepted contract"
		uiState.next = "Waiting for Taker to offer contract"
		if r == participant {
			uiState.makeButton = true
			uiState.buttonText = "Make contract"
			uiState.buttonFn = s.partMakeContract
		}

	case pb.STATE_InitRedeemableContractOffered:
		uiState.current = "Taker offered contract"
		uiState.next = "Waiting for Maker to accept contract"
		if r == initiator {
			uiState.makeButton = true
			uiState.buttonText = "Review contract"
			uiState.buttonFn = s.initAcceptContract
		}

	case pb.STATE_InitRedeemableContractAccepted:
		uiState.current = "Maker accepted contract"
		uiState.next = "Waiting for Maker to publish contract"
		if r == initiator {
			uiState.makeButton = true
			uiState.buttonText = "Publish contract"
			uiState.buttonFn = s.initPublishContract
		}

	// Swap sequence - part 2

	case pb.STATE_PartRedeemableContractPublished:
		uiState.current = "Maker published contract"
		uiState.next = "Waiting for Taker to publish contract"
		if r == participant {
			uiState.makeButton = true
			uiState.buttonText = "Publish contract"
			uiState.buttonFn = s.partPublishContract
		}
	case pb.STATE_InitRedeemableContractPublished:
		uiState.current = "Taker published contract"
		uiState.next = "Waiting for Maker contract confirmed"
		if r == initiator {
			uiState.makeButton = true
			uiState.buttonText = "Confirm"
			uiState.buttonFn = s.initConfirmContract
		}
	case pb.STATE_PartRedeemableContractConfirmed:
		uiState.current = "Maker contract confirmed"
		uiState.next = "Waiting for Taker contract confirmed"
		if r == participant {
			uiState.makeButton = true
			uiState.buttonText = "Confirm"
			uiState.buttonFn = s.partConfirmContract
		}
	case pb.STATE_InitRedeemableContractConfirmed:
		uiState.current = "Taker contract confirmed"
		uiState.next = "Waiting for Maker to publish a redeem tx"
		if r == initiator {
			uiState.makeButton = true
			uiState.buttonText = "Publish redeem tx"
			uiState.buttonFn = s.initPublishRedeemTx
		}

	// Swap sequence - part 3

	case pb.STATE_InitRedeemTxPublished:
		uiState.current = "Maker published redeem tx"
		uiState.next = "Waiting for Taker to publish a redeem tx"
		if r == participant {
			uiState.makeButton = true
			uiState.buttonText = "Publish redeem tx"
			uiState.buttonFn = s.partPublishRedeemTx
		}
	case pb.STATE_PartRedeemTxPublished:
		uiState.current = "Taker published redeem tx"
		uiState.next = "Complete"
		uiState.makeButton = true
		uiState.buttonText = "Remove session"
		uiState.buttonFn = s.removeLocalSession
		uiState.makeReview = true
		uiState.reviewFn = s.reviewSwap
	}
}

func getCoinsValueStr(sats int64, coinName string) string {
	a := Amount(sats)
	f := a.ToCoins()
	return fmt.Sprintf("%0.8f %s", f, coinName)
}

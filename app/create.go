package app

import (
	"fmt"

	"gitlab.com/devwarrior/dragon/logger"

	"github.com/therecipe/qt/widgets"
	"gitlab.com/devwarrior/dragon/app/gui"
	"gitlab.com/devwarrior/dragon/appconfig"
)

///////////////////
// Create dialog //
///////////////////

// Create dialog
type Create struct {
	app    *AppMain
	dialog *widgets.QDialog
	ui     *gui.UICreateDialog
}

// NewCreate constructs a Create dialog object
func newCreate(appMain *AppMain) *Create {
	create := Create{app: appMain}
	create.dialog = widgets.NewQDialog(appMain.window, 0)
	create.ui = &gui.UICreateDialog{}
	return &create
}

// Setup continues construction of the create dialog
func (c *Create) setup() *Create {
	c.ui.SetupUI(c.dialog)
	c.dialog.SetMinimumSize2(50, 50)
	// c.center()
	c.ui.ButtonBox.ConnectAccepted(c.onButtonBoxAccepted)
	c.ui.ButtonBox.ConnectRejected(c.onButtonBoxRejected)
	c.ui.CheckBoxTestnet.ConnectStateChanged(c.onCheckBoxStateChange)
	c.setDefaults()
	c.dialog.SetModal(true)
	return c
}

func (c *Create) center() {
	d := c.dialog
	w := c.app.window
	d.Move2(w.X()+((w.Width()-d.Width())/2), w.Y()+((w.Height()-d.Height())/2))
}

func (c *Create) showDialog() {
	c.dialog.Show()
	c.center()
}

func (c *Create) setDefaults() {
	minSnd := c.ui.DoubleSpinBoxSend.Minimum()
	minRcv := c.ui.DoubleSpinBoxReceive.Minimum()
	c.ui.DoubleSpinBoxSend.SetValue(minSnd)
	c.ui.DoubleSpinBoxReceive.SetValue(minRcv)
	c.ui.CheckBoxTestnet.SetChecked(false)
	c.ui.ComboBoxSend.Clear()
	c.ui.ComboBoxReceive.Clear()
	//TODO: populate dynamically depending coins available via rpc
	//      for the mainnet network
	// qv := core.NewQVariant14("test qvariant data")
	// c.ui.ComboBoxSend.AddItem("COIN", qv)
	mainnetCoins := appconfig.EnabledMainnetCoins
	c.ui.ComboBoxSend.AddItems(mainnetCoins)
	c.ui.ComboBoxReceive.AddItems(mainnetCoins)
	c.ui.ComboBoxSend.SetCurrentIndex(0)
	c.ui.ComboBoxReceive.SetCurrentIndex(0)
}

// get dialog selections
func (c *Create) getParams() *createParams {
	params := createParams{}
	params.testnet = c.ui.CheckBoxTestnet.IsChecked()
	params.coinSnd = c.ui.ComboBoxSend.ItemText(c.ui.ComboBoxSend.CurrentIndex())
	params.coinSndValue = c.ui.DoubleSpinBoxSend.Value() // float
	params.coinRcv = c.ui.ComboBoxReceive.ItemText(c.ui.ComboBoxReceive.CurrentIndex())
	params.coinRcvValue = c.ui.DoubleSpinBoxReceive.Value() // float
	return &params
}

func (c *Create) onCheckBoxStateChange(i int) {
	istestnet := c.ui.CheckBoxTestnet.IsChecked()
	logger.Log.Printf("onCheckBoxStateChange %d testnet %v\n", i, istestnet)
	c.ui.ComboBoxSend.Clear()
	c.ui.ComboBoxReceive.Clear()
	//TODO: populate dynamically depending coins available via rpc
	//      for the selected network
	mainnetCoins := appconfig.EnabledMainnetCoins
	testnetCoins := appconfig.EnabledTestnetCoins
	if istestnet {
		c.ui.ComboBoxSend.AddItems(testnetCoins)
		c.ui.ComboBoxReceive.AddItems(testnetCoins)
	} else {
		c.ui.ComboBoxSend.AddItems(mainnetCoins)
		c.ui.ComboBoxReceive.AddItems(mainnetCoins)
	}
	c.ui.ComboBoxSend.SetCurrentIndex(0)
	c.ui.ComboBoxReceive.SetCurrentIndex(0)
}

func (c *Create) onButtonBoxAccepted() {
	logger.Log.Println("accepted")

	// get dialog values
	params := c.getParams()
	//...create a new session
	s, err := newInitiatorSession(c.app, params)
	if err != nil {
		msg := fmt.Sprintf("dbgMsg %v", err)
		widgets.QMessageBox_Warning(nil, "Create session", msg, widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	} else {
		c.app.session = s
	}
	c.setDefaults()
	c.dialog.SetVisible(false)
}

func (c *Create) onButtonBoxRejected() {
	logger.Log.Println("rejected")
	c.setDefaults()
	c.dialog.SetVisible(false)
}

package app

import (
	"fmt"

	"gitlab.com/devwarrior/dragon/logger"

	"github.com/therecipe/qt/widgets"
	"gitlab.com/devwarrior/dragon/app/gui"
	"gitlab.com/devwarrior/dragon/app/local"
)

/////////////////////
// (Re)open dialog //
/////////////////////

// Open dialog
type Open struct {
	app    *AppMain
	dialog *widgets.QDialog
	ui     *gui.UIOpenDialog
}

// NewOpen constructs a Open dialog object
func newOpen(appMain *AppMain) *Open {
	open := Open{app: appMain}
	open.dialog = widgets.NewQDialog(appMain.window, 0)
	open.ui = &gui.UIOpenDialog{}
	return &open
}

// Setup continues construction of the open dialog
func (o *Open) setup() *Open {
	o.ui.SetupUI(o.dialog)
	o.ui.ButtonBox.ConnectAccepted(o.onButtonBoxAccepted)
	o.ui.ButtonBox.ConnectRejected(o.onButtonBoxRejected)
	o.setDefaults()
	o.dialog.SetModal(true)
	return o
}

func (o *Open) center() {
	d := o.dialog
	w := o.app.window
	d.Move2(w.X()+((w.Width()-d.Width())/2), w.Y()+((w.Height()-d.Height())/2))
}

func (o *Open) showDialog() {
	o.dialog.Show()
	o.center()
}

func (o *Open) setDefaults() {
	o.ui.ComboBoxSavedSessions.Clear()
	// Populate dynamically depending local.sessions entries
	sessionIDs := local.GetValidSessionIDs()
	o.ui.ComboBoxSavedSessions.AddItems(sessionIDs)
}

// get dialog selection
func (o *Open) getSessionID() string {
	idx := o.ui.ComboBoxSavedSessions.CurrentIndex()
	sessionID := o.ui.ComboBoxSavedSessions.ItemText(idx)
	return sessionID
}

func (o *Open) onButtonBoxAccepted() {
	logger.Log.Println("accepted")

	// get dialog value
	sessionID := o.getSessionID()
	if len(sessionID) == 0 {
		return //silently
	}
	//...open a new session
	s, err := newReopenedSession(o.app, sessionID)
	if err != nil {
		msg := fmt.Sprintf("Debug: %v", err)
		widgets.QMessageBox_Warning(nil, "Open session", msg, widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	} else {
		o.app.session = s
	}
	o.setDefaults()
	o.dialog.SetVisible(false)
}

func (o *Open) onButtonBoxRejected() {
	logger.Log.Println("rejected")
	o.setDefaults()
	o.dialog.SetVisible(false)
}

package app

//#include <stdint.h>
//#include <stdlib.h>
//#include <string.h>
//#include "moc.h"
import "C"
import (
	"runtime"
	"unsafe"

	"github.com/therecipe/qt"
	std_core "github.com/therecipe/qt/core"
)

func cGoUnpackString(s C.struct_Moc_PackedString) string {
	if int(s.len) == -1 {
		return C.GoString(s.data)
	}
	return C.GoStringN(s.data, C.int(s.len))
}
func cGoUnpackBytes(s C.struct_Moc_PackedString) []byte {
	if int(s.len) == -1 {
		return []byte(C.GoString(s.data))
	}
	return C.GoBytes(unsafe.Pointer(s.data), C.int(s.len))
}

type updater_ITF interface {
	std_core.QObject_ITF
	updater_PTR() *updater
}

func (ptr *updater) updater_PTR() *updater {
	return ptr
}

func (ptr *updater) Pointer() unsafe.Pointer {
	if ptr != nil {
		return ptr.QObject_PTR().Pointer()
	}
	return nil
}

func (ptr *updater) SetPointer(p unsafe.Pointer) {
	if ptr != nil {
		ptr.QObject_PTR().SetPointer(p)
	}
}

func PointerFromUpdater(ptr updater_ITF) unsafe.Pointer {
	if ptr != nil {
		return ptr.updater_PTR().Pointer()
	}
	return nil
}

func NewUpdaterFromPointer(ptr unsafe.Pointer) (n *updater) {
	if gPtr, ok := qt.Receive(ptr); !ok {
		n = new(updater)
		n.SetPointer(ptr)
	} else {
		switch deduced := gPtr.(type) {
		case *updater:
			n = deduced

		case *std_core.QObject:
			n = &updater{QObject: *deduced}

		default:
			n = new(updater)
			n.SetPointer(ptr)
		}
	}
	return
}

//export callbackupdater091c1b_Constructor
func callbackupdater091c1b_Constructor(ptr unsafe.Pointer) {
	this := NewUpdaterFromPointer(ptr)
	qt.Register(ptr, this)
}

//export callbackupdater091c1b_UpdateState
func callbackupdater091c1b_UpdateState(ptr unsafe.Pointer, v0 C.int) {
	if signal := qt.GetSignal(ptr, "updateState"); signal != nil {
		signal.(func(int))(int(int32(v0)))
	}

}

func (ptr *updater) ConnectUpdateState(f func(v0 int)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "updateState") {
			C.updater091c1b_ConnectUpdateState(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "updateState"); signal != nil {
			qt.ConnectSignal(ptr.Pointer(), "updateState", func(v0 int) {
				signal.(func(int))(v0)
				f(v0)
			})
		} else {
			qt.ConnectSignal(ptr.Pointer(), "updateState", f)
		}
	}
}

func (ptr *updater) DisconnectUpdateState() {
	if ptr.Pointer() != nil {
		C.updater091c1b_DisconnectUpdateState(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "updateState")
	}
}

func (ptr *updater) UpdateState(v0 int) {
	if ptr.Pointer() != nil {
		C.updater091c1b_UpdateState(ptr.Pointer(), C.int(int32(v0)))
	}
}

func updater_QRegisterMetaType() int {
	return int(int32(C.updater091c1b_updater091c1b_QRegisterMetaType()))
}

func (ptr *updater) QRegisterMetaType() int {
	return int(int32(C.updater091c1b_updater091c1b_QRegisterMetaType()))
}

func updater_QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.updater091c1b_updater091c1b_QRegisterMetaType2(typeNameC)))
}

func (ptr *updater) QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.updater091c1b_updater091c1b_QRegisterMetaType2(typeNameC)))
}

func updater_QmlRegisterType() int {
	return int(int32(C.updater091c1b_updater091c1b_QmlRegisterType()))
}

func (ptr *updater) QmlRegisterType() int {
	return int(int32(C.updater091c1b_updater091c1b_QmlRegisterType()))
}

func updater_QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.updater091c1b_updater091c1b_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *updater) QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.updater091c1b_updater091c1b_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *updater) __dynamicPropertyNames_atList(i int) *std_core.QByteArray {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQByteArrayFromPointer(C.updater091c1b___dynamicPropertyNames_atList(ptr.Pointer(), C.int(int32(i))))
		runtime.SetFinalizer(tmpValue, (*std_core.QByteArray).DestroyQByteArray)
		return tmpValue
	}
	return nil
}

func (ptr *updater) __dynamicPropertyNames_setList(i std_core.QByteArray_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b___dynamicPropertyNames_setList(ptr.Pointer(), std_core.PointerFromQByteArray(i))
	}
}

func (ptr *updater) __dynamicPropertyNames_newList() unsafe.Pointer {
	return C.updater091c1b___dynamicPropertyNames_newList(ptr.Pointer())
}

func (ptr *updater) __findChildren_atList2(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.updater091c1b___findChildren_atList2(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *updater) __findChildren_setList2(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b___findChildren_setList2(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *updater) __findChildren_newList2() unsafe.Pointer {
	return C.updater091c1b___findChildren_newList2(ptr.Pointer())
}

func (ptr *updater) __findChildren_atList3(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.updater091c1b___findChildren_atList3(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *updater) __findChildren_setList3(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b___findChildren_setList3(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *updater) __findChildren_newList3() unsafe.Pointer {
	return C.updater091c1b___findChildren_newList3(ptr.Pointer())
}

func (ptr *updater) __findChildren_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.updater091c1b___findChildren_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *updater) __findChildren_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b___findChildren_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *updater) __findChildren_newList() unsafe.Pointer {
	return C.updater091c1b___findChildren_newList(ptr.Pointer())
}

func (ptr *updater) __children_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.updater091c1b___children_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *updater) __children_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b___children_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *updater) __children_newList() unsafe.Pointer {
	return C.updater091c1b___children_newList(ptr.Pointer())
}

func NewUpdater(parent std_core.QObject_ITF) *updater {
	tmpValue := NewUpdaterFromPointer(C.updater091c1b_NewUpdater(std_core.PointerFromQObject(parent)))
	if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
		tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
	}
	return tmpValue
}

//export callbackupdater091c1b_DestroyUpdater
func callbackupdater091c1b_DestroyUpdater(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "~updater"); signal != nil {
		signal.(func())()
	} else {
		NewUpdaterFromPointer(ptr).DestroyUpdaterDefault()
	}
}

func (ptr *updater) ConnectDestroyUpdater(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "~updater"); signal != nil {
			qt.ConnectSignal(ptr.Pointer(), "~updater", func() {
				signal.(func())()
				f()
			})
		} else {
			qt.ConnectSignal(ptr.Pointer(), "~updater", f)
		}
	}
}

func (ptr *updater) DisconnectDestroyUpdater() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "~updater")
	}
}

func (ptr *updater) DestroyUpdater() {
	if ptr.Pointer() != nil {
		C.updater091c1b_DestroyUpdater(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

func (ptr *updater) DestroyUpdaterDefault() {
	if ptr.Pointer() != nil {
		C.updater091c1b_DestroyUpdaterDefault(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackupdater091c1b_Event
func callbackupdater091c1b_Event(ptr unsafe.Pointer, e unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "event"); signal != nil {
		return C.char(int8(qt.GoBoolToInt(signal.(func(*std_core.QEvent) bool)(std_core.NewQEventFromPointer(e)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewUpdaterFromPointer(ptr).EventDefault(std_core.NewQEventFromPointer(e)))))
}

func (ptr *updater) EventDefault(e std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.updater091c1b_EventDefault(ptr.Pointer(), std_core.PointerFromQEvent(e))) != 0
	}
	return false
}

//export callbackupdater091c1b_EventFilter
func callbackupdater091c1b_EventFilter(ptr unsafe.Pointer, watched unsafe.Pointer, event unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "eventFilter"); signal != nil {
		return C.char(int8(qt.GoBoolToInt(signal.(func(*std_core.QObject, *std_core.QEvent) bool)(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewUpdaterFromPointer(ptr).EventFilterDefault(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
}

func (ptr *updater) EventFilterDefault(watched std_core.QObject_ITF, event std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.updater091c1b_EventFilterDefault(ptr.Pointer(), std_core.PointerFromQObject(watched), std_core.PointerFromQEvent(event))) != 0
	}
	return false
}

//export callbackupdater091c1b_ChildEvent
func callbackupdater091c1b_ChildEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "childEvent"); signal != nil {
		signal.(func(*std_core.QChildEvent))(std_core.NewQChildEventFromPointer(event))
	} else {
		NewUpdaterFromPointer(ptr).ChildEventDefault(std_core.NewQChildEventFromPointer(event))
	}
}

func (ptr *updater) ChildEventDefault(event std_core.QChildEvent_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b_ChildEventDefault(ptr.Pointer(), std_core.PointerFromQChildEvent(event))
	}
}

//export callbackupdater091c1b_ConnectNotify
func callbackupdater091c1b_ConnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "connectNotify"); signal != nil {
		signal.(func(*std_core.QMetaMethod))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewUpdaterFromPointer(ptr).ConnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *updater) ConnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b_ConnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackupdater091c1b_CustomEvent
func callbackupdater091c1b_CustomEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "customEvent"); signal != nil {
		signal.(func(*std_core.QEvent))(std_core.NewQEventFromPointer(event))
	} else {
		NewUpdaterFromPointer(ptr).CustomEventDefault(std_core.NewQEventFromPointer(event))
	}
}

func (ptr *updater) CustomEventDefault(event std_core.QEvent_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b_CustomEventDefault(ptr.Pointer(), std_core.PointerFromQEvent(event))
	}
}

//export callbackupdater091c1b_DeleteLater
func callbackupdater091c1b_DeleteLater(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "deleteLater"); signal != nil {
		signal.(func())()
	} else {
		NewUpdaterFromPointer(ptr).DeleteLaterDefault()
	}
}

func (ptr *updater) DeleteLaterDefault() {
	if ptr.Pointer() != nil {
		C.updater091c1b_DeleteLaterDefault(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackupdater091c1b_Destroyed
func callbackupdater091c1b_Destroyed(ptr unsafe.Pointer, obj unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "destroyed"); signal != nil {
		signal.(func(*std_core.QObject))(std_core.NewQObjectFromPointer(obj))
	}

}

//export callbackupdater091c1b_DisconnectNotify
func callbackupdater091c1b_DisconnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "disconnectNotify"); signal != nil {
		signal.(func(*std_core.QMetaMethod))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewUpdaterFromPointer(ptr).DisconnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *updater) DisconnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b_DisconnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackupdater091c1b_ObjectNameChanged
func callbackupdater091c1b_ObjectNameChanged(ptr unsafe.Pointer, objectName C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "objectNameChanged"); signal != nil {
		signal.(func(string))(cGoUnpackString(objectName))
	}

}

//export callbackupdater091c1b_TimerEvent
func callbackupdater091c1b_TimerEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "timerEvent"); signal != nil {
		signal.(func(*std_core.QTimerEvent))(std_core.NewQTimerEventFromPointer(event))
	} else {
		NewUpdaterFromPointer(ptr).TimerEventDefault(std_core.NewQTimerEventFromPointer(event))
	}
}

func (ptr *updater) TimerEventDefault(event std_core.QTimerEvent_ITF) {
	if ptr.Pointer() != nil {
		C.updater091c1b_TimerEventDefault(ptr.Pointer(), std_core.PointerFromQTimerEvent(event))
	}
}

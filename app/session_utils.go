package app

///////////////////
// session utils //
///////////////////

import (
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"math/rand"
	"time"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"

	"github.com/devwarrior777/atomicswap/libs"

	"gitlab.com/devwarrior/dragon/app/wallets"
	"gitlab.com/devwarrior/dragon/appconfig"
	pb "gitlab.com/devwarrior/dragon/session"
)

const hexstr32len = 32 * 2

// getRand32 creates a 32-'byte' pseudo random hex string
func getRand32() string {
	src := rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, 32)
	_, _ = src.Read(b)
	return hex.EncodeToString(b)
}

// hash256 takes a 32-'byte' hex string and hashes the binary number
// represented then outputs as a hex string ready for database input
func hash256(s string) (string, error) {
	if len(s) != hexstr32len {
		return "", errors.New("hash256 - invalid input")
	}
	b, err := hex.DecodeString(s)
	if err != nil {
		return "", err
	}
	if len(b) != 32 {
		return "", errors.New("hash256 - invalid input byte length")
	}
	h32 := sha256.Sum256(b)
	return hex.EncodeToString(h32[:]), nil
}

// sessionIsDeleted finds sessions marked as deleted
func sessionIsDeleted(state pb.STATE) bool {
	if state == pb.STATE_MarkedDeleted {
		return true
	}
	return false
}

// walletForCoin gets a concrete wallet for a coin name
func walletForCoin(testnet bool, coinName string) (wallets.Wallet, error) {
	for k, v := range appconfig.CoinsConfig {
		if k == coinName {
			rpcInfo := libs.RPCInfo{}
			if testnet {
				if !v.EnabledTestnet {
					return nil, fmt.Errorf("testnet %s not enabled", coinName)
				}
				rpcInfo.WalletPass = ""
				// If the testnet wallet is encrypted ask the user for the passphrase
				if v.WalletEncryptedTestnet {
					wpass, err := getWalletPass(testnet, coinName)
					if err != nil {
						return nil, err
					}
					rpcInfo.WalletPass = wpass
				}
				rpcInfo.HostPort = v.HostPortTestnet
				rpcInfo.User = v.UserTestnet
				rpcInfo.Pass = v.PassTestnet
				rpcInfo.Certs = v.CertsTestnet
				return wallets.WalletForCoin(testnet, rpcInfo, coinName)
			}
			// Mainnet
			if !v.EnabledMainnet {
				return nil, fmt.Errorf("mainnet %s not enabled", coinName)
			}
			rpcInfo.WalletPass = ""
			// If the mainnet wallet is encrypted ask the user for the passphrase
			if v.WalletEncryptedMainnet {
				wpass, err := getWalletPass(testnet, coinName)
				if err != nil {
					return nil, err
				}
				rpcInfo.WalletPass = wpass
			}
			rpcInfo.HostPort = v.HostPortMainnet
			rpcInfo.User = v.UserMainnet
			rpcInfo.Pass = v.PassMainnet
			rpcInfo.Certs = v.CertsMainnet
			return wallets.WalletForCoin(testnet, rpcInfo, coinName)
		}
	}
	return nil, fmt.Errorf("unsupported coin %s", coinName)
}

// getWalletPass gets the wallet passphrase from user input
func getWalletPass(testnet bool, coinName string) (string, error) {
	title := "password"
	net := ""
	if testnet {
		net = "[testnet]"
	}
	label := fmt.Sprintf("wallet password for %s %s", coinName, net)
	var ok bool
	wpass := widgets.QInputDialog_GetText(
		nil, title, label, widgets.QLineEdit__Password, "", &ok, core.Qt__Dialog, core.Qt__ImhNone)
	if !ok {
		return "", errors.New("password input cancelled")
	}
	if len(wpass) == 0 {
		return "", errors.New("blank password")
	}
	return wpass, nil
}

func infoMsg(title, msg string) {
	widgets.QMessageBox_Information(
		nil, title, msg, widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
}

func warnMsg(title, msg string) {
	widgets.QMessageBox_Warning(
		nil, title, msg, widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
}

func errMsg(title, msg string) {
	widgets.QMessageBox_Critical(
		nil, title, msg, widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
}

func okMsg(title, question string) bool {
	pressed := widgets.QMessageBox_Question(
		nil, title, question, widgets.QMessageBox__Cancel, widgets.QMessageBox__Ok)
	if pressed == widgets.QMessageBox__Cancel {
		return false
	}
	return true
}

// dbnetwork prepares a 'network' parameter for database access
func dbnetwork(testnet bool) pb.NET_TYPE {
	if testnet {
		return pb.NET_TYPE_Testnet
	}
	return pb.NET_TYPE_Mainnet
}

// istestnet decides if a database 'network' is a testnet
func istestnet(network pb.NET_TYPE) bool {
	if network == pb.NET_TYPE_Testnet {
		return true
	}
	return false
}

// dbcoin converts a coin name to db representation
func dbcoin(coinName string) pb.COIN {
	return pb.COIN(pb.COIN_value[coinName])
}

// coinname gets the coin name from db representation
func coinname(dbCoin pb.COIN) string {
	return pb.COIN_name[int32(dbCoin)]
}

var uirole = map[role]string{
	initiator:   "Maker",
	participant: "Taker",
}

var sessionrole = map[string]role{
	"Maker": initiator,
	"Taker": participant,
}

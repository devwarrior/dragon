package app

import (
	"fmt"

	"gitlab.com/devwarrior/dragon/logger"

	"github.com/therecipe/qt/core"
	qtgui "github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/devwarrior/dragon/app/gui"
	pb "gitlab.com/devwarrior/dragon/session"
)

////////////////////
// Confirm dialog //
////////////////////

// Confirm dialog
type Confirm struct {
	app    *AppMain
	dialog *widgets.QDialog
	ui     *gui.UIConfirmationDialog
	timer  *core.QTimer
	txid   string
}

// newConfirm constructs a Confirm dialog object
func newConfirm(appMain *AppMain, txid string) *Confirm {
	confirm := &Confirm{app: appMain}
	confirm.dialog = widgets.NewQDialog(appMain.window, core.Qt__Dialog)
	confirm.ui = &gui.UIConfirmationDialog{}
	confirm.timer = core.NewQTimer(confirm.dialog)
	confirm.txid = txid
	return confirm
}

// setup continues construction of the confirm dialog
func (c *Confirm) setup() *Confirm {
	c.ui.SetupUI(c.dialog)
	c.dialog.ConnectCloseEvent(c.closeX)
	c.ui.PushButtonOk.ConnectClicked(c.onOk)
	c.ui.PushButtonCancel.ConnectClicked(c.onCancel)
	c.timer.ConnectTimeout(c.onTick)
	c.timer.Start(5000)
	c.dialog.SetModal(true)
	c.dialog.SetVisible(false)
	return c
}

func (c *Confirm) center() {
	d := c.dialog
	w := c.app.window
	d.Move2(w.X()+((w.Width()-d.Width())/2), w.Y()+((w.Height()-d.Height())/2))
}

func (c *Confirm) show() {
	c.dialog.SetVisible(true)
	c.center()
	c.ui.PlainTextEditLog.SetPlainText("TXID: " + c.txid)
}

func (c *Confirm) closeX(e *qtgui.QCloseEvent) {
	// clean up dialog
	c.endDialog()
	e.Accept()
}

func (c *Confirm) onTick() {
	// logger.Log.Println("Tick")
	s := c.app.session
	result, err := s.sndWallet.GetTx(c.txid)
	if err != nil {
		errstr := fmt.Sprintf("%v", err)
		c.ui.PlainTextEditLog.AppendPlainText(errstr)
		return
	}
	if result.Confirmations == 0 {
		c.ui.PlainTextEditLog.AppendPlainText("Confirmations: 0")
		return
	}
	msg := fmt.Sprintf("Confirmations: %d", result.Confirmations)
	c.ui.PlainTextEditLog.AppendPlainText(msg)
	c.ui.PushButtonOk.SetEnabled(true)
}

func (c *Confirm) onOk(bool) {
	logger.Log.Println("Ok")
	s := c.app.session
	r := s.role

	var err error
	if r == initiator {
		// update db state to PartRedeemableContractConfirmed (8)
		request := &pb.InitSetPartRedeemableContractConfirmedRequest{Session: s.sessionID}
		_, err = dbInitSetPartRedeemableContractConfirmed(request)
		if err != nil {
			logger.Log.Printf("dbInitSetPartRedeemableContractConfirmed failed - %v \n", err)
		}
	} else { // participant
		// update db state to InitRedeemableContractConfirmed (9)
		request := &pb.PartSetInitRedeemableContractConfirmedRequest{Session: s.sessionID}
		_, err = dbPartSetInitRedeemableContractConfirmed(request)
		if err != nil {
			logger.Log.Printf("dbPartSetInitRedeemableContractConfirmed failed - %v \n", err)
		}
	}

	if err != nil {
		warnMsg("error", "cannot set confirmation")
		c.endDialog()
		return
	}
	// poller updates gui
	s.poller.checkPoll()
	// clean up dialog
	c.endDialog()
}

func (c *Confirm) onCancel(bool) {
	logger.Log.Println("Cancel dialog")
	// clean up dialog
	c.endDialog()
}

func (c *Confirm) endDialog() {
	logger.Log.Println("endDialog")
	c.dialog.SetVisible(false)
	c.timer.Stop()
	c.timer.DestroyQTimer()
}

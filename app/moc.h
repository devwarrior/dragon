

#pragma once

#ifndef GO_MOC_091c1b_H
#define GO_MOC_091c1b_H

#include <stdint.h>

#ifdef __cplusplus
class updater091c1b;
void updater091c1b_updater091c1b_QRegisterMetaTypes();
extern "C" {
#endif

struct Moc_PackedString { char* data; long long len; };
struct Moc_PackedList { void* data; long long len; };
void updater091c1b_ConnectUpdateState(void* ptr);
void updater091c1b_DisconnectUpdateState(void* ptr);
void updater091c1b_UpdateState(void* ptr, int v0);
int updater091c1b_updater091c1b_QRegisterMetaType();
int updater091c1b_updater091c1b_QRegisterMetaType2(char* typeName);
int updater091c1b_updater091c1b_QmlRegisterType();
int updater091c1b_updater091c1b_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName);
void* updater091c1b___dynamicPropertyNames_atList(void* ptr, int i);
void updater091c1b___dynamicPropertyNames_setList(void* ptr, void* i);
void* updater091c1b___dynamicPropertyNames_newList(void* ptr);
void* updater091c1b___findChildren_atList2(void* ptr, int i);
void updater091c1b___findChildren_setList2(void* ptr, void* i);
void* updater091c1b___findChildren_newList2(void* ptr);
void* updater091c1b___findChildren_atList3(void* ptr, int i);
void updater091c1b___findChildren_setList3(void* ptr, void* i);
void* updater091c1b___findChildren_newList3(void* ptr);
void* updater091c1b___findChildren_atList(void* ptr, int i);
void updater091c1b___findChildren_setList(void* ptr, void* i);
void* updater091c1b___findChildren_newList(void* ptr);
void* updater091c1b___children_atList(void* ptr, int i);
void updater091c1b___children_setList(void* ptr, void* i);
void* updater091c1b___children_newList(void* ptr);
void* updater091c1b_NewUpdater(void* parent);
void updater091c1b_DestroyUpdater(void* ptr);
void updater091c1b_DestroyUpdaterDefault(void* ptr);
char updater091c1b_EventDefault(void* ptr, void* e);
char updater091c1b_EventFilterDefault(void* ptr, void* watched, void* event);
void updater091c1b_ChildEventDefault(void* ptr, void* event);
void updater091c1b_ConnectNotifyDefault(void* ptr, void* sign);
void updater091c1b_CustomEventDefault(void* ptr, void* event);
void updater091c1b_DeleteLaterDefault(void* ptr);
void updater091c1b_DisconnectNotifyDefault(void* ptr, void* sign);
void updater091c1b_TimerEventDefault(void* ptr, void* event);
;

#ifdef __cplusplus
}
#endif

#endif
package app

import (
	"fmt"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/devwarrior/dragon/app/gui"
	pb "gitlab.com/devwarrior/dragon/session"
)

///////////////////
// Review dialog //
///////////////////

// Review dialog
type Review struct {
	app    *AppMain
	dialog *widgets.QDialog
	ui     *gui.UIDataDialog
}

// NewReview constructs an Review dialog object
func newReview(appMain *AppMain) *Review {
	review := &Review{app: appMain}
	review.dialog = widgets.NewQDialog(appMain.window, core.Qt__Dialog)
	review.ui = &gui.UIDataDialog{}
	return review
}

// Setup continues construction of the review dialog
func (r *Review) setup(dataResponse *pb.DataResponse) *Review {
	r.ui.SetupUI(r.dialog)
	r.populate(dataResponse)
	r.dialog.SetModal(true)
	r.dialog.SetVisible(false)
	return r
}

func (r *Review) populate(dataResponse *pb.DataResponse) {
	data := dataResponse.Data
	var html string
	html = "<html>"
	html += "<b><u>Session:</u></b>"
	html += "<table  border = '0' cellpadding = '3' cellspacing = '5'>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Session:</b>"
	html += "</td>"
	html += "<td>"
	html += data.Session
	html += "</td>"
	html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Secret Hash:</b>"
	html += "</td>"
	html += "<td>"
	html += data.Secrethash
	html += "</td>"
	html += "</tr>"
	// html += "<tr>"
	// html += "<td>"
	// html += "<b>Secret:</b>"
	// html += "</td>"
	// html += "<td>"
	// // html +=         self.ui_data.get('secret')
	// html += "</td>"
	// html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "</td>"
	html += "<td>"
	html += "</td>"
	html += "</tr>"
	html += "</table>"
	html += "<b><u>Maker Contract for Taker Redemption:</u></b>"
	html += "<br/>"
	html += "<table  border = '0' cellpadding = '3' cellspacing = '5'>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Contract Address:</b>"
	html += "</td>"
	html += "<td>"
	html += data.PartRedeemableContractP2Sh
	html += "</td>"
	html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Contract Value:</b>"
	html += "</td>"
	html += "<td>"
	coin := coinname(dataResponse.Data.PartCoinRcv)
	amount := Amount(dataResponse.Data.PartCoinRcvValue)
	html += fmt.Sprintf("%0.08f %s", amount.ToCoins(), coin)
	html += "</td>"
	html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Recipient Address:</b>"
	html += "</td>"
	html += "<td>"
	html += data.Participant
	html += "</td>"
	html += "</tr>"
	// html += "<tr>"
	// html += "<td>"
	// html += "<b>Author's Refund Address:</b>"
	// html += "</td>"
	// html += "<td>"
	// // html +=         self.ui_data.get('init_refund_addr')
	// html += "</td>"
	// html += "</tr>"
	// html += "<tr>"
	// html += "<td>"
	// html += "<b>Refund Locktime:</b>"
	// html += "</td>"
	// html += "<td>"
	// // html +=         self.ui_data.get('init_refund_locktime')
	// html += "</td>"
	// html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "</td>"
	html += "<td>"
	html += "</td>"
	html += "</tr>"
	html += "</table>"
	html += "<b><u>Taker Contract for Maker Redemption:</u></b>"
	html += "<br/>"
	html += "<table  border = '0' cellpadding = '3' cellspacing = '5'>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Contract Address:</b>"
	html += "</td>"
	html += "<td>"
	html += data.InitRedeemableContractP2Sh
	html += "</td>"
	html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Contract Value:</b>"
	html += "</td>"
	html += "<td>"
	coin2 := coinname(dataResponse.Data.InitCoinRcv)
	amount2 := Amount(dataResponse.Data.InitCoinRcvValue)
	html += fmt.Sprintf("%0.08f %s", amount2.ToCoins(), coin2)
	html += "</td>"
	html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Recipient Address:</b>"
	html += "</td>"
	html += "<td>"
	html += data.Initiator
	html += "</td>"
	html += "</tr>"
	// html += "<tr>"
	// html += "<td>"
	// html += "<b>Author's Refund Address:</b>"
	// html += "</td>"
	// html += "<td>"
	// // html +=         self.ui_data.get('part_refund_addr')
	// html += "</td>"
	// html += "</tr>"
	// html += "<tr>"
	// html += "<td>"
	// html += "<b>Refund Locktime:</b>"
	// html += "</td>"
	// html += "<td>"
	// // html +=         self.ui_data.get('part_refund_locktime')
	// html += "</td>"
	// html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "</td>"
	html += "<td>"
	html += "</td>"
	html += "</tr>"
	html += "</table>"
	html += "<b><u>Maker Redeem Transaction:</u></b>"
	html += "<br/>"
	html += "<table  border = '0' cellpadding = '3' cellspacing = '5'>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Transaction:</b>"
	html += "</td>"
	html += "<td>"
	html += data.InitRedeemTxid
	html += "</td>"
	html += "</tr>"
	// html += "<tr>"
	// html += "<td>"
	// html += "<b>Receive Address:</b>"
	// html += "</td>"
	// html += "<td>"
	// html += data.Initiator
	// html += "</td>"
	// html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "</td>"
	html += "<td>"
	html += "</td>"
	html += "</tr>"
	html += "</table>"
	html += "</table>"
	html += "<b><u>Taker Redeem Transaction:</u></b>"
	html += "<br/>"
	html += "<table  border = '0' cellpadding = '3' cellspacing = '5'>"
	html += "<tr>"
	html += "<td>"
	html += "<b>Transaction:</b>"
	html += "</td>"
	html += "<td>"
	html += data.PartRedeemTxid
	html += "</td>"
	html += "</tr>"
	// html += "<tr>"
	// html += "<td>"
	// html += "<b>Receive Address:</b>"
	// html += "</td>"
	// html += "<td>"
	// html += data.Participant
	// html += "</td>"
	// html += "</tr>"
	html += "<tr>"
	html += "<td>"
	html += "</td>"
	html += "<td>"
	html += "</td>"
	html += "</tr>"
	html += "</table>"
	html += "<html>"

	// fmt.Println(html)
	r.ui.TextEditData.SetHtml(html)
}

func (r *Review) center() {
	d := r.dialog
	w := r.app.window
	d.Move2(w.X()+((w.Width()-d.Width())/2), w.Y()+((w.Height()-d.Height())/2))
}

func (r *Review) show() {
	r.dialog.SetVisible(true)
	r.center()
}

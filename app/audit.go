package app

import (
	"gitlab.com/devwarrior/dragon/logger"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/devwarrior/dragon/app/gui"
	pb "gitlab.com/devwarrior/dragon/session"
)

//////////////////
// Audit dialog //
//////////////////

// Audit dialog
type Audit struct {
	app    *AppMain
	dialog *widgets.QDialog
	ui     *gui.UIAuditDialog
}

// NewAudit constructs an Audit dialog object
func newAudit(appMain *AppMain) *Audit {
	audit := &Audit{app: appMain}
	audit.dialog = widgets.NewQDialog(appMain.window, core.Qt__Dialog)
	audit.ui = &gui.UIAuditDialog{}
	return audit
}

// Setup continues construction of the audit dialog
func (a *Audit) setup(contractAddr, contractAmount, recipientAddr, contractLocktime string) *Audit {
	a.ui.SetupUI(a.dialog)
	a.setFields(contractAddr, contractAmount, recipientAddr, contractLocktime)
	a.ui.ButtonBoxAcceptAudit.ConnectAccepted(a.onButtonBoxAccepted)
	a.ui.ButtonBoxAcceptAudit.ConnectRejected(a.onButtonBoxRejected)
	a.dialog.SetModal(true)
	a.dialog.SetVisible(false)
	return a
}

func (a *Audit) setFields(contractAddr, contractAmount, recipientAddr, contractLocktime string) {
	a.ui.ContractAddressLineEdit.SetText(contractAddr)
	a.ui.ContractValueLineEdit.SetText(contractAmount)
	a.ui.RecipientAddressLineEdit.SetText(recipientAddr)
	a.ui.RefundLocktimeLineEdit.SetText(contractLocktime)
}

func (a *Audit) center() {
	d := a.dialog
	w := a.app.window
	d.Move2(w.X()+((w.Width()-d.Width())/2), w.Y()+((w.Height()-d.Height())/2))
}

func (a *Audit) show() {
	a.dialog.SetVisible(true)
	a.center()
}

func (a *Audit) onButtonBoxAccepted() {
	logger.Log.Println("accepted")
	s := a.app.session
	r := s.role

	var err error
	if r == participant {
		// update db state to PartRedeemableContractAccepted (3)
		request := &pb.PartSetPartRedeemableContractAcceptanceRequest{Session: s.sessionID}
		_, err = dbPartSetPartRedeemableContractAcceptance(request)
		if err != nil {
			logger.Log.Printf("dbPartSetPartRedeemableContractAcceptance failed - %v \n", err)
		}
	} else { // initiator
		// update db state to InitRedeemableContractAccepted (5)
		request := &pb.InitSetInitRedeemableContractAcceptanceRequest{Session: s.sessionID}
		_, err = dbInitSetInitRedeemableContractAcceptance(request)
		if err != nil {
			logger.Log.Printf("dbInitSetInitRedeemableContractAcceptance failed - %v \n", err)
		}
	}

	if err != nil {
		warnMsg("error", "cannot set acceptance")
		a.dialog.SetVisible(false)
		return
	}
	// poller updates gui
	s.poller.checkPoll()

	a.dialog.SetVisible(false)
}

func (a *Audit) onButtonBoxRejected() {
	logger.Log.Println("rejected")
	// this is the same as clicking delete session on the main window ui
	a.app.onActionDeleteTriggered(true)
}

package main

import (
	"os"

	"github.com/therecipe/qt/widgets"
	"gitlab.com/devwarrior/dragon/app"
	"gitlab.com/devwarrior/dragon/logger"
)

//////////////////////////////////////
// Dragon GUI swap tool entry point //
//////////////////////////////////////

func main() {
	// needs to be called once before you can start using the QWidgets
	qtApp := widgets.NewQApplication(len(os.Args), os.Args)

	// Create the basic main top level window
	appMain := app.NewAppMain(qtApp)

	// Load local.sessions list
	err := appMain.LoadLocal()
	if err != nil {
		logger.Log.Fatalf("loading local sessions list failed: %v", err)
	}
	logger.Log.Println("loaded local sessions list ok")

	// Start up the UI for the main top level window
	appMain.Start()

	// start the main Qt event loop
	qtApp.Exec()
}

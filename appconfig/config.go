package appconfig

import (
	"fmt"
	"os"
	"strings"

	"github.com/go-ini/ini"
)

const (
	configFile = "config.ini"
)

type config struct {
	AppMode string
	// [client]
	UseTLS       bool
	CertPath     string
	ServerAddr   string
	ServerPort   int
	HostOverride string
}

// Config is the exported configuration
var Config = &config{}

// CoinConfig is configuration info for a specific coin's wallet node(s)
type CoinConfig struct {
	EnabledMainnet         bool   // wallet node exists
	HostPortMainnet        string // HostPort host[:port], if no port then default coin port is used
	UserMainnet            string // RPC user 'name'
	PassMainnet            string // RPC password
	CertsMainnet           string // DCR Wallet certificate path
	WalletEncryptedMainnet bool
	//
	EnabledTestnet         bool
	HostPortTestnet        string
	UserTestnet            string
	PassTestnet            string
	CertsTestnet           string
	WalletEncryptedTestnet bool
}

// CoinsConfig is the configuration info for all coins
var CoinsConfig = make(map[string]*CoinConfig)

// EnabledMainnetCoins is a list of enabled mainnet coins
var EnabledMainnetCoins = []string{}

// EnabledTestnetCoins is a list of enabled mainnet coins
var EnabledTestnetCoins = []string{}

// sets up the exported configuration for any packages that import this config pkg
func init() {
	cfg, err := ini.Load(configFile)
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	// [DEFAULT]
	Config.AppMode = cfg.Section("").Key("app_mode").String()

	// [client]
	clientSection := cfg.Section("client")
	Config.UseTLS = clientSection.Key("use_tls").MustBool(false)
	Config.CertPath = clientSection.Key("cert_path").String()
	Config.ServerAddr = clientSection.Key("server_addr").String()
	Config.ServerPort = clientSection.Key("server_port").MustInt(10000)
	Config.HostOverride = clientSection.Key("host_override").String()

	fmt.Printf("Config: %v\n", Config)

	// [coin.XXX]
	for n, coinSection := range cfg.ChildSections("coin") {
		p := strings.Split(coinSection.Name(), ".")
		// perhaps we should just panic here
		if len(p) < 2 {
			continue
		}
		coinName := strings.ToUpper(p[1])
		fmt.Printf("%d) %s\n", n, coinName)
		coinCfg := &CoinConfig{}

		coinCfg.EnabledMainnet = coinSection.Key("enabled_mainnet").MustBool(false)
		if coinCfg.EnabledMainnet {
			EnabledMainnetCoins = append(EnabledMainnetCoins, coinName)
		}
		coinCfg.HostPortMainnet = coinSection.Key("hostport_mainnet").String()
		coinCfg.UserMainnet = coinSection.Key("rpcuser_mainnet").String()
		coinCfg.PassMainnet = coinSection.Key("rpcpass_mainnet").String()
		coinCfg.CertsMainnet = coinSection.Key("certs_mainnet").String()
		coinCfg.WalletEncryptedMainnet = coinSection.Key("wallet_encrypted_mainnet").MustBool(true)

		coinCfg.EnabledTestnet = coinSection.Key("enabled_testnet").MustBool(false)
		if coinCfg.EnabledTestnet {
			EnabledTestnetCoins = append(EnabledTestnetCoins, coinName)
		}
		coinCfg.HostPortTestnet = coinSection.Key("hostport_testnet").String()
		coinCfg.UserTestnet = coinSection.Key("rpcuser_testnet").String()
		coinCfg.PassTestnet = coinSection.Key("rpcpass_testnet").String()
		coinCfg.CertsTestnet = coinSection.Key("certs_testnet").String()
		coinCfg.WalletEncryptedTestnet = coinSection.Key("wallet_encrypted_testnet").MustBool(true)
		CoinsConfig[coinName] = coinCfg
	}

	for k, v := range CoinsConfig {
		fmt.Printf("CoinsConfig[%v]: %v\n", k, v)
	}
	fmt.Printf("EnabledMainnetCoins: %v\n", EnabledMainnetCoins)
	fmt.Printf("EnabledTestnetCoins: %v\n", EnabledTestnetCoins)
}

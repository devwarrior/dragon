package testdata

import (
	"encoding/hex"
	"fmt"
	"math/rand"
	"time"

	pb "gitlab.com/devwarrior/dragon/session"
)

////////////////
// TEST UTILS //
////////////////

// GetRandSessionID creates a pseudo random session id
func GetRandSessionID() string {
	src := rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, 32)
	_, _ = src.Read(b)
	return hex.EncodeToString(b)[:]
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// DumpData dumps data
func DumpData(response *pb.DataResponse) {
	if response == nil {
		return
	}
	fmt.Printf("-------------------------------\n")
	fmt.Printf("Errno %v\n", response.Errno)
	fmt.Printf("Session %v\n", response.Data.Session)
	fmt.Printf("State %v\n", response.Data.State)
	fmt.Printf("Initiator %v\n", response.Data.Initiator)
	fmt.Printf("Participant %v\n", response.Data.Participant)
	fmt.Printf("Secrethash %v\n", response.Data.Secrethash)
	fmt.Printf("Network %v\n", response.Data.Network)
	fmt.Printf("InitCoinRcv %v\n", response.Data.InitCoinRcv)
	fmt.Printf("InitCoinRcvValue %d\n", response.Data.InitCoinRcvValue)
	fmt.Printf("PartCoinRcv %v\n", response.Data.PartCoinRcv)
	fmt.Printf("PartCoinRcvValue %d\n", response.Data.PartCoinRcvValue)
	lenIrc := len(response.Data.InitRedeemableContract)
	fmt.Printf("InitRedeemableContract %s...\n", response.Data.InitRedeemableContract[0:min(lenIrc, 96)])
	lenIrct := len(response.Data.InitRedeemableContractTx)
	fmt.Printf("InitRedeemableContractTx %s...\n", response.Data.InitRedeemableContractTx[0:min(lenIrct, 64)])
	fmt.Printf("InitRedeemableContractP2Sh %s\n", response.Data.InitRedeemableContractP2Sh)
	fmt.Printf("InitRedeemableContractTxid %v\n", response.Data.InitRedeemableContractTxid)
	fmt.Printf("InitRedeemableContractLocktime %d\n", response.Data.InitRedeemableContractLocktime)
	lenPrc := len(response.Data.PartRedeemableContract)
	fmt.Printf("PartRedeemableContract %s...\n", response.Data.PartRedeemableContract[0:min(lenPrc, 96)])
	lenPrct := len(response.Data.PartRedeemableContractTx)
	fmt.Printf("PartRedeemableContractTx %s...\n", response.Data.PartRedeemableContractTx[0:min(lenPrct, 64)])
	fmt.Printf("PartRedeemableContractP2Sh %s\n", response.Data.PartRedeemableContractP2Sh)
	fmt.Printf("PartRedeemableContractTxid %v\n", response.Data.PartRedeemableContractTxid)
	fmt.Printf("PartRedeemableContractLocktime %d\n", response.Data.PartRedeemableContractLocktime)
	fmt.Printf("InitRedeemTxid %v\n", response.Data.InitRedeemTxid)
	fmt.Printf("OptSsig %v\n", response.Data.OptSsig)
	fmt.Printf("PartRedeemTxid %v\n", response.Data.PartRedeemTxid)
	fmt.Printf("-------------------------------\n")
}

///////////////
// TEST DATA //
///////////////

var DataRequest pb.DataRequest = pb.DataRequest{
	Session: "deadbeef",
}

var StateRequest pb.StateRequest = pb.StateRequest{
	Session: "deadbeef",
}

var NewSessionRequest pb.NewSessionRequest = pb.NewSessionRequest{
	Session:          "deadbeef",
	Network:          pb.NET_TYPE_Testnet,
	Initiator:        "misX5t37fKpqLHRXeRrjS3DGZ6UAjUt2zw",
	Secrethash:       "ffffffffffffffffffffffffffffffff",
	InitCoinRcv:      pb.COIN_LTC,
	InitCoinRcvValue: 10000000,
	PartCoinRcv:      pb.COIN_XZC,
	PartCoinRcvValue: 70000000,
}

var AddParticipantRequest pb.AddParticipantRequest = pb.AddParticipantRequest{
	Session:     "deadbeef",
	Participant: "TCzsMYF2Gv7cV3fyHzibDaYeNXXGyU2fYZ",
}

// Swap state machine - 1. Make and approve contracts

var InitStorePartRedeemableContractRequest pb.InitStorePartRedeemableContractRequest = pb.InitStorePartRedeemableContractRequest{
	Session:                        "deadbeef",
	PartRedeemableContract:         "6382012088a820f57c931636dca87e592042f08a0c91ec74aa3d07af962123335d23bdf8c52c048876a9142138cf12a66d1155f446063b3805c397e92520276704b3a6835cb17576a9141c67b93eede9a11e9c2e601eabdf75b7ec1892ad6888ac",
	PartRedeemableContractTx:       "01000000015b45692ec30ffdff6791da8baa778f8033c358baff155de2d1a8238b0db81aa4010000006a47304402204cb618b0ca79cfa89545703dc87a40a74897a8a5af6520ee29872e3f9f3ce5d302207bd0003b623d4751f55a2702d9cf5ec14bbdf039226026256a61e2841f34bfd4012103ab599b5ac57cb3fcf40242d7b6eb6c8a653d24cb2f6826a31cfeedd8711ac978feffffff0220606204320000001976a9148006957b3570dc3b2dfffb0177859691bb96f4c088ac801d2c040000000017a914a9287228afff25b443b85e568f76749bf1e35afa8700000000",
	PartRedeemableContractP2Sh:     "P2SH_FOR_PARTICIPANT",
	PartRedeemableContractLocktime: 1556530077,
}

var PartSetPartRedeemableContractAcceptanceRequest pb.PartSetPartRedeemableContractAcceptanceRequest = pb.PartSetPartRedeemableContractAcceptanceRequest{
	Session: "deadbeef",
}

var PartStoreInitRedeemableContractRequest pb.PartStoreInitRedeemableContractRequest = pb.PartStoreInitRedeemableContractRequest{
	Session:                        "deadbeef",
	InitRedeemableContract:         "6382012088a820f57c931636dca87e592042f08a0c91ec74aa3d07af962123335d23bdf8c52c048876a91424cc11828193bbb0d25d5db2f34cb9034d8c01706704fb59825cb17576a914870597013b5f3c50364b95d15ec058473711c0756888ac",
	InitRedeemableContractTx:       "020000000102ec158ae8fde2f94c10d85b178c5bd538ebe30b4d3dfbe8af4bbdb47a3f53fe000000006b483045022100a46733af1fa2fc85f11a20fe1ef945fc747d9130b85a7b5e675acd1e41d8d7fb022002008bed159531f36de69b28334c321c09a598282ca4e66a9ac74d6346b5e4010121025ff30ef6d5cda161cf752de181922e7616caf1f4659e1dec5e7037fed23dfa69feffffff02809698000000000017a91420e082f2137a95f4cd62bba5d70c65278681167e87f747a700000000001976a914cb6379eb868a2ccae5b3c886eed5e9244ad00aae88ac00000000",
	InitRedeemableContractP2Sh:     "P2SH_FOR_INITIATOR",
	InitRedeemableContractLocktime: 1556530078,
}

var InitSetInitRedeemableContractAcceptanceRequest pb.InitSetInitRedeemableContractAcceptanceRequest = pb.InitSetInitRedeemableContractAcceptanceRequest{
	Session: "deadbeef",
}

// Swap state machine - 2. Publish and confirm contracts

var InitStorePublishedPartRedeemableContractTxidRequest pb.InitStorePublishedPartRedeemableContractTxidRequest = pb.InitStorePublishedPartRedeemableContractTxidRequest{
	Session:                    "deadbeef",
	PartRedeemableContractTxid: "0bec366fe53e2f561b86802834e39aa5b7ea37d627da9d753b41eeb1e46b6bc2",
}

var PartStorePublishedInitRedeemableContractTxidRequest pb.PartStorePublishedInitRedeemableContractTxidRequest = pb.PartStorePublishedInitRedeemableContractTxidRequest{
	Session:                    "deadbeef",
	InitRedeemableContractTxid: "a529bdf50e1253070004c080fff3e099102a44236f95910f8ec8956dbfe3cf37",
}

var InitSetPartRedeemableContractConfirmedRequest pb.InitSetPartRedeemableContractConfirmedRequest = pb.InitSetPartRedeemableContractConfirmedRequest{
	Session: "deadbeef",
}

var PartSetInitRedeemableContractConfirmedRequest pb.PartSetInitRedeemableContractConfirmedRequest = pb.PartSetInitRedeemableContractConfirmedRequest{
	Session: "deadbeef",
}

// Swap state machine - 3. Redeem counterparty contracts

var InitStoreInitPublishedRedeemTxidRequest pb.InitStoreInitPublishedRedeemTxidRequest = pb.InitStoreInitPublishedRedeemTxidRequest{
	Session:        "deadbeef",
	InitRedeemTxid: "6645b00a6fe6157bb663554e1c68e23a0cf5396d1e44c6f173ce6ad4cc053534",
	OptSsig:        "56deb0ab51139ce6a03040d6545f7418188c2513eaf0bf39c66fd0095e2685f2",
}

var PartStorePartPublishedRedeemTxidRequest pb.PartStorePartPublishedRedeemTxidRequest = pb.PartStorePartPublishedRedeemTxidRequest{
	Session:        "deadbeef",
	PartRedeemTxid: "3c4848774840292befa2f0aa015f17bfe1a4b33ef6e7bef314ffdfb8d193c964",
}

// Delete functions

var MarkDeletedRequest pb.MarkDeletedRequest = pb.MarkDeletedRequest{
	Session: "deadbeef",
}

var DeleteRequest pb.DeleteRequest = pb.DeleteRequest{
	Session: "deadbeef",
}

package dbclient

import (
	"log"
	"testing"

	"google.golang.org/grpc/status"

	pb "gitlab.com/devwarrior/dragon/session"
	"gitlab.com/devwarrior/dragon/session/testdata"
)

func TestClient(t *testing.T) {
	log.Println("Ping")
	req := pb.PingRequest{}
	_, err := Ping(&req)
	if err != nil {
		// The Go gRPC implementation guarantees that all errors returned from
		// RPC calls are status type errors.
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	log.Println("Pong")

	// NewSession
	resp, err := NewSession(&testdata.NewSessionRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno := resp.Errno
	errstr := resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// AddParticipant
	resp, err = AddParticipant(&testdata.AddParticipantRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// InitStorePartRedeemableContract
	resp, err = InitStorePartRedeemableContract(&testdata.InitStorePartRedeemableContractRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// PartSetPartRedeemableContractAcceptance
	resp, err = PartSetPartRedeemableContractAcceptance(&testdata.PartSetPartRedeemableContractAcceptanceRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// PartStoreInitRedeemableContract
	resp, err = PartStoreInitRedeemableContract(&testdata.PartStoreInitRedeemableContractRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// InitSetInitRedeemableContractAcceptance
	resp, err = InitSetInitRedeemableContractAcceptance(&testdata.InitSetInitRedeemableContractAcceptanceRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// InitStorePublishedPartRedeemableContractTxid
	resp, err = InitStorePublishedPartRedeemableContractTxid(&testdata.InitStorePublishedPartRedeemableContractTxidRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// PartStorePublishedInitRedeemableContractTxid
	resp, err = PartStorePublishedInitRedeemableContractTxid(&testdata.PartStorePublishedInitRedeemableContractTxidRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// InitSetPartRedeemableContractConfirmed
	resp, err = InitSetPartRedeemableContractConfirmed(&testdata.InitSetPartRedeemableContractConfirmedRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// PartSetInitRedeemableContractConfirmed
	resp, err = PartSetInitRedeemableContractConfirmed(&testdata.PartSetInitRedeemableContractConfirmedRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// InitStoreInitPublishedRedeemTxid
	resp, err = InitStoreInitPublishedRedeemTxid(&testdata.InitStoreInitPublishedRedeemTxidRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	// PartStorePartPublishedRedeemTxid
	resp, err = PartStorePartPublishedRedeemTxid(&testdata.PartStorePartPublishedRedeemTxidRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	log.Println("GetData")
	// GetData
	resp, err = GetData(&testdata.DataRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = resp.Errno
	errstr = resp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		testdata.DumpData(resp)
	}

	log.Println("GetState")
	// GetState
	stateResp, err := GetState(&testdata.StateRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = stateResp.Errno
	errstr = stateResp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		log.Printf("StateResponse: %v\n", stateResp)
		testdata.DumpData(resp)
	}

	log.Println("MarkDeleted")
	// MarkDeleted
	markDeletedResp, err := MarkDeleted(&testdata.MarkDeletedRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = markDeletedResp.Errno
	errstr = markDeletedResp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		log.Printf("MarkDeleted: %v\n", markDeletedResp)
	}

	log.Println("Delete")
	// Delete
	deleteResp, err := Delete(&testdata.DeleteRequest)
	if err != nil {
		s := status.Convert(err)
		t.Errorf("Status: %d - %v - %v\n", s.Code(), s.Code(), s.Message())
		return
	}
	errno = markDeletedResp.Errno
	errstr = markDeletedResp.Errstr
	log.Printf("Errno: %v - %s\n", errno, errstr)
	if errno == pb.ERRNO_OK {
		log.Printf("Delete: %v\n", deleteResp)
	}

	log.Println("PASS: Tests completed")
}

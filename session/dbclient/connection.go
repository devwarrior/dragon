package dbclient

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/devwarrior/dragon/appconfig"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	pb "gitlab.com/devwarrior/dragon/session"
)

var (
	useTLS             = appconfig.Config.UseTLS
	certPath           = appconfig.Config.CertPath
	serverAddr         = appconfig.Config.ServerAddr
	serverPort         = appconfig.Config.ServerPort
	serverHostOverride = appconfig.Config.HostOverride
)

// getClientConnection gets a connection to the swap session server
func getClientConnection() (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	if useTLS {
		creds, err := credentials.NewClientTLSFromFile(certPath, serverHostOverride)
		if err != nil {
			log.Fatalf("Failed to create TLS credentials %v", err)
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		log.Println("Warning: No TLS")
		opts = append(opts, grpc.WithInsecure())
	}
	serverHostPort := fmt.Sprintf("%s:%d", serverAddr, serverPort)
	conn, err := grpc.Dial(serverHostPort, opts...)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

/////////
// API //
/////////

// Ping is for the client to check if the server is running
func Ping(request *pb.PingRequest) (*pb.PingResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	response, err := client.Ping(ctx, request)
	if err != nil {
		return response, err
	}
	return response, nil
}

// GetState gets session state
func GetState(request *pb.StateRequest) (*pb.StateResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	stateResponse, err := client.GetState(ctx, request)
	if err != nil {
		return stateResponse, err
	}
	return stateResponse, nil
}

// GetData gets session data
func GetData(request *pb.DataRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.GetData(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// NewSession - the initiator starts a new session with initial shared parameters
func NewSession(request *pb.NewSessionRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.NewSession(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// AddParticipant - the participant joins the new session
func AddParticipant(request *pb.AddParticipantRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.AddParticipant(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// InitStorePartRedeemableContract is
func InitStorePartRedeemableContract(request *pb.InitStorePartRedeemableContractRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.InitStorePartRedeemableContract(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// PartSetPartRedeemableContractAcceptance is
func PartSetPartRedeemableContractAcceptance(request *pb.PartSetPartRedeemableContractAcceptanceRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.PartSetPartRedeemableContractAcceptance(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// PartStoreInitRedeemableContract is
func PartStoreInitRedeemableContract(request *pb.PartStoreInitRedeemableContractRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.PartStoreInitRedeemableContract(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// InitSetInitRedeemableContractAcceptance is
func InitSetInitRedeemableContractAcceptance(request *pb.InitSetInitRedeemableContractAcceptanceRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.InitSetInitRedeemableContractAcceptance(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// InitStorePublishedPartRedeemableContractTxid is
func InitStorePublishedPartRedeemableContractTxid(request *pb.InitStorePublishedPartRedeemableContractTxidRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.InitStorePublishedPartRedeemableContractTxid(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// PartStorePublishedInitRedeemableContractTxid is
func PartStorePublishedInitRedeemableContractTxid(request *pb.PartStorePublishedInitRedeemableContractTxidRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.PartStorePublishedInitRedeemableContractTxid(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// InitSetPartRedeemableContractConfirmed is
func InitSetPartRedeemableContractConfirmed(request *pb.InitSetPartRedeemableContractConfirmedRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.InitSetPartRedeemableContractConfirmed(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// PartSetInitRedeemableContractConfirmed is
func PartSetInitRedeemableContractConfirmed(request *pb.PartSetInitRedeemableContractConfirmedRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.PartSetInitRedeemableContractConfirmed(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// InitStoreInitPublishedRedeemTxid is
func InitStoreInitPublishedRedeemTxid(request *pb.InitStoreInitPublishedRedeemTxidRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.InitStoreInitPublishedRedeemTxid(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// InitSetInitRedeemConfirmed is
// func InitSetInitRedeemConfirmed(request *pb.InitSetInitRedeemConfirmedRequest) (*pb.DataResponse, error) {
// 	conn, err := getClientConnection()
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer conn.Close()
// 	client := pb.NewSwapSessionClient(conn)
// 	ctx, cancel := context.WithCancel(context.Background())
// 	defer cancel()
// 	dataResponse, err := client.InitSetInitRedeemConfirmed(ctx, request)
// 	if err != nil {
// 		return dataResponse, err
// 	}
// 	return dataResponse, nil
// }

// PartStorePartPublishedRedeemTxid is
func PartStorePartPublishedRedeemTxid(request *pb.PartStorePartPublishedRedeemTxidRequest) (*pb.DataResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	dataResponse, err := client.PartStorePartPublishedRedeemTxid(ctx, request)
	if err != nil {
		return dataResponse, err
	}
	return dataResponse, nil
}

// PartSetPartRedeemConfirmed is
// func PartSetPartRedeemConfirmed(request *pb.PartSetPartRedeemConfirmedRequest) (*pb.DataResponse, error) {
// 	conn, err := getClientConnection()
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer conn.Close()
// 	client := pb.NewSwapSessionClient(conn)
// 	ctx, cancel := context.WithCancel(context.Background())
// 	defer cancel()
// 	dataResponse, err := client.PartSetPartRedeemConfirmed(ctx, request)
// 	if err != nil {
// 		return dataResponse, err
// 	}
// 	return dataResponse, nil
// }

// MarkDeleted is
func MarkDeleted(request *pb.MarkDeletedRequest) (*pb.MarkDeletedResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	markDeletedResponse, err := client.MarkDeleted(ctx, request)
	if err != nil {
		return markDeletedResponse, err
	}
	return markDeletedResponse, nil
}

// Delete is
func Delete(request *pb.DeleteRequest) (*pb.DeleteResponse, error) {
	conn, err := getClientConnection()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	client := pb.NewSwapSessionClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	deleteResponse, err := client.Delete(ctx, request)
	if err != nil {
		return deleteResponse, err
	}
	return deleteResponse, nil
}

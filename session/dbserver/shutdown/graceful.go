package main

import (
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strconv"
	"syscall"
)

const pidFile = "/tmp/dragon.session.server.pid"

func main() {
	if runtime.GOOS == "windows" {
		logger.Log.Fatalln("This does not work on Windows")
	}
	dat, err := ioutil.ReadFile(pidFile)
	if err != nil {
		logger.Log.Fatalf("Cannot read %s, error: %v\n", pidFile, err)
	}
	sPid := string(dat)
	logger.Log.Printf("pid: %s\n", sPid)
	pid, err := strconv.ParseInt(sPid, 10, 64)
	if err != nil {
		logger.Log.Fatalf("Cannot parse %s, error: %v\n", string(dat), err)
	}
	process, err := os.FindProcess(int(pid))
	if err != nil {
		logger.Log.Fatalf("Cannot find process %d, error: %v\n", pid, err)
	}
	// logger.Log.Printf("Process %v\n", process)
	process.Signal(syscall.SIGTERM) // Not on windows
	logger.Log.Printf("SIGTERM: =>  %d\n", pid)
}

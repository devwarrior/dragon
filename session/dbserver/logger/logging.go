package logger

import (
	"log"
	"os"
)

var (
	// Log is the main file logger
	Log *log.Logger
)

const (
	logfileName = "logfile.txt"
)

func init() {
	logfile, err := os.OpenFile(logfileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalln("Failed to open log file", logfileName, ":", err)
	}
	Log = log.New(logfile, "LOG ", log.Ldate|log.Ltime|log.Lshortfile)
}

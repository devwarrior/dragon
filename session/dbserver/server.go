package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"

	pb "gitlab.com/devwarrior/dragon/session"
	"gitlab.com/devwarrior/dragon/session/dbserver/logger"
	"gitlab.com/devwarrior/dragon/session/dbserver/sqlite3db"
	"gitlab.com/devwarrior/dragon/session/dbserver/srvconfig"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// Configuration
var (
	pidFile     = srvconfig.Config.PidFile
	tls         = srvconfig.Config.UseTLS
	certPath    = srvconfig.Config.CertPath
	certKeyPath = srvconfig.Config.CertKeyPath
	serverAddr  = srvconfig.Config.ServerAddr
	serverPort  = srvconfig.Config.ServerPort
)

// gRPC server instance
var grpcServer *grpc.Server

// swapSessionServer implements SwapSessionServer
type swapSessionServer struct {
}

///////////////////////////////////
// Only meta response will error //
///////////////////////////////////

// Ping pings the server to establish if it is running
func (s *swapSessionServer) Ping(ctx context.Context, request *pb.PingRequest) (*pb.PingResponse, error) {
	response := pb.PingResponse{}
	logger.Log.Printf("Ping\n")
	return &response, nil
}

// GetState returns the session state for the requested session
func (s *swapSessionServer) GetState(ctx context.Context, request *pb.StateRequest) (*pb.StateResponse, error) {
	//logger.Log.Printf("GetState%s\n",  request.Session)
	return sqlite3db.GetState(request), nil
}

// GetData returns all the session data for the requested session
func (s *swapSessionServer) GetData(ctx context.Context, request *pb.DataRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("GetData %s\n", request.Session)
	return sqlite3db.GetData(request), nil
}

// NewSession creates a new swap session and returns all the session data for the new session
func (s *swapSessionServer) NewSession(ctx context.Context, request *pb.NewSessionRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("NewSession%s\n", request.Session)
	return sqlite3db.NewSession(request), nil
}

// AddParticipant adds 1 (one) participant to an existing session and returns all the session data
// for the requested session
func (s *swapSessionServer) AddParticipant(ctx context.Context, request *pb.AddParticipantRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("AddParticipant%s\n", request.Session)
	return sqlite3db.AddParticipant(request), nil
}

// InitStorePartRedeemableContract stores participant redeemable contract data and returns all the
// session data for the requested session
func (s *swapSessionServer) InitStorePartRedeemableContract(ctx context.Context,
	request *pb.InitStorePartRedeemableContractRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("InitStorePartRedeemableContract%s\n", request.Session)
	return sqlite3db.InitStorePartRedeemableContract(request), nil
}

// PartSetPartRedeemableContractAcceptance sets participant redeemable contract data as being acceptable
// and returns all the session data for the requested session
func (s *swapSessionServer) PartSetPartRedeemableContractAcceptance(ctx context.Context,
	request *pb.PartSetPartRedeemableContractAcceptanceRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("PartSetPartRedeemableContractAcceptance%s\n", request.Session)
	return sqlite3db.PartSetPartRedeemableContractAcceptance(request), nil
}

// PartStoreInitRedeemableContract stores initiator redeemable contract data and returns all the
// session data for the requested session
func (s *swapSessionServer) PartStoreInitRedeemableContract(ctx context.Context,
	request *pb.PartStoreInitRedeemableContractRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("PartStoreInitRedeemableContract%s\n", request.Session)
	return sqlite3db.PartStoreInitRedeemableContract(request), nil
}

// InitSetInitRedeemableContractAcceptance sets initiator redeemable contract data as being acceptable
// and returns all the session data for the requested session
func (s *swapSessionServer) InitSetInitRedeemableContractAcceptance(ctx context.Context,
	request *pb.InitSetInitRedeemableContractAcceptanceRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("InitSetInitRedeemableContractAcceptance%s\n", request.Session)
	return sqlite3db.InitSetInitRedeemableContractAcceptance(request), nil
}

// InitStorePublishedPartRedeemableContractTxid stores the published participant redeemable contract txid
// and returns all the session data for the requested session
func (s *swapSessionServer) InitStorePublishedPartRedeemableContractTxid(ctx context.Context,
	request *pb.InitStorePublishedPartRedeemableContractTxidRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("InitStorePublishedPartRedeemableContractTxid%s\n", request.Session)
	return sqlite3db.InitStorePublishedPartRedeemableContractTxid(request), nil
}

// PartStorePublishedInitRedeemableContractTxid stores the published initiator redeemable contract txid
// and returns all the session data for the requested session
func (s *swapSessionServer) PartStorePublishedInitRedeemableContractTxid(ctx context.Context,
	request *pb.PartStorePublishedInitRedeemableContractTxidRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("PartStorePublishedInitRedeemableContractTxid%s\n", request.Session)
	return sqlite3db.PartStorePublishedInitRedeemableContractTxid(request), nil
}

// InitSetPartRedeemableContractConfirmed sets the published participant redeemable contract as having
// been mined and returns all the session data for the requested session
func (s *swapSessionServer) InitSetPartRedeemableContractConfirmed(ctx context.Context,
	request *pb.InitSetPartRedeemableContractConfirmedRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("PartSetPartRedeemableContractConfirmed%s\n", request.Session)
	return sqlite3db.InitSetPartRedeemableContractConfirmed(request), nil
}

// PartSetInitRedeemableContractConfirmed sets the published initiator redeemable contract as having
// been mined and returns all the session data for the requested session
func (s *swapSessionServer) PartSetInitRedeemableContractConfirmed(ctx context.Context,
	request *pb.PartSetInitRedeemableContractConfirmedRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("InitSetInitRedeemableContractConfirmed%s\n", request.Session)
	return sqlite3db.PartSetInitRedeemableContractConfirmed(request), nil
}

// InitStoreInitPublishedRedeemTxid stores txid of the transaction redeeming the counterparty contract
// and returns all the session data for the requested session
func (s *swapSessionServer) InitStoreInitPublishedRedeemTxid(ctx context.Context,
	request *pb.InitStoreInitPublishedRedeemTxidRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("InitStoreInitPublishedRedeemTxid%s\n", request.Session)
	return sqlite3db.InitStoreInitPublishedRedeemTxid(request), nil
}

// PartStorePartPublishedRedeemTxid stores txid of the transaction redeeming the counterparty contract
// and returns all the session data for the requested session
func (s *swapSessionServer) PartStorePartPublishedRedeemTxid(ctx context.Context,
	request *pb.PartStorePartPublishedRedeemTxidRequest) (*pb.DataResponse, error) {
	logger.Log.Printf("PartStorePartPublishedRedeemTxid%s\n", request.Session)
	return sqlite3db.PartStorePartPublishedRedeemTxid(request), nil
}

// MarkDeleted marks a session as having been deleted early. The counterparty will notice this state
// and do the final deletion or database maintenance will do it later.
func (s *swapSessionServer) MarkDeleted(ctx context.Context,
	request *pb.MarkDeletedRequest) (*pb.MarkDeletedResponse, error) {
	logger.Log.Printf("MarkDeleted%s\n", request.Session)
	return sqlite3db.MarkDeleted(request), nil
}

// Delete deletes a session and all data.
func (s *swapSessionServer) Delete(ctx context.Context,
	request *pb.DeleteRequest) (*pb.DeleteResponse, error) {
	logger.Log.Printf("Delete%s\n", request.Session)
	return sqlite3db.Delete(request), nil
}

//////////
// MAIN //
//////////

// newServer is the swapSessionServer Constructor
func newServer() *swapSessionServer {
	s := &swapSessionServer{}
	return s
}

func main() {
	// one instance
	ensureUniqueServerProcess()
	// start listening socket
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", serverAddr, serverPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	logger.Log.Printf("Server listening on %s:%v\n", serverAddr, serverPort)
	// server credentials
	var opts []grpc.ServerOption
	if tls {
		creds, err := credentials.NewServerTLSFromFile(certPath, certKeyPath)
		if err != nil {
			log.Fatalf("Failed to generate credentials %v", err)
		}
		opts = []grpc.ServerOption{grpc.Creds(creds)}
	} else {
		log.Println("Warning: No TLS")
	}
	// Start database
	openDbConnection()
	// export process lock/pid file
	setPidFile()
	// Good to go
	startSignalHandler()
	grpcServer = grpc.NewServer(opts...)
	pb.RegisterSwapSessionServer(grpcServer, newServer())
	grpcServer.Serve(lis)
}

/////////////////////////
// One Server Instance //
/////////////////////////

func ensureUniqueServerProcess() {
	if runtime.GOOS == "windows" {
		log.Fatalln("This server does not run on Windows")
	}
	if checkPidfileExists() {
		log.Fatalln("server already running")
	}
}

func checkPidfileExists() bool {
	_, err := os.Stat(pidFile)
	if err == nil {
		return true
	}
	// logger.Log.Printf("checkPidfileExists: %v\n", err)
	return false
}

//////////////////
// Needed Setup //
//////////////////

func openDbConnection() {
	err := sqlite3db.DbOpen()
	if err != nil {
		log.Fatalf("cannot open database connection, error: %v\n", err)
	}
	logger.Log.Println("Opened database connection")
}

// Allow shutdown process to discover and gracefully stop server
func setPidFile() {
	pid := strconv.FormatInt(int64(os.Getpid()), 10)
	f, err := os.Create(pidFile)
	if err != nil {
		log.Fatalf("cannot create pid file: %s\n", pidFile)
	}
	defer f.Close()
	f.WriteString(pid)
	f.Sync()
	logger.Log.Printf("server pid: %s\n", pid)
	log.Printf("server pid: %s\n", pid)
}

///////////////////////////////////////
// Graceful Shutdown Signal Handling //
///////////////////////////////////////

// Capture SIGINT or SIGTERM
func startSignalHandler() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go signalHandler(sigs)
}

func signalHandler(sigs chan os.Signal) {
	sig := <-sigs
	fmt.Printf("\nReceived SIG: %v\n", sig)
	logger.Log.Printf("Received SIG: %v\n", sig)
	gracefulShutdown()
}

func gracefulShutdown() {
	msg := "closing database connection"
	log.Println(msg)
	logger.Log.Println(msg)
	_ = sqlite3db.DbClose()
	msg = "waiting for server to gracefully shut down..."
	log.Println(msg)
	logger.Log.Println(msg)
	grpcServer.GracefulStop()
	msg = "...server has shut down"
	log.Println(msg)
	logger.Log.Println(msg)
	os.Remove(pidFile)
	msg = "removed lock file"
	log.Println(msg)
	logger.Log.Println(msg)
	os.Exit(0)
}

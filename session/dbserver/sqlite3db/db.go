package sqlite3db

import _ "github.com/mattn/go-sqlite3" // Sqlite3 cgo database driver import

import (
	"database/sql"
	"fmt"

	pb "gitlab.com/devwarrior/dragon/session"
	"gitlab.com/devwarrior/dragon/session/dbserver/logger"
)

const (
	driver = "sqlite3"
	dbpath = "./session.db"
)

var (
	dbConn *sql.DB
)

// DbOpen is called once from the session server on startup
func DbOpen() error {
	db, err := sql.Open(driver, dbpath)
	if err != nil {
		logger.Log.Printf("dbOpen %v\n", err)
		return err
	}
	dbConn = db
	return nil
}

// DbClose is called from the session server on graceful shutdown
func DbClose() error {
	if dbConn == nil {
		return nil
	}
	err := dbConn.Close()
	if err != nil {
		return err
	}
	return nil
}

// CreateSwapTable is
func CreateSwapTable() error {
	sqlCreateSwapTable := `
	CREATE TABLE IF NOT EXISTS swaps
	(
		session TEXT PRIMARY KEY,
		state INTEGER,
		initiator TEXT,
		participant TEXT,
		secrethash TEXT,
		network INTEGER,
		init_coin_rcv INTEGER,
		init_coin_rcv_value INTEGER,
		part_coin_rcv INTEGER,
		part_coin_rcv_value INTEGER,
		init_redeemable_contract TEXT,
		init_redeemable_contract_tx TEXT,
		init_redeemable_contract_p2sh TEXT,
		init_redeemable_contract_txid TEXT,
		init_redeemable_contract_locktime INTEGER,
		part_redeemable_contract TEXT,
		part_redeemable_contract_tx TEXT,
		part_redeemable_contract_p2sh TEXT,
		part_redeemable_contract_txid TEXT,
		part_redeemable_contract_locktime INTEGER,
		init_redeem_txid TEXT,
		opt_ssig TEXT,
		part_redeem_txid TEXT
	);`
	_, err := dbConn.Exec(sqlCreateSwapTable)
	if err != nil {
		logger.Log.Printf("%q: %s\n", err, sqlCreateSwapTable)
		return err
	}

	return nil
}

/////////
// API //
/////////

// GetState returns the state of a swap session
func GetState(request *pb.StateRequest) *pb.StateResponse {
	response := pb.StateResponse{Errno: pb.ERRNO_OK}

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	count := dbSessionCount(request.Session)

	// Check we have exactly one record with this session id - fast path
	// logger.Log.Printf("GetState row count for session id %s is %d\n", request.Session, count)
	switch {
	case count <= 0:
		var e string
		e = fmt.Sprintf("no such session %s", request.Session)
		response.Errno = pb.ERRNO_NO_SUCH_SESSION
		response.Errstr = e
		response.CurrentState = pb.STATE_Unknown
		return &response
	case count > 1:
		var e string
		e = fmt.Sprintf("database reports mutiple records for session id %s", request.Session)
		response.Errno = pb.ERRNO_INVALID_SESSION
		response.Errstr = e
		response.CurrentState = pb.STATE_Unknown
		return &response
	}

	err := dbSessionState(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("GetState %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// GetData returns all data in a swap session
func GetData(request *pb.DataRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id
	err := dbCheckSessionUnique(request.Session, &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Fill the response from the database
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("GetData %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// NewSession creates a new swap session
func NewSession(request *pb.NewSessionRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check session id not in use
	count := dbSessionCount(request.Session)
	if count > 0 {
		e := fmt.Sprintf("NewSession row count for session id %s is %d\n", request.Session, count)
		response.Errno = pb.ERRNO_ALREADY_EXIST
		response.Errstr = e
		return &response
	}

	// Insert new session record into database
	sqlNewSession :=
		`INSERT INTO swaps VALUES
		(
			?,?,?,NULL,?,?,?,?,?,?, NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL
		);`
	_, err := dbConn.Exec(sqlNewSession,
		request.Session,
		int32(pb.STATE_NewSession),
		request.Initiator,
		request.Secrethash,
		int32(request.Network),
		int32(request.InitCoinRcv),
		request.InitCoinRcvValue,
		int32(request.PartCoinRcv),
		request.PartCoinRcvValue)
	if err != nil {
		logger.Log.Printf("NewSession %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		return &response
	}

	return &response
}

// AddParticipant adds participant's receive address to the new swap session
func AddParticipant(request *pb.AddParticipantRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 0
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_NewSession), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlAddParticipant := `UPDATE swaps 
						  SET participant = ?, state = ? 
						  WHERE session = ?;`
	_, err = dbConn.Exec(sqlAddParticipant,
		request.Participant,
		int32(pb.STATE_ParticipantJoined),
		request.Session)
	if err != nil {
		logger.Log.Printf("AddParticipant %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		return &response
	}

	return &response
}

// InitStorePartRedeemableContract stores participant redeemable contract data
func InitStorePartRedeemableContract(request *pb.InitStorePartRedeemableContractRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 1
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_ParticipantJoined), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlISPRC := `UPDATE swaps 
				 SET part_redeemable_contract = ?, 
					 part_redeemable_contract_tx = ?,
					 part_redeemable_contract_p2sh = ?,
					 part_redeemable_contract_locktime = ?,
				     state = ? 
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlISPRC,
		request.PartRedeemableContract,
		request.PartRedeemableContractTx,
		request.PartRedeemableContractP2Sh,
		request.PartRedeemableContractLocktime,
		int32(pb.STATE_PartRedeemableContractOffered),
		request.Session)
	if err != nil {
		logger.Log.Printf("InitStorePartRedeemableContract %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("InitStorePartRedeemableContract %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// PartSetPartRedeemableContractAcceptance sets participant redeemable contract data acceptance
func PartSetPartRedeemableContractAcceptance(request *pb.PartSetPartRedeemableContractAcceptanceRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 2
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_PartRedeemableContractOffered), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlPSPRCA := `UPDATE swaps 
				 SET state = ? 
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlPSPRCA,
		int32(pb.STATE_PartRedeemableContractAccepted),
		request.Session)
	if err != nil {
		logger.Log.Printf("PartSetPartRedeemableContractAcceptance %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno is set for us
		logger.Log.Printf("PartSetPartRedeemableContractAcceptance %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// PartStoreInitRedeemableContract stores initiator redeemable contract data
func PartStoreInitRedeemableContract(request *pb.PartStoreInitRedeemableContractRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 3
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_PartRedeemableContractAccepted), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlPSIRC := `UPDATE swaps 
				 SET init_redeemable_contract = ?, 
					 init_redeemable_contract_tx = ?, 
					 init_redeemable_contract_p2sh = ?,
					 init_redeemable_contract_locktime = ?,
				     state = ? 
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlPSIRC,
		request.InitRedeemableContract,
		request.InitRedeemableContractTx,
		request.InitRedeemableContractP2Sh,
		request.InitRedeemableContractLocktime,
		int32(pb.STATE_InitRedeemableContractOffered),
		request.Session)
	if err != nil {
		logger.Log.Printf("PartStoreInitRedeemableContract %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("PartStoreInitRedeemableContract %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// InitSetInitRedeemableContractAcceptance sets initiator redeemable contract data acceptance
func InitSetInitRedeemableContractAcceptance(request *pb.InitSetInitRedeemableContractAcceptanceRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 4
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_InitRedeemableContractOffered), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlISIRCA := `UPDATE swaps 
				 SET state = ? 
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlISIRCA,
		int32(pb.STATE_InitRedeemableContractAccepted),
		request.Session)
	if err != nil {
		logger.Log.Printf("InitSetInitRedeemableContractAcceptance %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("InitSetInitRedeemableContractAcceptance %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// InitStorePublishedPartRedeemableContractTxid stores participant redeemable contract txid after publishing
func InitStorePublishedPartRedeemableContractTxid(request *pb.InitStorePublishedPartRedeemableContractTxidRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 5
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_InitRedeemableContractAccepted), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlISPRCT := `UPDATE swaps 
				 SET state = ?, part_redeemable_contract_txid = ?
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlISPRCT,
		int32(pb.STATE_PartRedeemableContractPublished),
		request.PartRedeemableContractTxid,
		request.Session)
	if err != nil {
		logger.Log.Printf("InitStorePublishedPartRedeemableContractTxid %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("InitStorePublishedPartRedeemableContractTxid %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// PartStorePublishedInitRedeemableContractTxid stores initiator redeemable contract txid after publishing
func PartStorePublishedInitRedeemableContractTxid(request *pb.PartStorePublishedInitRedeemableContractTxidRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 6
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_PartRedeemableContractPublished), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlPSIRCT := `UPDATE swaps 
				 SET state = ?, init_redeemable_contract_txid = ?
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlPSIRCT,
		int32(pb.STATE_InitRedeemableContractPublished),
		request.InitRedeemableContractTxid,
		request.Session)
	if err != nil {
		logger.Log.Printf("PartStorePublishedInitRedeemableContractTxid %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("PartStorePublishedInitRedeemableContractTxid %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// InitSetPartRedeemableContractConfirmed set when published participant redeemable contract is confirmed
func InitSetPartRedeemableContractConfirmed(request *pb.InitSetPartRedeemableContractConfirmedRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 7
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_InitRedeemableContractPublished), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlPSPRCC := `UPDATE swaps 
				 SET state = ?
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlPSPRCC,
		int32(pb.STATE_PartRedeemableContractConfirmed),
		request.Session)
	if err != nil {
		logger.Log.Printf("InitSetPartRedeemableContractConfirmed %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("InitetPartRedeemableContractConfirmed %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// PartSetInitRedeemableContractConfirmed set when published initiator redeemable contract is confirmed
func PartSetInitRedeemableContractConfirmed(request *pb.PartSetInitRedeemableContractConfirmedRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 8
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_PartRedeemableContractConfirmed), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlPSPRCC := `UPDATE swaps 
				 SET state = ?
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlPSPRCC,
		int32(pb.STATE_InitRedeemableContractConfirmed),
		request.Session)
	if err != nil {
		logger.Log.Printf("PartSetInitRedeemableContractConfirmed %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("InitSetInitRedeemableContractConfirmed %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// InitStoreInitPublishedRedeemTxid stores the initiator contract redemption txid
func InitStoreInitPublishedRedeemTxid(request *pb.InitStoreInitPublishedRedeemTxidRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 9
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_InitRedeemableContractConfirmed), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlISIPRT := `UPDATE swaps
				 SET state = ?, init_redeem_txid = ?, opt_ssig = ?
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlISIPRT,
		int32(pb.STATE_InitRedeemTxPublished),
		request.InitRedeemTxid,
		request.OptSsig,
		request.Session)
	if err != nil {
		logger.Log.Printf("InitStoreInitPublishedRedeemTxid %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno, Errstr is set for us
		logger.Log.Printf("InitStoreInitPublishedRedeemTxid %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// PartStorePartPublishedRedeemTxid stores the participant contract redemption txid
func PartStorePartPublishedRedeemTxid(request *pb.PartStorePartPublishedRedeemTxidRequest) *pb.DataResponse {
	response := pb.DataResponse{Errno: pb.ERRNO_OK}
	responseData := pb.Data{}
	response.Data = &responseData

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Check we have exactly one record with this session id and current state is 10
	err := dbCheckSessionStateUnique(request.Session, int32(pb.STATE_InitRedeemTxPublished), &response)
	if err != nil {
		// response.ERRNO, Errstr updated with session error
		return &response
	}

	// Update session record
	sqlPSPRT := `UPDATE swaps
				 SET state = ?, part_redeem_txid = ? 
				 WHERE session = ?;`
	_, err = dbConn.Exec(sqlPSPRT,
		int32(pb.STATE_PartRedeemTxPublished),
		request.PartRedeemTxid,
		request.Session)
	if err != nil {
		logger.Log.Printf("PartStorePartPublishedRedeemTxid %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	// Get the newly inserted session record to pass back to client
	err = dbSessionData(request.Session, &response)
	if err != nil {
		// response.Errno is set for us
		logger.Log.Printf("PartStorePartPublishedRedeemTxid %v %v\n", response.Errno, err)
		return &response
	}

	return &response
}

// Deletion

// MarkDeleted marks a session record as ready to be deleted
func MarkDeleted(request *pb.MarkDeletedRequest) *pb.MarkDeletedResponse {
	response := pb.MarkDeletedResponse{Errno: pb.ERRNO_OK}

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Update session record
	sqlMSD := `UPDATE swaps
				 SET state = ? 
				 WHERE session = ?;`
	_, err := dbConn.Exec(sqlMSD,
		int32(pb.STATE_MarkedDeleted),
		request.Session)
	if err != nil {
		logger.Log.Printf("MarkDeleted %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	return &response
}

// Delete permanently removes a session record
func Delete(request *pb.DeleteRequest) *pb.DeleteResponse {
	response := pb.DeleteResponse{Errno: pb.ERRNO_OK}

	if dbConn == nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = "no database connection"
		return &response
	}

	// Delete session record
	sqlMSD := `DELETE FROM swaps WHERE session = ?;`
	_, err := dbConn.Exec(sqlMSD,
		request.Session)
	if err != nil {
		logger.Log.Printf("Delete %v\n", err)
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return &response
	}

	return &response
}

////////////////
// Unexported //
////////////////

func dbCheckSessionStateUnique(session string, currentState int32, response *pb.DataResponse) error {
	// Check we have exactly one record with this session id And state
	// If any errors update the session ERRNO, Errstr
	count := dbSessionCountForState(session, currentState)
	// logger.Log.Printf("row count for session id %s is %d\n", session, count)
	switch {
	case count <= 0:
		e := fmt.Sprintf("no such session %s in state %d", session, currentState)
		response.Errno = pb.ERRNO_INVALID_SESSION_STATE
		response.Errstr = e
		return fmt.Errorf(e)
	case count > 1:
		e := fmt.Sprintf("database reports mutiple records for session id %s", session)
		response.Errno = pb.ERRNO_INVALID_SESSION_STATE
		response.Errstr = e
		return fmt.Errorf(e)
	}
	return nil
}

func dbSessionCountForState(session string, currentState int32) int32 {
	sqlCount := `SELECT COUNT(session) FROM swaps WHERE session = ? AND state = ?;`
	var count int32
	// There will always be one row returned with one column: 'count'
	err := dbConn.QueryRow(sqlCount, session, currentState).Scan(&count)
	if err != nil {
		return -1
	}
	return count
}

func dbCheckSessionUnique(session string, response *pb.DataResponse) error {
	// Check we have exactly one record with this session id
	// If any errors update the session ERRNO, Errstr
	count := dbSessionCount(session)
	// logger.Log.Printf("row count for session id %s is %d\n", session, count)
	switch {
	case count <= 0:
		e := fmt.Sprintf("no such session %s", session)
		response.Errno = pb.ERRNO_NO_SUCH_SESSION
		response.Errstr = e
		// response.Data.State = pb.STATE_Unknown
		return fmt.Errorf(e)
	case count > 1:
		e := fmt.Sprintf("database reports mutiple records for session id %s", session)
		response.Errno = pb.ERRNO_INVALID_SESSION
		response.Errstr = e
		// response.Data.State = pb.STATE_Unknown
		return fmt.Errorf(e)
	}
	return nil
}

func dbSessionCount(session string) int32 {
	sqlCount := `SELECT COUNT(session) FROM swaps WHERE session = ?;`
	var count int32
	// There will always be one row returned with one column: 'count'
	err := dbConn.QueryRow(sqlCount, session).Scan(&count)
	if err != nil {
		return -1
	}
	return count
}

func dbSessionState(session string, response *pb.StateResponse) error {
	var state int32

	sqlCount := `SELECT state
				 FROM swaps
				 WHERE session = ?;`
	err := dbConn.QueryRow(sqlCount, session).Scan(&state)
	if err != nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return err
	}

	response.Errno = pb.ERRNO_OK
	response.Errstr = "dbSessionState OK"
	response.CurrentState = pb.STATE(state)

	return nil
}

func dbSessionData(session string, response *pb.DataResponse) error {
	var initiator sql.NullString   // Can be NULL
	var participant sql.NullString // Can be NULL
	var secrethash sql.NullString  // Can be NULL
	var state int32
	var network int32
	var initCoinRcv int32
	var initCoinRcvValue uint64
	var partCoinRcv int32
	var partCoinRcvValue uint64
	var initRedeemableContract sql.NullString     // Can be NULL
	var initRedeemableContractTx sql.NullString   // Can be NULL
	var initRedeemableContractP2SH sql.NullString // Can be NULL
	var initRedeemableContractTxid sql.NullString // Can be NULL
	var initRedeemableContractLocktime int64
	var partRedeemableContract sql.NullString     // Can be NULL
	var partRedeemableContractTx sql.NullString   // Can be NULL
	var partRedeemableContractP2SH sql.NullString // Can be NULL
	var partRedeemableContractTxid sql.NullString // Can be NULL
	var partRedeemableContractLocktime int64
	var initRedeemTxid sql.NullString // Can be NULL
	var optSsig sql.NullString        // Can be NULL
	var partRedeemTxid sql.NullString // Can be NULL

	sqlCount := `SELECT 
				 initiator, participant, secrethash, state, network,
				 init_coin_rcv, init_coin_rcv_value,
				 part_coin_rcv, part_coin_rcv_value,
				 init_redeemable_contract, init_redeemable_contract_tx,
				 init_redeemable_contract_p2sh, init_redeemable_contract_txid, init_redeemable_contract_locktime,
				 part_redeemable_contract, part_redeemable_contract_tx,
				 part_redeemable_contract_p2sh, part_redeemable_contract_txid, part_redeemable_contract_locktime,
				 init_redeem_txid,
				 opt_ssig,
				 part_redeem_txid
				 FROM swaps
				 WHERE session = ?;`
	err := dbConn.QueryRow(sqlCount, session).Scan(
		&initiator, &participant, &secrethash, &state, &network,
		&initCoinRcv, &initCoinRcvValue,
		&partCoinRcv, &partCoinRcvValue,
		&initRedeemableContract, &initRedeemableContractTx,
		&initRedeemableContractP2SH, &initRedeemableContractTxid, &initRedeemableContractLocktime,
		&partRedeemableContract, &partRedeemableContractTx,
		&partRedeemableContractP2SH, &partRedeemableContractTxid, &partRedeemableContractLocktime,
		&initRedeemTxid,
		&optSsig,
		&partRedeemTxid)
	if err != nil {
		response.Errno = pb.ERRNO_DATABASE
		response.Errstr = err.Error()
		return err
	}

	response.Errno = pb.ERRNO_OK
	response.Errstr = "dbSessionData OK"

	response.Data.Session = session
	response.Data.State = pb.STATE(state)
	response.Data.Initiator = initiator.String
	response.Data.Participant = participant.String
	response.Data.Secrethash = secrethash.String
	response.Data.Network = pb.NET_TYPE(network)
	response.Data.InitCoinRcv = pb.COIN(initCoinRcv)
	response.Data.InitCoinRcvValue = int64(initCoinRcvValue)
	response.Data.PartCoinRcv = pb.COIN(partCoinRcv)
	response.Data.PartCoinRcvValue = int64(partCoinRcvValue)
	response.Data.InitRedeemableContract = initRedeemableContract.String
	response.Data.InitRedeemableContractTx = initRedeemableContractTx.String
	response.Data.InitRedeemableContractP2Sh = initRedeemableContractP2SH.String
	response.Data.InitRedeemableContractTxid = initRedeemableContractTxid.String
	response.Data.InitRedeemableContractLocktime = int64(initRedeemableContractLocktime)
	response.Data.PartRedeemableContract = partRedeemableContract.String
	response.Data.PartRedeemableContractTx = partRedeemableContractTx.String
	response.Data.PartRedeemableContractP2Sh = partRedeemableContractP2SH.String
	response.Data.PartRedeemableContractTxid = partRedeemableContractTxid.String
	response.Data.PartRedeemableContractLocktime = int64(partRedeemableContractLocktime)
	response.Data.InitRedeemTxid = initRedeemTxid.String
	response.Data.OptSsig = optSsig.String
	response.Data.PartRedeemTxid = partRedeemTxid.String

	return nil
}

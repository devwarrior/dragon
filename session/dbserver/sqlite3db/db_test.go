package sqlite3db

import (
	"fmt"
	"testing"

	pb "gitlab.com/devwarrior/dragon/session"
	"gitlab.com/devwarrior/dragon/session/testdata"
)

////////////////
// TEST CASES //
////////////////

func TestDbSequence(t *testing.T) {
	err := DbOpen()
	if err != nil {
		t.Errorf("DbOpen %v\n", err)
		return
	}

	rnd := testdata.GetRandSessionID()
	fmt.Printf("%s\n", rnd)

	err = CreateSwapTable()
	if err != nil {
		t.Errorf("CreateSwapTable %v\n", err)
		return
	}

	////////////////////////////////////////////////////////////////
	// All errors from here are packed in the response structures //
	////////////////////////////////////////////////////////////////

	resp := NewSession(&testdata.NewSessionRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("NewSession %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = AddParticipant(&testdata.AddParticipantRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("AddParticipant %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = InitStorePartRedeemableContract(&testdata.InitStorePartRedeemableContractRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("InitStorePartRedeemableContract %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = PartSetPartRedeemableContractAcceptance(&testdata.PartSetPartRedeemableContractAcceptanceRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("PartSetPartRedeemableContractAcceptance %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = PartStoreInitRedeemableContract(&testdata.PartStoreInitRedeemableContractRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("PartStoreInitRedeemableContract %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = InitSetInitRedeemableContractAcceptance(&testdata.InitSetInitRedeemableContractAcceptanceRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("InitSetInitRedeemableContractAcceptance %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = InitStorePublishedPartRedeemableContractTxid(&testdata.InitStorePublishedPartRedeemableContractTxidRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("InitStorePublishedPartRedeemableContractTxid %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = PartStorePublishedInitRedeemableContractTxid(&testdata.PartStorePublishedInitRedeemableContractTxidRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("PartStorePublishedInitRedeemableContractTxid %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = InitSetPartRedeemableContractConfirmed(&testdata.PartSetPartRedeemableContractConfirmedRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("InitSetPartRedeemableContractConfirmed %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = PartSetInitRedeemableContractConfirmed(&testdata.InitSetInitRedeemableContractConfirmedRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("PartSetInitRedeemableContractConfirmed %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	resp = InitStoreInitPublishedRedeemTxid(&testdata.InitStoreInitPublishedRedeemTxidRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("InitStoreInitPublishedRedeemTxid %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	// resp = InitSetInitRedeemConfirmed(&testdata.InitSetInitRedeemConfirmedRequest)
	// if resp.Errno != pb.ERRNO_OK {
	// 	t.Errorf("InitSetInitRedeemConfirmed %v %v\n", resp.Errno, resp.Errstr)
	// 	return
	// }
	// testdata.DumpData(resp)

	resp = PartStorePartPublishedRedeemTxid(&testdata.PartStorePartPublishedRedeemTxidRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("PartStorePartPublishedRedeemTxid %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(resp)

	// resp = PartSetPartRedeemConfirmed(&testdata.PartSetPartRedeemConfirmedRequest)
	// if resp.Errno != pb.ERRNO_OK {
	// 	t.Errorf("PartSetPartRedeemConfirmed %v %v\n", resp.Errno, resp.Errstr)
	// 	return
	// }
	// testdata.DumpData(resp)

	// General Data & State

	fmt.Println("Get Data")
	dataResp := GetData(&testdata.DataRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("GetData %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	testdata.DumpData(dataResp)

	fmt.Println("Get State")
	stateResp := GetState(&testdata.StateRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("GetState %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	fmt.Printf("state response: %v\n", *stateResp)

	//Deletion

	fmt.Println("Mark Deleted")
	markDeletedResp := MarkDeleted(&testdata.MarkDeletedRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("MarkDeleted %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	fmt.Printf("mark deleted response: %v\n", *markDeletedResp)

	fmt.Println("Delete")
	deleteResp := Delete(&testdata.DeleteRequest)
	if resp.Errno != pb.ERRNO_OK {
		t.Errorf("Delete %v %v\n", resp.Errno, resp.Errstr)
		return
	}
	fmt.Printf("delete response: %v\n", *deleteResp)

	err = DbClose()
	if err != nil {
		t.Errorf("DbClose %v\n", err)
		return
	}

	fmt.Println("PASS: test completed")
}
